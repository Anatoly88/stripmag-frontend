import CabinetLayout from '../../components/layouts/CabinetLayout'
import styled from 'styled-components'
import { GetServerSideProps } from 'next'
import BalanceInfo from '../../components/cabinet/BalanceInfo'
import { IBalance } from '../../interfaces/cabinet'

const CabinetPageTitle = styled.h2`
  font-weight: 600;
  font-size: 28px;
  line-height: 38px;
  margin-bottom: 40px;
`

const Cabinet = ({ balance }) => {
  const balanceData: IBalance = balance
  return (
    <CabinetLayout title={'Кабинет'}>
      <CabinetPageTitle>Баланс</CabinetPageTitle>
      <BalanceInfo
        moneyNow={balanceData?.money_now}
        moneyReserv={balanceData?.money_reserv}
        discount={balanceData?.discount}
      />
    </CabinetLayout>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const token = context.req.cookies['SessionToken']
  // const { API_URL } = process.env;
  const API_URL = 'https://p5s.ru'
  const fetchBalance = await fetch(`${API_URL}/api/personal/getBalanceInfo`, {
    headers: {
      'session-token': token,
    },
  })
  const resBalance = await fetchBalance.json()
  const balance = resBalance.status && resBalance.status === 1 ? resBalance.data : null
  console.log(resBalance)

  return {
    props: {
      balance,
    },
  }
}

export default Cabinet
