import CabinetLayout from '../../components/layouts/CabinetLayout'
import { GetServerSideProps } from 'next'

const CabinetAnalyticsPage = ({}) => {
  return <CabinetLayout title={'Аналитика'}>Аналитика</CabinetLayout>
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const token = context.req.cookies['SessionToken']
  // const { API_URL } = process.env;
  const API_URL = 'https://p5s.ru'

  return {
    props: {},
  }
}

export default CabinetAnalyticsPage
