import CabinetLayout from '../../components/layouts/CabinetLayout'
import { GetServerSideProps } from 'next'
import BalanceInfo from '../../components/cabinet/BalanceInfo'
import { IBalance } from '../../interfaces/cabinet'
import styled from 'styled-components'

const BalancePeriodHeader = styled.header``
const BalanceList = styled.ul``
const BalanceListItem = styled.li``

const CabinetBalancePage = ({ balance }) => {
  const balanceData: IBalance = balance
  return (
    <CabinetLayout title={'Баланс'}>
      <BalanceInfo
        moneyNow={balanceData.money_now}
        moneyReserv={balanceData.money_reserv}
        discount={balanceData.discount}
      />
      <BalancePeriodHeader></BalancePeriodHeader>
    </CabinetLayout>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const token = context.req.cookies['SessionToken']
  // const { API_URL } = process.env;
  const API_URL = 'https://p5s.ru'
  const fetchBalance = await fetch(`${API_URL}/api/personal/getBalanceInfo`, {
    headers: {
      'session-token': token,
    },
  })
  const resBalance = await fetchBalance.json()
  const fetchBalancePeriod = await fetch(`${API_URL}/api/personal/getBalanceHistory?dateFrom=2019-08-14`, {
    headers: {
      'session-token': token,
    },
  })
  const resBalancePeriod = await fetchBalancePeriod.json()
  console.log(resBalancePeriod)

  const balance = resBalance.status && resBalance.status === 1 ? resBalance.data : null

  return {
    props: {
      balance,
    },
  }
}

export default CabinetBalancePage
