import AppContainer from '../../../components/ui/AppContainer'
import MainLayout from '../../../components/layouts/MainLayout'
import Breadcrumbs from '../../../components/ui/Breadcrumbs'
import CatalogNav from '../../../components/catalog/CatalogNav'
import { GetServerSideProps } from 'next'
import GridContainer from '../../../components/ui/GridContainer'
import CatalogHeader from '../../../components/catalog/CatalogHeader'
import CatalogSidebar from '../../../components/catalog/CatalogSidebar'
import CatalogList from '../../../components/catalog/CatalogList'
import {
  ICatalogDetailPageData,
  ICatalogFilterItem,
  ICatalogItem,
  ICatalogPageData,
  IFilters,
} from '../../../interfaces/catalog'
import { fetchMenu, useMenu } from '../../../hooks'
import { QueryClient } from 'react-query'
import { dehydrate } from 'react-query/hydration'
import { useRouter } from 'next/router'
import CatalogDetailLayout from '../../../components/layouts/CatalogDetailLayout'
import React, { useEffect, useState } from 'react'
import queryString from 'query-string'
import pickBy from 'lodash/pickBy'
import isEmpty from 'lodash/isEmpty'
import { isMobile, isDesktop } from 'react-device-detect'
import styled from 'styled-components'
import CatalogFiltersModal from '../../../components/catalog/CatalogFiltersModal'

interface ICategoryProps {
  catalogData: ICatalogPageData
  catalogItems: ICatalogItem[]
  catalogItemsTotal: number
  catalogSort: object
  filters: IFilters
  filterOthers: ICatalogFilterItem
  pageId: string
  detailData: ICatalogDetailPageData
  slug: string
  totalItems: number
}

const Category = ({
  catalogData,
  catalogItems,
  catalogItemsTotal,
  catalogSort,
  filters,
  detailData,
  totalItems,
}: ICategoryProps) => {
  let sidebarMenu = []
  const pageId = catalogData?.id
  const router = useRouter()
  const pageQuery = router.query.slug
  const isCategory = pageQuery.length === 1
  const { isLoading, error, data } = useMenu('catalog')
  const [mobile, setMobile] = useState(false)
  const showNavInHeader = isCategory && sidebarMenu

  useEffect(() => {
    setMobile(isMobile)
  }, [setMobile])

  if (error) console.log(error)

  if (data && data.length > 0 && !error && catalogData) {
    // TODO вынести в helpers
    const findItemNested = (arr, itemId, nestingKey) =>
      arr.reduce((a, item) => {
        if (a) return a
        if (item.id === itemId) return item
        if (item[nestingKey]) return findItemNested(item[nestingKey], itemId, nestingKey)
      }, null)
    const result = findItemNested(data, pageId, 'children')

    if (isCategory) {
      const currentMenu = data.filter((item) => item.id === catalogData.id)
      sidebarMenu = [...currentMenu[0].children]
    } else {
      sidebarMenu = result
    }
  }

  return (
    <MainLayout title={catalogData?.name}>
      <AppContainer>
        <Breadcrumbs navList={[{ name: 'Каталог' }]} />
        {catalogData && (
          <>
            <CatalogHeader
              title={catalogData?.h1Title || catalogData?.name || 'Заголовок...'}
              titleMargin={!showNavInHeader && mobile ? '32px 0 42px' : ''}
            >
              {showNavInHeader && <CatalogNav list={sidebarMenu} />}
              {mobile && <CatalogFiltersModal filters={filters} sort={catalogSort} total={totalItems} />}
            </CatalogHeader>
            {mobile && <CatalogList list={catalogItems} total={catalogItemsTotal} />}

            {!mobile && (
              <GridContainer cols={'276px 1fr'} gap={'0 48px'}>
                <CatalogSidebar filters={filters}>
                  {!isCategory && sidebarMenu && (
                    <CatalogNav inSidebar={true} list={sidebarMenu} active={catalogData.id} />
                  )}
                </CatalogSidebar>
                <CatalogList list={catalogItems} total={catalogItemsTotal} sort={catalogSort} />
              </GridContainer>
            )}
          </>
        )}
        {!catalogData && detailData && <CatalogDetailLayout data={detailData} />}
      </AppContainer>
    </MainLayout>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  let resCatalog = null
  let resFilter = null
  let resDetail = null
  const token = context.req.cookies['SessionToken']
  const querySlug = context.query.slug
  const queryPage = context.query.page || 1
  const queryPerPage = context.query.per_page || 30
  const querySort = context.query.sort || 1
  // @ts-ignore
  const slug = querySlug.join('/')
  const lastSlug = querySlug[querySlug.length - 1]
  const isDetail = querySlug.length > 1 && /[0-9]/.test(lastSlug)
  // const { API_URL } = process.env;
  const API_URL = 'https://p5s.ru'
  // const { API_MOCK_URL } = process.env;
  const API_MOCK_URL = 'https://my-mock-data-serv.herokuapp.com'
  const queryClient = new QueryClient()
  await queryClient.prefetchQuery('menu', () => fetchMenu('catalog'))
  const resCatalogMock = await (await fetch(`${API_MOCK_URL}/catalogAPI`)).json()
  const { catalogOthers } = resCatalogMock
  const filtersQuery = pickBy(
    { ...(context.query || {}) },
    (q, key) => !isEmpty(q) && key !== 'slug' && key !== 'page' && key !== 'sort' && key !== 'per_page'
  )
  const newQuery = Object.entries(filtersQuery).map(([key, val]) => {
    const filterName = `filter[${key}]`
    // @ts-ignore
    return { [filterName]: val.split(',') }
  })

  const newQueryStringify = newQuery.map((item) => {
    return queryString.stringify(
      {
        ...item,
      },
      { arrayFormat: 'index' }
    )
  })

  const filtersParams = newQueryStringify.join('&')
  if (!isDetail) {
    const fetchCatalog = await fetch(
      `${API_URL}/api/catalog/getList?type=catalog&slug=${slug}&per_page=${queryPerPage}&sort=${querySort}&page=${queryPage}&${filtersParams}`,
      {
        headers: {
          'session-token': token,
        },
      }
    )
    resCatalog = await fetchCatalog.json()

    resFilter = await (
      await fetch(
        `${API_URL}/api/catalog/getFilter?type=catalog&slug=${slug}&per_page=${queryPerPage}&sort=${querySort}&page=${queryPage}&${filtersParams}`
      )
    ).json()

    if (!resCatalog || resCatalog.status === 0) {
      return {
        notFound: true,
      }
    }
  } else {
    resDetail = await (
      await fetch(`${API_URL}/api/catalog/getProduct?slug=/${slug}/`, {
        headers: {
          'session-token': token,
        },
      })
    ).json()

    if (!resDetail || resDetail.status === 0) {
      return {
        notFound: true,
      }
    }
  }

  const catalogData = resCatalog ? resCatalog?.catalog : null
  const catalogItems = resCatalog ? resCatalog?.items : null
  const catalogItemsTotal = resCatalog ? resCatalog?.total : 0
  const catalogSort = resCatalog ? resCatalog?.sort : null
  const detailData = resDetail ? resDetail?.data : null
  const filters = resFilter ? resFilter : null
  const filterOthers = catalogOthers ? catalogOthers : null // TODO mock tmp
  const totalItems = resCatalog ? resCatalog.total : 0

  return {
    props: {
      catalogData,
      catalogItems,
      catalogItemsTotal,
      catalogSort,
      detailData,
      filters,
      filterOthers,
      slug,
      totalItems,
      dehydratedState: dehydrate(queryClient),
    },
  }
}

export default Category
