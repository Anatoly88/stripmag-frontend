import MainLayout from '../../components/layouts/MainLayout'
import AppSectionTitle from '../../components/ui/AppSectionTitle'
import AppContainer from '../../components/ui/AppContainer'

const Order = () => {
  return (
    <MainLayout>
      <AppContainer>
        <AppSectionTitle text={'Оформление заказа'} marginTop={'140px'} />
      </AppContainer>
    </MainLayout>
  )
}

export default Order
