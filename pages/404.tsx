import MainLayout from "../components/layouts/MainLayout";
import AppContainer from "../components/ui/AppContainer";
import AppLink from "../components/ui/AppLink";
import ErrorSvg from '../public/images/404.svg'
import styled from "styled-components";

const StyledErrorWrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-flow: column nowrap;
  width: 100%;
  height: calc(100vh - ${({theme}) => theme.headerHeight});
  
  @media ${({theme}) => theme.media.mobile} {
    height: calc(100vh - ${({theme}) => theme.headerHeightMob});
  }
`

const StyledErrorIcon = styled(ErrorSvg)`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  width: 100%;
  height: 100%;
  max-width: 1032px;
  max-height: 406px;
  
  @media ${({theme}) => theme.media.mobile} {
    position: relative;
    left: auto;
    top: auto;
    transform: none;
    max-width: 222px;
    max-height: 88px;
    margin-bottom: 28px;
  }
`

const StyledErrorText = styled.p`
  max-width: 600px;
  text-align: center;
  font-size: 25px;
  line-height: 34px;
  position: relative;
  z-index: 1;
  
  @media ${({theme}) => theme.media.mobile} {
    max-width: 320px;
    font-size: 16px;
    line-height: 24px;
  }
`

const StyledErrorLink = styled(AppLink)`
  text-decoration: underline;
  color: #000;
`

const ErrorPage = () => {
  return (
    <MainLayout title={'Ошибка - страница не найдена'} withoutFooter noPadding>
      <AppContainer>
        <StyledErrorWrapper>
          <StyledErrorIcon />
          <StyledErrorText>
            Похоже, что данной страницы у нас на сайте нет. Может, никогда и не было. Но вы точно можете перейти на нашу
            {' '}
            <StyledErrorLink url={'/'} text={'главную страницу'} />
            .
          </StyledErrorText>
        </StyledErrorWrapper>
      </AppContainer>
    </MainLayout>
  );
};

export default ErrorPage;