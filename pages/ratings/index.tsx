import MainLayout from '../../components/layouts/MainLayout'
import AppSectionTitle from '../../components/ui/AppSectionTitle'
import Breadcrumbs from '../../components/ui/Breadcrumbs'
import AppContainer from '../../components/ui/AppContainer'
import { GetServerSideProps } from 'next'
import RatingsList from '../../components/ratings/RatingsList'
import { useEffect, useState } from 'react'
import { isMobile } from 'react-device-detect'

const Ratings = ({ ratingsList }) => {
  const [mobile, setMobile] = useState(false)
  useEffect(() => {
    setMobile(isMobile)
  }, [setMobile])

  return (
    <MainLayout>
      <AppContainer>
        <Breadcrumbs navList={[{ name: 'Каталог' }]} />
        <AppSectionTitle
          text={'Рейтинги'}
          marginTop={mobile ? '32px' : '52px'}
          marginBottom={mobile ? '40px' : '72px'}
        />
        <RatingsList list={ratingsList} />
      </AppContainer>
    </MainLayout>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const API_URL = 'https://p5s.ru'
  // const { API_URL } = process.env
  let ratingsList = []
  const fetchList = await (await fetch(`${API_URL}/api/catalog/getMenu?type=top`)).json()

  if (fetchList && fetchList.status === 1) {
    ratingsList = fetchList?.data
  }

  return {
    props: {
      ratingsList,
    },
  }
}

export default Ratings
