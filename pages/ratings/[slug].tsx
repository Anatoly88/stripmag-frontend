import MainLayout from '../../components/layouts/MainLayout'
import { useRouter } from 'next/router'
import { GetServerSideProps } from 'next'
import Breadcrumbs from '../../components/ui/Breadcrumbs'
import AppContainer from '../../components/ui/AppContainer'
import RatingYears from '../../components/ratings/RatingYears'
import CatalogTitle from '../../components/catalog/CatalogTitle'
import styled from 'styled-components'
import CatalogCard from '../../components/catalog/CatalogCard'

const RatingsList = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  gap: 96px 48px;
  padding-top: 96px;

  @media ${({ theme }) => theme.media.mobile} {
    display: flex;
    flex-flow: column nowrap;
    padding-top: 60px;
    gap: 0;
  }
`

const RatingsTitle = styled(CatalogTitle)`
  @media ${({ theme }) => theme.media.mobile} {
    margin: 32px 0 40px;
  }
`

const RatingsCatalogCard = styled(CatalogCard)`
  @media ${({ theme }) => theme.media.mobile} {
    &:not(:last-of-type) {
      margin-bottom: 60px;
    }
  }
`

const years = [
  '2021',
  '2020',
  '2019',
  '2018',
  '2017',
  '2016',
  '2015',
  '2014',
  '2013',
  '2012',
  '2011',
  '2010',
  '2009',
  '2008',
  '2006',
  '2004',
  '2002',
]

const RatingDetail = ({ ratingsList, pageData }) => {
  const showList = ratingsList && ratingsList.length > 0

  return (
    <MainLayout>
      <AppContainer>
        <Breadcrumbs navList={[{ name: 'Каталог' }]} />
        <RatingsTitle text={pageData?.name} />
        <RatingYears list={years} />

        {showList && (
          <RatingsList>
            {ratingsList.map((item, index) => (
              <RatingsCatalogCard
                name={item.name}
                brand_name={item.brand_name}
                image={item.image}
                slug={item.slug}
                price={item.price}
                key={`ratings_list_item_${index}`}
              />
            ))}
          </RatingsList>
        )}
      </AppContainer>
    </MainLayout>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const query = context.query
  const queryId = query?.id
  const queryYear = query?.year || 2021
  const API_URL = 'https://p5s.ru'
  // const { API_URL } = process.env
  let ratingsList = []
  let pageData = null
  const fetchList = await fetch(
    `${API_URL}/api/catalog/getList/?type=top&id=${queryId}&per_page=30&page=1&filter[year]=${queryYear}`
  )

  const parsedList = await fetchList.json()

  if (parsedList && parsedList.status === 1) {
    ratingsList = parsedList?.items
    pageData = parsedList?.catalog
  }

  return {
    props: {
      ratingsList,
      parsedList,
      pageData,
    },
  }
}

export default RatingDetail
