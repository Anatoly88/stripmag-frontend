import MainLayout from '../../components/layouts/MainLayout'
import AppSectionTitle from '../../components/ui/AppSectionTitle'
import Breadcrumbs from '../../components/ui/Breadcrumbs'
import AppContainer from '../../components/ui/AppContainer'
import { GetServerSideProps } from 'next'
import { fetchCollectionMenu, useCollectionMenu } from '../../hooks'
import { QueryClient } from 'react-query'
import { dehydrate } from 'react-query/hydration'
import { useRouter } from 'next/router'
import styled from 'styled-components'
import AppReadMore from '../../components/ui/AppReadMore'
import Link from 'next/link'
import { lineClamp } from '../../styles/mixins'
import { useEffect, useState } from 'react'
import { isMobile } from 'react-device-detect'

const CollectionsList = styled.ul`
  list-style: none;
  padding: 0 0 90px;
  margin: 0;
`

const CollectionsListItem = styled.li`
  position: relative;
  display: grid;
  grid-template-columns: 384px 660px;
  gap: 108px 0;
  min-height: 264px;
  padding: 46px;
  border: 2px solid #000;
  border-radius: ${({ theme }) => theme.radius};

  &:not(:last-of-type) {
    margin-bottom: 48px;

    @media ${({ theme }) => theme.media.mobile} {
      margin-bottom: 28px;
    }
  }

  @media ${({ theme }) => theme.media.mobile} {
    display: flex;
    flex-flow: column nowrap;
    padding: 24px 24px 74px;
    gap: 0;
  }
`

const CollectionsListDescription = styled.div`
  @media ${({ theme }) => theme.media.mobile} {
    font-size: 18px;
    line-height: 28px;
    ${lineClamp(6, '168px')}
  }
`

const CollectionsListInfo = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-between;

  h5 {
    font-weight: 600;
    font-size: 28px;
    line-height: 38px;
    margin-bottom: 6px;

    @media ${({ theme }) => theme.media.mobile} {
      font-size: 24px;
      line-height: 34px;
    }

    ${lineClamp(3, '102px')}
  }

  ul {
    display: flex;
    padding: 0;
    margin: 0;
    list-style: none;
    color: ${({ theme }) => theme.colors.grayText};

    @media ${({ theme }) => theme.media.mobile} {
      margin-bottom: 24px;
    }
  }

  li {
    position: relative;

    &:not(:first-of-type) {
      margin-left: 36px;

      &:before {
        content: '';
        position: absolute;
        left: -20px;
        top: 50%;
        width: 4px;
        height: 4px;
        border-radius: 50%;
        background-color: ${({ theme }) => theme.colors.grayText};
        transform: translateY(-50%);
      }
    }

    @media ${({ theme }) => theme.media.mobile} {
      font-size: 16px;
      line-height: 24px;
      white-space: nowrap;
    }
  }
`

const CollectionsListItemLink = styled(AppReadMore)`
  @media ${({ theme }) => theme.media.mobile} {
    position: absolute;
    bottom: 24px;
    left: 24px;
  }
`

const Collections = () => {
  const { query } = useRouter()
  const [mobile, setMobile] = useState(false)
  let collectionsList = []
  const { isLoading, error, data } = useCollectionMenu()

  useEffect(() => {
    setMobile(isMobile)
  }, [setMobile])

  if (error) console.log(error)

  if (data && data.length > 0 && !error) {
    collectionsList = data.filter((item) => item.parent_id === query.brandId)
  }

  const showCollectionsList = collectionsList && collectionsList.length > 0

  return (
    <MainLayout>
      <AppContainer>
        <Breadcrumbs navList={[{ name: 'Каталог' }]} />
        <AppSectionTitle text={'Коллекции'} marginTop={mobile ? '32px' : '52px'} marginBottom="72px" />

        {showCollectionsList && (
          <CollectionsList>
            {collectionsList.map((item, index) => (
              <CollectionsListItem key={`collections_list_item_${index}`}>
                <CollectionsListInfo>
                  <header>
                    <h5>{item.name}</h5>
                    <ul>
                      <li>34 товара</li>
                      <li>От 1 250 до 3 987 ₽</li>
                    </ul>
                  </header>

                  <Link href={{ pathname: `/collections/${item.code}`, query: { id: item.id } }} passHref>
                    <CollectionsListItemLink />
                  </Link>
                </CollectionsListInfo>
                <CollectionsListDescription dangerouslySetInnerHTML={{ __html: item.description }} />
              </CollectionsListItem>
            ))}
          </CollectionsList>
        )}
      </AppContainer>
    </MainLayout>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const queryClient = new QueryClient()
  await queryClient.prefetchQuery('menu', () => fetchCollectionMenu())

  return {
    props: { dehydratedState: dehydrate(queryClient) },
  }
}

export default Collections
