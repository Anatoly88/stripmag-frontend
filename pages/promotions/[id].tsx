import { GetServerSideProps } from 'next'
import AppContainer from '../../components/ui/AppContainer'
import Breadcrumbs from '../../components/ui/Breadcrumbs'
import CatalogHeader from '../../components/catalog/CatalogHeader'
import CatalogFiltersModal from '../../components/catalog/CatalogFiltersModal'
import CatalogList from '../../components/catalog/CatalogList'
import GridContainer from '../../components/ui/GridContainer'
import CatalogSidebar from '../../components/catalog/CatalogSidebar'
import MainLayout from '../../components/layouts/MainLayout'
import React, { useEffect, useState } from 'react'
import { isMobile } from 'react-device-detect'
import styled from 'styled-components'
import pickBy from 'lodash/pickBy'
import isEmpty from 'lodash/isEmpty'
import queryString from 'query-string'

const ActionDescription = styled.div`
  max-width: 816px;
  font-size: 18px;
  line-height: 28px;
  margin-bottom: 72px;
  text-align: left;

  > div {
    text-align: left;
  }

  p {
    text-align: left;
    margin-bottom: 16px;

    &:last-of-type {
      margin-bottom: 0;
    }
  }

  ul {
    padding: 0;
    list-style: none;

    li {
      margin-bottom: 16px;

      &:last-of-type {
        margin-bottom: 0;
      }
    }
  }

  @media ${({ theme }) => theme.media.mobile} {
    margin-bottom: 60px;
  }
`

const PromotionPage = ({ actionsData, actionsSort, totalItems, filters, actionItems }) => {
  const [mobile, setMobile] = useState(false)
  useEffect(() => {
    setMobile(isMobile)
  }, [setMobile])
  return (
    <MainLayout title={actionsData?.name}>
      <AppContainer>
        <Breadcrumbs navList={[{ name: 'Каталог' }]} />
        <CatalogHeader title={actionsData?.h1Title || actionsData?.name || 'Заголовок...'}>
          <ActionDescription dangerouslySetInnerHTML={{ __html: actionsData.description }} />
          {mobile && <CatalogFiltersModal filters={filters} sort={actionsSort} total={totalItems} />}
        </CatalogHeader>
        {mobile && <CatalogList list={actionItems} total={totalItems} />}

        {!mobile && (
          <GridContainer cols={'276px 1fr'} gap={'0 48px'}>
            <CatalogSidebar filters={filters} />
            <CatalogList list={actionItems} total={totalItems} sort={actionsSort} />
          </GridContainer>
        )}
      </AppContainer>
    </MainLayout>
  )
}
export const getServerSideProps: GetServerSideProps = async (context) => {
  let resActions = null
  let resFilter = null
  const queryId = context.query?.id || 0
  const queryPage = context.query.page || 1
  const queryPerPage = context.query.per_page || 30
  const querySort = context.query.sort || 1
  // const { API_URL } = process.env;
  const API_URL = 'https://p5s.ru'

  const filtersQuery = pickBy(
    { ...(context.query || {}) },
    (q, key) => !isEmpty(q) && key !== 'slug' && key !== 'page' && key !== 'sort' && key !== 'per_page' && key !== 'id'
  )
  const newQuery = Object.entries(filtersQuery).map(([key, val]) => {
    const filterName = `filter[${key}]`
    // @ts-ignore
    return { [filterName]: val.split(',') }
  })

  const newQueryStringify = newQuery.map((item) => {
    return queryString.stringify(
      {
        ...item,
      },
      { arrayFormat: 'index' }
    )
  })

  const filtersParams = newQueryStringify.join('&')

  const fetchActions = await fetch(
    `${API_URL}/api/catalog/getList?type=action&id=${queryId}&per_page=${queryPerPage}&sort=${querySort}&page=${queryPage}&${filtersParams}`
  )
  resActions = await fetchActions.json()

  resFilter = await (
    await fetch(
      `${API_URL}/api/catalog/getFilter?type=action&id=${queryId}&per_page=${queryPerPage}&sort=${querySort}&page=${queryPage}&${filtersParams}`
    )
  ).json()

  if (!resActions || resActions.status === 0) {
    return {
      notFound: true,
    }
  }

  const actionsData = resActions ? resActions?.catalog : null
  const actionsSort = resActions ? resActions?.sort : null
  const actionItems = resActions ? resActions?.items : null
  const filters = resFilter ? resFilter : null
  const totalItems = resActions ? resActions.total : 0

  return {
    props: {
      actionsData,
      filters,
      actionsSort,
      totalItems,
      actionItems,
    },
  }
}

export default PromotionPage
