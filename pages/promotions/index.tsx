import MainLayout from '../../components/layouts/MainLayout'
import { GetServerSideProps } from 'next'
import { QueryClient } from 'react-query'
import { fetchActionMenu, useActionMenu } from '../../hooks'
import { dehydrate } from 'react-query/hydration'
import AppContainer from '../../components/ui/AppContainer'
import styled from 'styled-components'
import { IPromotionsCard } from '../../interfaces/promotions'
import Breadcrumbs from '../../components/ui/Breadcrumbs'
import React from 'react'
import AppSectionTitle from '../../components/ui/AppSectionTitle'
import Image from 'next/image'
import AppReadMore from '../../components/ui/AppReadMore'
import Link from 'next/link'
import { lineClamp } from '../../styles/mixins'
import moment from 'moment'

const ActionsGrid = styled.div`
  display: grid;
  grid-template-areas:
    'first second second three'
    'four four five five'
    'six seven seven eight'
    'nine nine ten ten';
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-auto-rows: 276px;
  grid-gap: 48px;
`

const ActionCard = styled.div`
  position: relative;
  overflow: hidden;
  border-radius: ${({ theme }) => theme.radius};

  &:nth-child(1) {
    grid-area: first;
  }
  &:nth-child(2) {
    grid-area: second;
  }
  &:nth-child(3) {
    grid-area: three;
  }
  &:nth-child(4) {
    grid-area: four;
  }
  &:nth-child(5) {
    grid-area: five;
  }
  &:nth-child(6) {
    grid-area: six;
  }
  &:nth-child(7) {
    grid-area: seven;
  }
  &:nth-child(8) {
    grid-area: eight;
  }
  &:nth-child(9) {
    grid-area: nine;
  }
  &:nth-child(10) {
    grid-area: ten;
  }
`

const ActionCardBg = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  border-radius: ${({ theme }) => theme.radius};
  overflow: hidden;
  background: linear-gradient(51.06deg, #9358f7 0.87%, #7b78f2 25.96%, #6197ee 49.23%, #45b5e9 74.93%, #10d7e2 97.48%),
    linear-gradient(223.53deg, #6671e5 3.65%, #4852d9 102.22%),
    linear-gradient(224.86deg, #73ccd8 4.87%, #2b6b9f 96.04%),
    linear-gradient(132.95deg, #8de6e5 4.31%, #75aff2 49.13%, #6a8dfd 95.6%),
    linear-gradient(224.86deg, #d5daf0 4.87%, #296edf 98.81%), #3bc2ba;
`

const ActionCardWrapper = styled.div`
  position: relative;
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-between;
  z-index: 1;
  width: 100%;
  height: 100%;
  padding: 32px;
  background-color: rgba(0, 0, 0, 0.8);

  h5 {
    font-weight: 600;
    font-size: 28px;
    line-height: 38px;
    color: ${({ theme }) => theme.colors.white};
    margin: 0;
    word-break: break-word;
    ${lineClamp(4, '152px')};

    &.isEnd {
      color: ${({ theme }) => theme.colors.lightGray};
    }
  }
`

const ActionReadMore = styled(AppReadMore)`
  color: ${({ theme }) => theme.colors.white};

  svg {
    fill: ${({ theme }) => theme.colors.white};
  }
`

const ActionEnd = styled.p`
  font-weight: 600;
  font-size: 18px;
  line-height: 26px;
  color: ${({ theme }) => theme.colors.lightGray};
`

const Promotions = () => {
  const actionsMenu = useActionMenu()
  let actionList: IPromotionsCard[] = []
  if (!actionsMenu.isLoading) {
    actionList = actionsMenu.data.reverse()
  }

  const getEndDate = (date) => {
    const actionEndDate = moment(date)
    const currentDate = moment()

    return currentDate > actionEndDate
  }

  return (
    <MainLayout>
      <AppContainer>
        <Breadcrumbs navList={[{ name: 'Каталог' }]} />
        <AppSectionTitle text={'Акции'} marginTop={'52px'} />
        <ActionsGrid>
          {actionList &&
            actionList.map((item, index) => (
              <ActionCard key={`action_${index}`}>
                <ActionCardBg className={getEndDate(item.end_date) ? 'isEnd' : ''}>
                  {item.image500 && <Image src={item.image500} alt="" layout="fill" objectFit="cover" />}
                </ActionCardBg>
                <ActionCardWrapper>
                  <h5 className={getEndDate(item.end_date) ? 'isEnd' : ''}>{item.name}</h5>
                  {getEndDate(item.end_date) && <ActionEnd>Акция закончилась</ActionEnd>}
                  {!getEndDate(item.end_date) && (
                    <Link href={`/promotions/${item.id}`} passHref>
                      <ActionReadMore />
                    </Link>
                  )}
                </ActionCardWrapper>
              </ActionCard>
            ))}
        </ActionsGrid>
      </AppContainer>
    </MainLayout>
  )
}
export const getServerSideProps: GetServerSideProps = async (context) => {
  const queryClient = new QueryClient()
  await queryClient.prefetchQuery('actionMenu', () => fetchActionMenu())

  return {
    props: {
      dehydratedState: dehydrate(queryClient),
    },
  }
}
export default Promotions
