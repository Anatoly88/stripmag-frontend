import MainLayout from '../../components/layouts/MainLayout'
import AppSectionTitle from '../../components/ui/AppSectionTitle'
import Breadcrumbs from '../../components/ui/Breadcrumbs'
import AppContainer from '../../components/ui/AppContainer'
import { GetServerSideProps } from 'next'
import BrandsAlphabet from '../../components/brands/BrandsAlphabet'
import {fetchBrandMenu, fetchMenu, getBrandMenuItem, useBrandMenu, useMenu} from '../../hooks'
import { QueryClient } from 'react-query'
import { dehydrate } from 'react-query/hydration'
import { useRouter } from 'next/router'
import BrandsList from '../../components/brands/BrandsList'
import { useEffect, useState } from 'react'
import { isMobile } from 'react-device-detect'

const alphabet = [
  '#',
  'A',
  'B',
  'C',
  'D',
  'E',
  'F',
  'G',
  'H',
  'I',
  'J',
  'K',
  'L',
  'M',
  'N',
  'O',
  'P',
  'Q',
  'S',
  'R',
  'T',
  'U',
  'V',
  'W',
  'X',
  'Y',
  'Z',
]

const Brands = ({}) => {
  const { query } = useRouter()
  const { isLoading, error, data } = useBrandMenu()
  const [mobile, setMobile] = useState(false)
  let filteredList = []
  const searchTerm = query?.letter || '#'

  useEffect(() => {
    setMobile(isMobile)
  }, [setMobile])

  if (searchTerm === '#' && data) {
    filteredList = data.filter((item) => /[0-9]/.test(item.name.charAt(0)))
  } else {
    if (typeof searchTerm === 'string' && data) {
      filteredList = data.filter((item) => item.name.trim().toLowerCase().startsWith(searchTerm.trim().toLowerCase()))
    }
  }

  if (error) console.log(error)

  return (
    <MainLayout>
      <AppContainer>
        <Breadcrumbs navList={[{ name: 'Каталог' }]} />
        <AppSectionTitle text={getBrandMenuItem().name} marginTop={mobile ? '32px' : '52px'} marginBottom={mobile ? '40px' : '60px'} />
        <BrandsAlphabet list={alphabet} />

        <BrandsList list={filteredList} firstLetter={query.letter} />
      </AppContainer>
    </MainLayout>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const queryClient = new QueryClient()
  await queryClient.prefetchQuery('menu', () => fetchBrandMenu())

  return {
    props: { dehydratedState: dehydrate(queryClient) },
  }
}

export default Brands
