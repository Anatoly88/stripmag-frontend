import MainLayout from '../../components/layouts/MainLayout'
import { GetServerSideProps } from 'next'
import Breadcrumbs from '../../components/ui/Breadcrumbs'
import AppContainer from '../../components/ui/AppContainer'
import CatalogHeader from '../../components/catalog/CatalogHeader'
import CatalogNav from '../../components/catalog/CatalogNav'
import GridContainer from '../../components/ui/GridContainer'
import CatalogSidebar from '../../components/catalog/CatalogSidebar'
import CatalogList from '../../components/catalog/CatalogList'
import { useBrandMenu, useCollectionMenu, useMenu } from '../../hooks'
import React, { useEffect, useState } from 'react'
import { isMobile } from 'react-device-detect'
import CatalogFiltersModal from '../../components/catalog/CatalogFiltersModal'
import { ModalProvider, BaseModalBackground } from 'styled-react-modal'
import styled from 'styled-components'
import pickBy from 'lodash/pickBy'
import isEmpty from 'lodash/isEmpty'
import queryString from 'query-string'

const FadingBackground = styled(BaseModalBackground)`
  opacity: ${(props) => props.opacity};
  transition: all 0.3s ease-in-out;
`

const RatingDetail = ({ brandsList, brandsSort, pageData, filters, totalList }) => {
  let sidebarMenu = []
  const [mobile, setMobile] = useState(false)

  useEffect(() => {
    setMobile(isMobile)
  }, [setMobile])

  const { isLoading, error, data } = useCollectionMenu()

  if (error) console.log(error)

  if (data && data.length > 0 && !error && pageData) {
    sidebarMenu = data.filter((item) => item.parent_id === pageData.id)
  }

  const showNav = sidebarMenu && sidebarMenu.length > 0
  return (
    <MainLayout>
      <AppContainer>
        <Breadcrumbs navList={[{ name: 'Каталог' }]} />
        {pageData && (
          <>
            <CatalogHeader
              title={pageData?.h1Title || pageData?.name || 'Заголовок...'}
              titleMargin={mobile ? '16px 0 42px' : ''}
              preview={pageData.preview}
              description={pageData.description}
            >
              {showNav && (
                <CatalogNav
                  list={sidebarMenu}
                  title={'Коллекции'}
                  titleLink={{ pathname: '/collections/', query: { brandId: pageData.id } }}
                  isBrand
                />
              )}
              {mobile && (
                <ModalProvider backgroundComponent={FadingBackground}>
                  <CatalogFiltersModal filters={filters} sort={brandsSort} />
                </ModalProvider>
              )}
            </CatalogHeader>
            {mobile && <CatalogList list={brandsList} total={totalList} />}

            {!mobile && (
              <GridContainer cols={'276px 1fr'} gap={'0 48px'}>
                <CatalogSidebar filters={filters} />
                <CatalogList list={brandsList} total={totalList} sort={brandsSort} />
              </GridContainer>
            )}
          </>
        )}
      </AppContainer>
    </MainLayout>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  let brandsList = []
  let brandsSort = null
  let pageData = null
  let filters = null
  let totalList = null
  const token = context.req.cookies['SessionToken']
  const querySlug = context.query.slug
  const queryPage = context.query.page || 1
  const queryPerPage = context.query.per_page || 30
  const querySort = context.query.sort || 1
  const API_URL = 'https://p5s.ru'
  const filtersQuery = pickBy(
    { ...(context.query || {}) },
    (q, key) => !isEmpty(q) && key !== 'slug' && key !== 'page' && key !== 'sort' && key !== 'per_page' && key !== 'id'
  )
  const newQuery = Object.entries(filtersQuery).map(([key, val]) => {
    const filterName = `filter[${key}]`
    // @ts-ignore
    return { [filterName]: val.split(',') }
  })
  const newQueryStringify = newQuery.map((item) => {
    return queryString.stringify(
      {
        ...item,
      },
      { arrayFormat: 'index' }
    )
  })
  const filtersParams = newQueryStringify.join('&')

  const fetchList = await fetch(
    `${API_URL}/api/catalog/getList?type=brand&slug=${querySlug}&per_page=${queryPerPage}&sort=${querySort}&page=${queryPage}&${filtersParams}`,
    {
      headers: {
        'session-token': token,
      },
    }
  )
  const fetchFilter = await fetch(
    `${API_URL}/api/catalog/getFilter?type=brand&slug=${querySlug}&per_page=${queryPerPage}&sort=${querySort}&page=${queryPage}&${filtersParams}`
  )

  const parsedList = await fetchList.json()
  console.log(token)
  const parsedFilter = await fetchFilter.json()

  if (parsedList && parsedList.status === 1) {
    brandsList = parsedList?.items
    brandsSort = parsedList?.sort
    pageData = parsedList?.catalog
    totalList = parsedList?.total
  }

  if (parsedFilter && parsedFilter.status === 1) {
    filters = parsedFilter
  }

  return {
    props: {
      brandsList,
      parsedList,
      pageData,
      filters,
      totalList,
      brandsSort,
    },
  }
}

export default RatingDetail
