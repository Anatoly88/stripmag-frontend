import { normalize } from 'styled-normalize'
import styled, { createGlobalStyle, ThemeProvider } from 'styled-components'
import NextNprogress from 'nextjs-progressbar'
import { hideScrollbar } from '../styles/mixins'
import { QueryClient, QueryClientProvider, useQuery } from 'react-query'
import { Hydrate } from 'react-query/hydration'
import { ModalProvider, BaseModalBackground } from 'styled-react-modal'
import { wrapper } from '../store'
import React, { FC, useEffect } from 'react'
import { AppProps } from 'next/app'
import { useCookies } from 'react-cookie'
import { to } from '@react-spring/web'
import { useDispatch } from 'react-redux'
import { checkIsAuth } from '../store/actions/authActions'
import { useTypedSelector } from '../hooks/useTypedSelector'

const GlobalStyle = createGlobalStyle`
  ${normalize}

  :root {
    --page-padding: 96px;
    --border-radius: 8px;
    --section-padding-top: 90px;
    --section-padding-bottom: 90px;
    
    @media ${({ theme }) => theme.media.mobile} {
      --section-padding-top: 48px;
      --section-padding-bottom: 48px;
    }
  }

  html,
  body {
    padding: 0;
    font-family: Inter, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
    Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
    background-color: ${(props) => props.theme.colors.background};
    font-size: ${({ theme }) => theme.fonts.baseSize};
    line-height: ${({ theme }) => theme.fonts.baseLineHeight};
    font-feature-settings: normal;
    height: 100%;
    hyphens: auto;
    ${hideScrollbar}
  }
  
  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  p {
    margin-top: 0;
  }

  a {
    color: inherit;
    text-decoration: none;
    transition: opacity 0.2s;
    
    &:hover {
      opacity: 0.9;
      transition: opacity 0.2s;
    }
  }

  * {
    box-sizing: border-box;
  }
  
  button {
    cursor: pointer;
  }
  
  .wrapper {
    background-color: ${({ theme }) => theme.colors.background};
    overflow-x: hidden;
    padding-bottom: 130px;
    height: 100%;

    @media ${({ theme }) => theme.media.mobile} {
      padding-bottom: 92px;
    }
  }
`

export const Field = styled.label`
  display: flex;
  flex-flow: column nowrap;
  align-items: flex-start;
  width: 100%;
  background-color: ${({ theme }) => theme.colors.white};
`

export const FieldInput = styled.input`
  width: 100%;
  padding: 8px;
  border: 2px solid ${({ theme }) => theme.colors.lightGray};
  border-radius: ${({ theme }) => theme.radius};
  height: 60px;
`

export const FieldLabel = styled.span`
  margin-bottom: 24px;
  cursor: pointer;
  color: ${({ theme }) => theme.colors.grayText};
`

const FadingBackground = styled(BaseModalBackground)`
  background-color: rgba(0, 0, 0, 0.8);
  opacity: ${(props) => props.opacity};
  transition: all 0.3s ease-in-out;
`

const theme = {
  colors: {
    primary: '#0070f3',
    background: '#f0f0f0',
    border: '#e0e0e0',
    grayText: '#909090',
    accent: '#bf1b2c',
    lightGray: '#a0a0a0',
    lightGrayRgba: 'rgba(#a0a0a0, 0.1)',
    black: '#000',
    white: '#fff',
  },
  radius: '8px',
  media: {
    mobile: '(max-width: 1280px) and (min-width: 320px)',
    desktop: '(min-width: 1281px)',
  },
  layoutWidth: '1248px',
  bannersWidth: '1440px',
  fonts: {
    baseSize: '16px',
    baseFamily: 'Inter',
    baseLineHeight: '24px',
  },
  headerHeight: '120px',
  headerHeightMob: '84px',
  hideScrollbar: `
      scrollbar-width: none;
      -ms-overflow-style: none;
      ::-webkit-scrollbar {
        width: 0;
      }
    `,
}

const WrappedApp: FC<AppProps> = ({ Component, pageProps }) => {
  const [queryClient] = React.useState(() => new QueryClient())
  const [cookies] = useCookies(['SessionToken'])
  const dispatch = useDispatch()

  useEffect(() => {
    const token = cookies?.SessionToken
    if (token && token.length > 0) {
      dispatch(checkIsAuth(token))
    }
  }, [dispatch, cookies])

  return (
    <ThemeProvider theme={theme}>
      <NextNprogress
        color="#bf1b2c"
        startPosition={0.8}
        stopDelayMs={200}
        height={4}
        showOnShallow={false}
        options={{ easing: 'ease', speed: 500, showSpinner: false }}
      />

      <QueryClientProvider client={queryClient}>
        <Hydrate state={pageProps.dehydratedState}>
          <ModalProvider backgroundComponent={FadingBackground}>
            <Component {...pageProps} />
          </ModalProvider>
        </Hydrate>
      </QueryClientProvider>

      <GlobalStyle />
    </ThemeProvider>
  )
}

export default wrapper.withRedux(WrappedApp)
