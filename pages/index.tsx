import { GetStaticProps } from 'next'
import styled from 'styled-components'

import { ICatalogItem } from '../interfaces/catalog'

import MainLayout from '../components/layouts/MainLayout'
import SliderOnce from '../components/ui/SliderOnce'
import PreviewMain from '../components/catalog/PreviewMain'
import PopularBrand from '../components/brands/PopularBrand'
import GridContainer from '../components/ui/GridContainer'
import SliderCarousel from '../components/ui/SliderCarousel'
import AppContainer from '../components/ui/AppContainer'
import AppSectionTitle from '../components/ui/AppSectionTitle'
import { useEffect, useState } from 'react'
import AppButton from '../components/ui/AppButton'
import { listToTree } from '../helpers/list-to-tree'
import { isMobile } from 'react-device-detect'
import Category from './catalog/[...slug]'
import {getBrandMenuItem} from "../hooks";

const SectionHome = styled.section`
  position: relative;
  padding: var(--section-padding-top) 0 var(--section-padding-bottom);
`

const ShowBrandsBtn = styled(AppButton)`
  margin-top: 42px;
`

const Home = ({ slides, catalogList, brandsList, newsList, articlesList }) => {
  const [showAllBrands, setShowAllBrands] = useState<Boolean>(true)
  const [isMob, setIsMob] = useState<Boolean>()

  useEffect(() => {
    setIsMob(isMobile)
  }, [setIsMob])

  const popBrandsCol = isMob ? '100%' : 'repeat(3, 1fr)'
  const popBrandsGap = isMob ? '28px' : '48px'

  return (
    <MainLayout>
      <SliderOnce slides={slides} />

      <AppContainer>
        {/*<SectionHome>*/}
        {/*  <AppSectionTitle text="Новинки" />*/}

        {/*  <GridContainer*/}
        {/*    cols={mob ? 'repeat(8, 276px)' : 'repeat(4, 1fr)'}*/}
        {/*    gap={mob ? '0 28px' : '96px 48px'}*/}
        {/*    flow={mob ? 'column' : 'row'}*/}
        {/*    scroll={mob}*/}
        {/*  >*/}
        {/*    {catalogList &&*/}
        {/*      catalogList.length > 0 &&*/}
        {/*      catalogList.map((item: ICatalogItem, index) => (*/}
        {/*        <PreviewMain*/}
        {/*          key={`catalog_news_${index}`}*/}
        {/*          name={item.name}*/}
        {/*          brands={item.brands}*/}
        {/*          price={item.price}*/}
        {/*          image={{ url: item.image.url, alt: item.image.alt }}*/}
        {/*          slug={item.slug}*/}
        {/*          tooltip={item?.tooltip}*/}
        {/*          sale={item?.sale}*/}
        {/*        />*/}
        {/*      ))}*/}
        {/*  </GridContainer>*/}
        {/*</SectionHome>*/}

        <SectionHome>
          <AppSectionTitle text="Популярные бренды" />

          <GridContainer cols={popBrandsCol} gap={popBrandsGap}>
            {brandsList &&
              brandsList.length > 0 &&
              brandsList.map((item, index) => (
                <PopularBrand
                  className={index > 2 && showAllBrands && isMob ? 'isHidden' : ''}
                  image={item?.image}
                  name={item?.name}
                  url={item.slug ? item.slug : (getBrandMenuItem().staticUrl + item?.code)}
                  key={`popular_brands_${index}`}
                />
              ))}
          </GridContainer>

          <ShowBrandsBtn style={{ display: isMob ? 'flex' : 'none' }} onClick={() => setShowAllBrands(!showAllBrands)}>
            {showAllBrands ? 'Посмотреть все бренды' : 'Скрыть бренды'}
          </ShowBrandsBtn>
        </SectionHome>
      </AppContainer>

      <AppContainer noPaddingRight={isMob}>
        <SliderCarousel title="Сводка новостей" slides={newsList} countShow={3} countScroll={3} />

        <SliderCarousel title="О нас говорят" slides={articlesList} countShow={3} countScroll={3} />
      </AppContainer>
    </MainLayout>
  )
}

export const getServerSideProps: GetStaticProps = async () => {
  let slides = null
  const API_URL  = process.env.NEXT_PUBLIC_API_URL
  const API_MOCK_URL = process.env.NEXT_PUBLIC_API_MOCK_URL

  const resMenu = await fetch(`${API_URL}/api/catalog/getMenu?type=catalog`)
  const dataMenu = await resMenu.json()
  let menu = []

  if (dataMenu && dataMenu.status === 1) {
    menu = listToTree(dataMenu.data)
  }

  const catalog = await fetch(`${API_MOCK_URL}/catalog/`)
  const brands = await fetch(`${API_URL}/api/catalog/getMenu?type=brand&isPopular=1`)
  const resBanners = await fetch(`${API_URL}/api/banners/`)
  const news = await fetch(`${API_URL}/api/news/?page=1&per_page=8`)
  const articles = await fetch(`${API_URL}/api/articles/?page=1&per_page=8`)

  const catalogList = await catalog.json()
  const brandsList = (await brands.json()).data
  const newsList = (await news.json()).items
  const articlesList = (await articles.json()).items
  const dataBanners = await resBanners.json()
  // slides = await banners.json()

  if (dataBanners) {
    const { items, status } = dataBanners

    if (status !== 0) {
      slides = items
    }
  }

  return {
    props: {
      slides,
      catalogList,
      brandsList,
      newsList,
      articlesList,
      menu,
    },
  }
}

export default Home
