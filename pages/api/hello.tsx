// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import {NextApiRequest, NextApiResponse} from "next";

interface Message extends NextApiRequest {
  query: {
    name?: string
  }
}

export default function handler(req: Message, res: NextApiResponse) {
  res.setHeader('Content-Type', 'application/json')
  const msg = req.query.name
  res.status(200).json({ name: 'John Doe Ivanich' })
}
