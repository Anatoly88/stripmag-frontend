import { useEffect, useState } from "react";
import { isMobile } from "react-device-detect";
import NewsLayout from "../../../components/layouts/NewsLayout";
import { QueryClient } from 'react-query'
import { fetchMenuStatic } from '../../../hooks'
import { dehydrate } from 'react-query/hydration'

interface InterfaceParams {
  slug: string[]
}

interface InterfaceServerSideProps {
  params: InterfaceParams
}

const ArticlePage = ({pageData, pageNav}) => {
  const [mobile, setMobile] = useState<Boolean>()

  useEffect(() => {
    setMobile(isMobile)
  }, [setMobile])

  return (
    <>
      <NewsLayout
        pageData={pageData}
        isMobile={mobile}
      />
    </>
  );
};

export const getServerSideProps = async ({params}: InterfaceServerSideProps) => {
  // const { API_URL } = process.env;
  const API_URL = 'https://p5s.ru';
  const url = params.slug
  const queryClient = new QueryClient()
  await queryClient.prefetchQuery('menuStatic', () => fetchMenuStatic())
  const res = await (await fetch(`${API_URL}/api/news/?slug=/${url}/`)).json()
  const pageData = res.data ?? []

  if (res.status === 0) {
    return {
      notFound: true
    }
  }

  return {
    props: {
      pageData,
      dehydratedState: dehydrate(queryClient)
    },
  }
}

export default ArticlePage;