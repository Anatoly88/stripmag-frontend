import { useEffect, useState } from "react";
import { isMobile } from "react-device-detect";
import { GetServerSideProps } from "next";
import NewsLayout from "../../../components/layouts/NewsLayout";
import { dehydrate } from 'react-query/hydration'
import { QueryClient } from 'react-query'
import { fetchMenuStatic } from '../../../hooks'

const NewsPage = ({ pageData, pageNav }) => {
  const [mobile, setMobile] = useState<Boolean>()

  useEffect(() => {
    setMobile(isMobile)
  }, [setMobile])

  return (
    <>
      <NewsLayout
        pageData={pageData}
        isMobile={mobile}
      />
    </>
  );
};


export const getServerSideProps: GetServerSideProps = async () => {
  // const { API_URL } = process.env;
  const API_URL = 'https://p5s.ru';
  const response = await (await fetch(`${API_URL}/api/news/?per_page=3&page=1`)).json()
  const pageData = response ?? []
  const queryClient = new QueryClient()
  await queryClient.prefetchQuery('menuStatic', () => fetchMenuStatic())

  if (!response || response.status === 0) {
    return {
      notFound: true
    }
  }

  return {
    props: {
      pageData,
      dehydratedState: dehydrate(queryClient)
    },
  }
}

export default NewsPage;