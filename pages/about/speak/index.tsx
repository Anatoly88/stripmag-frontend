import { GetServerSideProps } from "next";
import { useEffect, useState } from "react";
import { isMobile } from "react-device-detect";
import NewsLayout from "../../../components/layouts/NewsLayout";
import { QueryClient } from 'react-query'
import { fetchMenuStatic } from '../../../hooks'
import { dehydrate } from 'react-query/hydration'

const SpeakPage = ({pageData}) => {
  const [mobile, setMobile] = useState<Boolean>()

  useEffect(() => {
    setMobile(isMobile)
  }, [setMobile])

  return (
    <>
      <NewsLayout
        pageData={pageData}
        isMobile={mobile}
      />
    </>
  )
};

export const getServerSideProps: GetServerSideProps = async () => {
  // const { API_URL } = process.env;
  const API_URL = 'https://p5s.ru';
  const res = await (await fetch(`${API_URL}/api/articles/?per_page=3&page=1`)).json()
  const pageData = res ?? []
  const queryClient = new QueryClient()
  await queryClient.prefetchQuery('menuStatic', () => fetchMenuStatic())

  return {
    props: {
      pageData,
      dehydratedState: dehydrate(queryClient)
    },
  }
}

export default SpeakPage;