import { InterfaceStaticData } from '../interfaces/staticData'
import StaticLayout from '../components/layouts/StaticLayout'
import { useEffect, useState } from 'react'
import { isMobile } from 'react-device-detect'
import { fetchMenuStatic, useMenuStatic } from '../hooks'
import { QueryClient } from 'react-query'
import { dehydrate } from 'react-query/hydration'
import { useRouter } from 'next/router'

interface InterfaceParams {
  staticUrl: string[]
}

interface InterfaceServerSideProps {
  params: InterfaceParams
}

const StaticPage = ({ pageData }) => {
  const [mobile, setMobile] = useState<Boolean>()
  const menu = useMenuStatic()
  const router = useRouter()
  const query = router.query.staticUrl[0] || []
  const currentPageNav = menu.data.filter((item) => item.code === query)
  const sidebarNav = [{ ...currentPageNav[0] }, ...currentPageNav[0].children]

  useEffect(() => {
    setMobile(isMobile)
  }, [setMobile])

  return <StaticLayout {...pageData} mobile={mobile} sidebarNav={sidebarNav} />
}

export const getServerSideProps = async ({ params }: InterfaceServerSideProps) => {
  // const { API_URL } = process.env;
  const API_URL = 'https://p5s.ru'
  const url = params.staticUrl
  const lastIndexUrl = url[url.length - 1]
  const slug = url.join('/')

  const queryClient = new QueryClient()
  await queryClient.prefetchQuery('menuStatic', () => fetchMenuStatic())
  const response = await (await fetch(`${API_URL}/api/static/?staticUrl=${slug}`)).json()

  if (!response || response.status === 0) {
    return {
      notFound: true,
    }
  }

  // if (lastIndexUrl === 'news') {
  //   return {
  //     redirect: {
  //       destination: '/news',
  //       permanent: true
  //     },
  //   }
  // }
  //
  // if (lastIndexUrl === 'speak') {
  //   return {
  //     redirect: {
  //       destination: '/speak',
  //       permanent: true
  //     },
  //   }
  // }

  const pageData: InterfaceStaticData = response

  return {
    props: {
      pageData,
      dehydratedState: dehydrate(queryClient),
    },
  }
}
export default StaticPage
