import { useQuery } from 'react-query'
import { listToTree } from '../helpers/list-to-tree'

const fetchMenu = async (type = 'catalog') => {
  const resParsed = await (await fetch(`https://p5s.ru/api/catalog/getMenu?type=${type}`)).json()
  return listToTree(resParsed.data)
}

const fetchBrandMenu = async () => {
  const resParsed = await (await fetch(`https://p5s.ru/api/catalog/getMenu?type=brand`)).json()
  return resParsed.data
}

const fetchCollectionMenu = async () => {
  const resParsed = await (await fetch(`https://p5s.ru/api/catalog/getMenu?type=collection`)).json()
  return resParsed.data
}

const fetchActionMenu = async () => {
  const resParsed = await (await fetch(`https://p5s.ru/api/catalog/getMenu?type=action`)).json()
  return resParsed.data
}

const fetchMenuStatic = async () => {
  const resParsed = await (await fetch(`https://p5s.ru/api/static-list`)).json()
  return listToTree(resParsed.pages)
}

const useMenu = (catalog) => {
  return useQuery(['menu', catalog], () => fetchMenu(catalog))
}

const useMenuStatic = () => {
  return useQuery('menuStatic', () => fetchMenuStatic(), {retry: false})
}

const useBrandMenu = () => {
  return useQuery('menuBrand', () => fetchBrandMenu())
}

const useActionMenu = () => {
  return useQuery('menuBrand', () => fetchActionMenu())
}

const useCollectionMenu = () => {
  return useQuery('menuBrand', () => fetchCollectionMenu())
}

const getBrandMenuItem = () => {
  return {
    id: 0,
    parent_id: null,
    name: 'Бренды',
    code: 'brands',
    staticUrl: '/brands/'
  }
}

export {
  useMenu,
  fetchMenu,
  useMenuStatic,
  fetchMenuStatic,
  fetchBrandMenu,
  useBrandMenu,
  fetchCollectionMenu,
  useCollectionMenu,
  useActionMenu,
  fetchActionMenu,
  getBrandMenuItem
}
