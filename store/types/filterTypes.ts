export enum FilterTypes {
  SET_FILTER = 'SET_FILTER',
  SET_FILTER_ITEM = 'SET_FILTER_ITEM',
  REMOVE_FILTER_ITEM = 'REMOVE_FILTER_ITEM',
}

interface IStateFilter {
  type: FilterTypes.SET_FILTER
}

interface ISetFilterItem {
  type: FilterTypes.SET_FILTER_ITEM
  payload: any[]
}

interface IRemoveFilterItem {
  type: FilterTypes.REMOVE_FILTER_ITEM
  payload: any[]
}

export type FilterAction = IStateFilter | ISetFilterItem | IRemoveFilterItem
