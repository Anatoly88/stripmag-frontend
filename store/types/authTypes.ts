export enum AuthTypes {
  AUTH = 'AUTH',
  AUTH_SUCCESS = 'AUTH_SUCCESS',
  AUTH_ERROR = 'AUTH_ERROR',
  AUTH_CHECK = 'AUTH_CHECK',
}

export interface AuthState {
  sessionToken?: string | null
  isAuth: boolean
  error?: null | string
}

export interface AuthsResponse {
  status: number
  message?: string
  session_token?: string
}

interface FetchAuth {
  type: AuthTypes.AUTH
}

interface FetchAuthSuccess {
  type: AuthTypes.AUTH_SUCCESS
  payload: string
}

interface FetchAuthError {
  type: AuthTypes.AUTH_ERROR
  payload: string
}

interface CheckAuth {
  type: AuthTypes.AUTH_CHECK
  payload: boolean
}

export type AuthAction = FetchAuth | FetchAuthSuccess | FetchAuthError | CheckAuth
