import { Dispatch } from 'redux'
import { FilterAction, FilterTypes } from '../types/filterTypes'

export const addFilter = (filterItem) => {
  return (dispatch: Dispatch<FilterAction>) => {
    dispatch({ type: FilterTypes.SET_FILTER_ITEM, payload: filterItem })
  }
}

export const removeFilter = (filterItem) => {
  return (dispatch: Dispatch<FilterAction>) => {
    dispatch({ type: FilterTypes.REMOVE_FILTER_ITEM, payload: filterItem })
  }
}
