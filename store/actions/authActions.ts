import { Dispatch } from 'redux'
import { AuthAction, AuthsResponse, AuthTypes } from '../types/authTypes'

export const fetchAuthToken = (email, password) => {
  return async (dispatch: Dispatch<AuthAction>) => {
    try {
      dispatch({ type: AuthTypes.AUTH })

      let formData = new FormData()
      formData.append('email', email)
      formData.append('password', password)
      await fetch('https://p5s.ru/api/auth/', {
        body: formData,
        method: 'POST',
      }).then(async (response) => {
        const res: AuthsResponse = await response.json()

        if (res && res.status === 1) {
          dispatch({ type: AuthTypes.AUTH_SUCCESS, payload: res.session_token })
        }

        if (res && res.status === 0) {
          dispatch({ type: AuthTypes.AUTH_ERROR, payload: res.message })
        }
      })
    } catch (e) {
      await dispatch({ type: AuthTypes.AUTH_ERROR, payload: 'Ошибка запроса' })
    }
  }
}

export const checkIsAuth = (token) => {
  return async (dispatch: Dispatch<AuthAction>) => {
    dispatch({ type: AuthTypes.AUTH_CHECK, payload: token && token.length > 0 })
  }
}
