import { AuthAction, AuthState, AuthTypes } from '../types/authTypes'

const initialState: AuthState = { sessionToken: '', error: null, isAuth: false }

export const authReducer = (state = initialState, action: AuthAction): AuthState => {
  switch (action.type) {
    case AuthTypes.AUTH:
      return { sessionToken: '', error: null, isAuth: false }
    case AuthTypes.AUTH_SUCCESS:
      return {
        sessionToken: action.payload,
        error: null,
        isAuth: true,
      }
    case AuthTypes.AUTH_ERROR:
      return { sessionToken: null, error: action.payload, isAuth: false }
    case AuthTypes.AUTH_CHECK:
      return { isAuth: action.payload }
    default:
      return state
  }
}
