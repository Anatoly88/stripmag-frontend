import { FilterTypes } from '../types/filterTypes'

const initialState = { filteredList: [] }

export const filterReducer = (state = initialState, action) => {
  switch (action.type) {
    case FilterTypes.SET_FILTER:
      return { filteredList: [] }
    case FilterTypes.SET_FILTER_ITEM:
      return {
        filteredList: [...state.filteredList, action.payload],
        // filteredList: [ {brand: [1,2,3]}, {material: [1,2,3]} ],
      }
    case FilterTypes.REMOVE_FILTER_ITEM:
      return {
        filteredList: state.filteredList.filter((item) => item.id !== action.payload.id),
      }
    default:
      return state
  }
}
