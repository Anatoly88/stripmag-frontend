import IconClose from '../../public/images/icons/close-modal.svg'
import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import Modal from 'styled-react-modal'
import AppButton from '../ui/AppButton'
import { Field, FieldInput, FieldLabel } from '../../pages/_app'
import { ToastContainer, toast, Slide } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { useCookies } from 'react-cookie'
import { fetchAuthToken } from '../../store/actions/authActions'
import { useDispatch } from 'react-redux'
import { useTypedSelector } from '../../hooks/useTypedSelector'

const StyledModalAuth = Modal.styled`
  position: relative;
  width: 588px;
  height: 100%;
  display: flex;
  flex-flow: column nowrap;
  align-items: flex-start;
  justify-content: flex-start;
  background-color: white;
  overflow-y: auto;
  padding: 140px 64px 100px;
  margin-left: auto;
  background-color: #fff;
  opacity: ${(props) => props.opacity};
  transition : all 0.3s ease-in-out;
`

const CloseModalBtn = styled.button`
  position: absolute;
  right: 64px;
  top: 64px;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 48px;
  height: 48px;
  border-radius: 50%;
  background-color: ${({ theme }) => theme.colors.background};
  border: none;
`

const ModalAuthTitle = styled.h2`
  font-weight: 600;
  font-size: 36px;
  line-height: 44px;
  margin-bottom: 40px;

  a {
    position: relative;
    display: inline-block;

    &:after {
      content: '';
      position: absolute;
      left: 0;
      bottom: -1.5px;
      width: 100%;
      height: 3px;
      background-color: #000;
    }
  }
`

const ModalAuthField = styled(Field)`
  margin-bottom: 40px;

  &:last-of-type {
    margin-bottom: 48px;
  }
`

const ModalAuthFooter = styled.footer`
  padding: 28px 0;
  color: ${({ theme }) => theme.colors.grayText};

  a {
    color: ${({ theme }) => theme.colors.black};
  }
`

const ModalAuthForm = styled.form`
  width: 100%;
`

const ModalAuth = ({ isShowed, setIsShowed }) => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [opacity, setOpacity] = useState(0)
  const [cookies, setCookie] = useCookies(['SessionToken'])
  const { error, sessionToken, isAuth } = useTypedSelector((state) => state.auth)
  const dispatch = useDispatch()

  useEffect(() => {
    if (sessionToken && sessionToken.length > 0 && isAuth) {
      setCookie('SessionToken', sessionToken)
      // @ts-ignore
      toggleModal()
    }

    if (error && error.length > 0 && !isAuth) {
      toast.error(error, {
        position: 'top-right',
        autoClose: 4000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: false,
        progress: undefined,
        pauseOnFocusLoss: false,
        transition: Slide,
      })
    }
  }, [isAuth, error, setCookie, sessionToken])

  function toggleModal(e) {
    setOpacity(0)
    setIsShowed(!isShowed)
  }

  function beforeOpen() {
    setTimeout(() => {
      setOpacity(1)
    }, 100)
  }

  function beforeClose() {
    return new Promise((resolve) => {
      setOpacity(0)
      setTimeout(resolve, 300)
    })
  }

  const handleChange = (event) => {
    if (event.target.name === 'email') {
      setEmail(event.target.value)
    } else {
      setPassword(event.target.value)
    }
  }

  const handlerSubmit = (event) => {
    event.preventDefault()
    dispatch(fetchAuthToken(email, password))
  }

  return (
    <>
      <StyledModalAuth
        isOpen={isShowed}
        beforeOpen={beforeOpen}
        beforeClose={beforeClose}
        onBackgroundClick={toggleModal}
        onEscapeKeydown={toggleModal}
        opacity={opacity}
        backgroundProps={{ opacity }}
      >
        <CloseModalBtn onClick={toggleModal}>
          <IconClose />
        </CloseModalBtn>
        <ModalAuthTitle>
          Войдите на сайт
          <br />
          или <a href="#">зарегистрируйтесь</a>
        </ModalAuthTitle>

        <ModalAuthForm action="" onSubmit={handlerSubmit}>
          <ModalAuthField>
            <FieldLabel>Электронная почта</FieldLabel>
            <FieldInput type="text" name="email" value={email} onChange={handleChange} />
          </ModalAuthField>

          <ModalAuthField>
            <FieldLabel>Пароль</FieldLabel>
            <FieldInput type="password" name="password" value={password} onChange={handleChange} />
          </ModalAuthField>

          <AppButton width={'228px'}>Авторизоваться</AppButton>
        </ModalAuthForm>

        <ModalAuthFooter>
          Можно <a href="#">восстановить пароль</a>, если вдруг забыли
        </ModalAuthFooter>
        <ToastContainer />
      </StyledModalAuth>
    </>
  )
}

export default ModalAuth
