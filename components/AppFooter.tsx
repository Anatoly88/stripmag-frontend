import styled from "styled-components"
import AppLink from "./ui/AppLink";
import { nav } from "../data/nav-list";
import { isMobile } from "react-device-detect";
import NavColumn from "./ui/NavColumn";
import AppContainer from "./ui/AppContainer";
import { useEffect, useState } from "react";
import { useMenuStatic } from '../hooks'

const StyledAppFooter = styled.footer`
  border-top: 1px solid ${({theme}) => theme.colors.border};
  
  @media ${({theme}) => theme.media.mobile} {
    border: none;
  }
`

const FooterNav = styled.div`
  display: grid;
  grid-template-columns: repeat(6, 1fr);
  column-gap: 48px;
  padding: 96px 0;
  
  @media ${({theme}) => theme.media.mobile} {
    display: block;
    padding: 0;
  }
`

const FooterBottom = styled.div`
  padding: 32px 0;
  color: ${({theme}) => theme.colors.grayText ?? '#000'};
  border-top: 1px solid ${({theme}) => theme.colors.border};
  
  p {
    margin: 0;

    @media ${({theme}) => theme.media.mobile} {
      font-size: 16px;
      line-height: 24px;
      margin-bottom: 8px;
    }
  }
`

const AppFooter = (props) => {
  const [mobile, setMobile] = useState<Boolean>()
  const { isLoading, error, data } = useMenuStatic()
  const navList = data

  useEffect(() => {
    setMobile(isMobile)
  }, [setMobile])

  return (
    <StyledAppFooter { ...props }>
      <AppContainer noPadding={mobile}>
        <FooterNav>
          {navList &&
            navList.map((col, index) =>
              <NavColumn
                key={`footer_nav_${index}`}
                navCol={col}
              />
            )
          }
        </FooterNav>
      </AppContainer>

      <FooterBottom>
        <AppContainer
          flex={!isMobile}
          justifyContent='space-between'
          align='center'
        >
          <p>&copy; 2021, Поставщик счастья</p>
          <AppLink url={'/politic/'} text={'Политика конфиденциальности'} />
        </AppContainer>
      </FooterBottom>
    </StyledAppFooter>
  )
};

export default AppFooter;