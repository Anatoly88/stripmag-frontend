import { ICatalogDetailPageData } from '../../interfaces/catalog'
import styled from 'styled-components'
import CatalogDetailPictures from '../catalog/CatalogDetailPictures'
import Head from 'next/head'
import CatalogNoPrice from '../catalog/CatalogNoPrice'
import AppLink from '../ui/AppLink'
import AppButton from '../ui/AppButton'
import CatalogDetailAssortment from '../catalog/CatalogDetailAssortment'
import CatalogRecommends from '../catalog/CatalogRecommends'
import React, { useEffect, useRef, useState } from 'react'
import { isMobile } from 'react-device-detect'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import IconShield from '../../public/images/icons/shield_green.svg'
import ModalAuth from '../modals/ModalAuth'
import {getBrandMenuItem} from "../../hooks";

const DetailWrapper = styled.section`
  padding-top: 148px;
  min-height: 100vh;

  @media ${({ theme }) => theme.media.mobile} {
    padding-top: 32px;
  }
`

const DetailProduct = styled.div`
  position: relative;
  display: grid;
  grid-template-columns: 816px 384px;
  gap: 0 48px;
  align-items: center;
  margin-bottom: 72px;

  @media ${({ theme }) => theme.media.mobile} {
    display: block;
    margin-bottom: 60px;
  }
`

const DetailProductInfo = styled.div`
  padding-top: ${({ paddingTop }) => paddingTop ?? '60px'};

  @media ${({ theme }) => theme.media.mobile} {
    padding-top: 40px;
  }
`

const DetailContent = styled.div`
  position: relative;
  z-index: 1;
`

const DetailTitle = styled.h1`
  position: absolute;
  right: 0;
  top: 0;
  z-index: 1;
  font-weight: 600;
  font-size: 56px;
  line-height: 64px;
  margin-bottom: 0;
  margin-left: auto;
  max-width: 600px;
  transform: translateY(-50%);

  @media ${({ theme }) => theme.media.mobile} {
    position: relative;
    right: auto;
    font-size: 32px;
    line-height: 40px;
    transform: none;
    margin-bottom: 36px;
  }
`

const InfoList = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0 0 40px;
`

const InfoItem = styled.li`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  font-size: 16px;
  line-height: 24px;
  color: ${({ theme }) => theme.colors.grayText};

  &:not(:last-of-type) {
    margin-bottom: 12px;
  }
`

const InfoName = styled.span`
  width: 168px;
`

const InfoValue = styled.span`
  padding-left: 0;
`

const InfoLink = styled(AppLink)`
  padding-left: 0;
  font-size: 16px;
  line-height: 24px;
`

const DetailBtn = styled(AppButton)`
  margin-top: 40px;

  @media ${({ theme }) => theme.media.mobile} {
    margin-top: 36px;
  }
`

const DetailDescriptions = styled.div`
  max-width: 600px;
  margin-left: auto;
  margin-bottom: 96px;

  @media ${({ theme }) => theme.media.mobile} {
    margin-bottom: 60px;
  }
`

const DetailDescriptionsTitle = styled.h3`
  font-weight: 600;
  font-size: 28px;
  line-height: 38px;
  margin-bottom: 40px;

  @media ${({ theme }) => theme.media.mobile} {
    font-size: 24px;
    line-height: 34px;
    margin-bottom: 36px;
  }
`

const DetailDescription = styled.article`
  padding: 27px 0 28px;
  border-bottom: 1px solid ${({ theme }) => theme.colors.border};

  &:first-of-type {
    padding-top: 0;
  }

  &:last-of-type {
    border-bottom: none;
  }

  h5 {
    margin-bottom: 12px;
    font-weight: normal;
    font-size: 16px;
    line-height: 24px;
    color: ${({ theme }) => theme.colors.grayText};

    @media ${({ theme }) => theme.media.mobile} {
      margin-bottom: 8px;
    }
  }

  p {
    font-size: 18px;
    line-height: 28px;
    margin-bottom: 0;

    &:first-letter {
      text-transform: capitalize;
    }
  }

  @media ${({ theme }) => theme.media.mobile} {
    padding: 23px 0 24px;
  }
`

const DetailPrice = styled.div`
  display: flex;
  align-items: flex-end;

  h5 {
    font-weight: 600;
    font-size: 36px;
    line-height: 44px;
    margin: 0;
  }

  p {
    color: ${({ theme }) => theme.colors.grayText};
    margin-bottom: 4px;
    margin-left: 16px;
    font-size: 18px;
    line-height: 18px;
    text-decoration-line: line-through;

    svg {
      margin-left: 16px;
      margin-bottom: 2px;
    }
  }
`
const DetailPriceInfo = styled.ul`
  display: flex;
  list-style: none;
  padding: 8px 0 0;
  margin: 0;
  color: ${({ theme }) => theme.colors.grayText};

  li {
    &:last-of-type {
      position: relative;
      margin-left: 36px;

      &:before {
        content: '';
        position: absolute;
        top: 50%;
        left: -20px;
        width: 4px;
        height: 4px;
        border-radius: 50%;
        background-color: ${({ theme }) => theme.colors.grayText};
        transform: translateY(-50%);
      }
    }
  }
`

const CatalogDetailLayout = ({ data }) => {
  const pageData: ICatalogDetailPageData = data
  const titleRef = useRef()
  const [titleHeight, setTitleHeight] = useState()
  const [showModal, setShowModal] = useState(false)
  const [mobile, setMobile] = useState(false)
  const modelYearString = pageData.model_year ? `, ${pageData.model_year}` : ''
  const { isAuth } = useTypedSelector((state) => state.auth)
  const bonuses = '184'

  useEffect(() => {
    setMobile(isMobile)
    // @ts-ignore
    setTitleHeight(titleRef.current.clientHeight)
  }, [titleRef, setTitleHeight, setMobile])

  const handleButton = () => {
    if (!isAuth) {
      setShowModal(!showModal)
    }
  }
  console.log(pageData)
  return (
    <>
      <Head>
        <title>{pageData.name}</title>
      </Head>

      <DetailWrapper>
        <DetailProduct>
          <DetailTitle dangerouslySetInnerHTML={{ __html: pageData.name }} ref={titleRef} />

          <CatalogDetailPictures list={pageData?.images} isAuth={isAuth} />
          {/*// @ts-ignore*/}
          <DetailProductInfo paddingTop={titleHeight > 192 ? '160px' : '60px'}>
            {!mobile && (
              <InfoList>
                {pageData.prodcode && (
                  <InfoItem>
                    <InfoName>Артикул</InfoName>
                    <InfoValue>{pageData.prodcode}</InfoValue>
                  </InfoItem>
                )}

                {/* TODO выяснить где брать эти данные */}
                {/*<InfoItem>*/}
                {/*  <InfoName>Модель (prodID)</InfoName>*/}
                {/*  <InfoValue>1288749</InfoValue>*/}
                {/*</InfoItem>*/}
                {/*<InfoItem>*/}
                {/*  <InfoName>Номер (aID)</InfoName>*/}
                {/*  <InfoValue>22233322</InfoValue>*/}
                {/*</InfoItem>*/}
                {/*<InfoItem>*/}
                {/*  <InfoName>Штрихкод</InfoName>*/}
                {/*  <InfoValue>02001293012390</InfoValue>*/}
                {/*</InfoItem>*/}

                {pageData.brand_name && (
                  <InfoItem>
                    <InfoName>Бренд</InfoName>
                    <InfoLink url={getBrandMenuItem().staticUrl} text={pageData.brand_name} color={'#000'} />
                  </InfoItem>
                )}

                {pageData.collection && (
                  <InfoItem>
                    <InfoName>Коллекция, год</InfoName>
                    <InfoLink url={`/collections/`} text={`${pageData.collection}${modelYearString}`} color={'#000'} />
                  </InfoItem>
                )}
              </InfoList>
            )}

            {isAuth && (
              <div>
                <DetailPrice>
                  <h5>{`${pageData?.price} ₽`}</h5>
                  {pageData?.old_price && (
                    <p>
                      {`${pageData?.old_price} ₽`}
                      <IconShield />
                    </p>
                  )}
                </DetailPrice>
                <DetailPriceInfo>
                  <li>{`Розница —  ${pageData?.old_price} ₽`}</li>
                  <li>{`Бонусы —  ${pageData?.bonuses || bonuses} ₽`}</li>
                </DetailPriceInfo>
              </div>
            )}
            {!isAuth && <CatalogNoPrice large descriptionText="Только для авторизованных клиентов" />}

            <DetailBtn onClick={handleButton} width={'100%'}>
              {isAuth ? 'Добавить в корзину' : 'Войти на сайт'}
            </DetailBtn>
          </DetailProductInfo>
        </DetailProduct>

        <CatalogDetailAssortment list={pageData.assortment} isMob={mobile} />

        <DetailDescriptions>
          <DetailDescriptionsTitle>Подробности</DetailDescriptionsTitle>
          {pageData.hometext && (
            <DetailDescription>
              <h5>Описание</h5>
              <p dangerouslySetInnerHTML={{ __html: pageData.hometext }} />
            </DetailDescription>
          )}

          {pageData.bodytext && (
            <DetailDescription>
              <h5>Комплект</h5>
              <p>{pageData.bodytext}</p>
            </DetailDescription>
          )}

          {pageData.material && (
            <DetailDescription>
              <h5>Материал</h5>
              <p>{pageData.material}</p>
            </DetailDescription>
          )}

          {pageData.pack_name && (
            <DetailDescription>
              <h5>Упаковка</h5>
              <p>{pageData.pack_name}</p>
            </DetailDescription>
          )}

          {pageData.function_name && (
            <DetailDescription>
              <h5>Назначение</h5>
              <p>{pageData.function_name}</p>
            </DetailDescription>
          )}
        </DetailDescriptions>
      </DetailWrapper>
      {/*<CatalogRecommends />*/}
      <ModalAuth isShowed={showModal} setIsShowed={setShowModal} />
    </>
  )
}

export default CatalogDetailLayout
