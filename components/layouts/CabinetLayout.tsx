import Head from 'next/head'
import AppHeader from '../AppHeader'
import AppFooter from '../AppFooter'
import AppContainer from '../ui/AppContainer'
import Breadcrumbs from '../ui/Breadcrumbs'
import StaticSidebar from '../ui/StaticSidebar'
import styled from 'styled-components'
import { isMobile } from 'react-device-detect'

const CabinetWrapper = styled.div`
  display: grid;
  grid-template-columns: 276px 816px;
  column-gap: 156px;
  padding-top: 52px;

  @media ${({ theme }) => theme.media.mobile} {
    display: block;
    padding-top: 32px;
  }
`

const CabinetContent = styled.div`
  max-width: 600px;
`

const CabinetTitle = styled.h1`
  font-weight: 600;
  font-size: 56px;
  line-height: 64px;
  margin-bottom: 60px;
`

const navList = [
  {
    name: 'Профиль',
    staticUrl: '/cabinet/profile',
  },
  {
    name: 'Заказы',
    staticUrl: '/cabinet/orders',
  },
  {
    name: 'Баланс',
    staticUrl: '/cabinet/balance',
  },
  {
    name: 'Аналитика',
    staticUrl: '/cabinet/analytics',
  },
  {
    name: 'Выгрузки',
    staticUrl: '/cabinet/unloads',
  },
  {
    name: 'Документы',
    staticUrl: '/cabinet/documents',
  },
  {
    name: 'Поддержка',
    staticUrl: '/cabinet/support',
  },
  {
    name: 'База знаний',
    staticUrl: '/cabinet/faq',
  },
  {
    name: 'Настройки',
    staticUrl: '/cabinet/settings',
  },
]

const CabinetLayout = ({ children, title = 'Общая информация' }) => {
  return (
    <>
      <Head>
        <title>{`Личный кабинет | ${title}`}</title>
      </Head>

      <AppHeader />

      <main className={'wrapper'}>
        <AppContainer>
          <Breadcrumbs
            navList={[
              { name: 'Сотрудничество', staticUrl: '/about/' },
              { name: 'Оптовые покупки', staticUrl: '/about/mission' },
            ]}
          />
          <CabinetWrapper>
            <StaticSidebar nav={navList} active={title} />
            <CabinetContent>
              <CabinetTitle>{title}</CabinetTitle>
              {children}
            </CabinetContent>
          </CabinetWrapper>
        </AppContainer>
      </main>

      <AppFooter isMobile={isMobile} />
    </>
  )
}

export default CabinetLayout
