import Head from 'next/head'
import AppHeader from "../AppHeader"
import AppFooter from "../AppFooter";

const defaultTitle = 'Поставщик счастья - оптовый поставщик интим-товаров для сексшопа. Дропшиппинг поставщик для интернет-магазинов.'

const MainLayout = ({
                      children,
                      title = defaultTitle,
                      withoutFooter = false,
                      noPadding = false
}) => {

  return (
    <>
      <Head>
        <title>{title}</title>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin="anonymous"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Inter:wght@400;600&display=swap"
          rel="stylesheet"
        />
        <link
          rel="stylesheet"
          type="text/css"
          charSet="UTF-8"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"
        />
        <link
          rel="stylesheet"
          type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"
        />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>

      <AppHeader/>

      <main className={'wrapper'} style={ noPadding ? {padding: 0} : {}}>
        {children}
      </main>

      {!withoutFooter &&
        <AppFooter />
      }
    </>
  )
}

export default MainLayout