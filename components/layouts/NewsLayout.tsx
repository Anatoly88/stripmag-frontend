import Head from 'next/head'
import AppHeader from "../AppHeader"
import AppFooter from "../AppFooter";
import AppContainer from "../ui/AppContainer";
import Breadcrumbs from "../ui/Breadcrumbs";
import StaticSidebar from "../ui/StaticSidebar";
import styled from "styled-components";
import { useEffect, useState } from "react";
import { INew } from "../../interfaces/new";
import NewCard from "../news/NewCard";
import FlexContainer from "../ui/FlexContainer";
import { useRouter } from "next/router";
import { useMenuStatic } from '../../hooks'

const PageWrapper = styled.div`
  display: grid;
  grid-template-columns: 276px 816px;
  column-gap: 156px;
  padding-top: 52px;
  padding-bottom: 42px;
  
  @media ${({theme}) => theme.media.mobile} {
    display: block;
    padding-top: 32px;
    padding-bottom: 20px;
  }
`

const PageContent = styled.section`
  font-size: 18px;

  @media ${({theme}) => theme.media.mobile} {
    padding-top: 68px;
  }
`

const PageTitle = styled.h1`
  font-style: normal;
  font-weight: 600;
  font-size: 56px;
  line-height: 64px;
  margin-bottom: 76px;
  
  @media ${({theme}) => theme.media.mobile} {
    font-size: 44px;
    line-height: 52px;
    margin-bottom: 26px;
  }
`

const Item = styled(NewCard)`
  margin-bottom: 48px;
  
  @media ${({theme}) => theme.media.mobile} {
    margin-bottom: 28px;
  }
`


const NewsLayout = ({ pageData, isMobile }) => {
  const router = useRouter()
  const menu = useMenuStatic()
  const currentPageNav = menu.data.filter(item => item.code === 'about')
  const sidebarNav = [ { ...currentPageNav[0] }, ...currentPageNav[0].children ]
  const pathName = router.pathname
  const isSpeakPage =  pathName.includes('/speak')
  const collection: Array<INew> = pageData?.items
  const name = pageData?.name
  const content = pageData?.content
  const activeSidebarName = isSpeakPage ? 'О нас говорят' : 'Новости'

  return (
    <>
      <Head>
        <title>{name}</title>
      </Head>

      <AppHeader>
      </AppHeader>

      <main className={'wrapper'}>
        <AppContainer>
          <Breadcrumbs navList={[{name: 'Сотрудничество', staticUrl: '/about/'}, {name: 'Оптовые покупки', staticUrl: '/about/mission'}]}/>

          <PageWrapper>
            {isMobile &&
              <PageTitle>
                {name}
              </PageTitle>
            }

            <StaticSidebar nav={sidebarNav} active={activeSidebarName}/>

            <PageContent>
              {!isMobile &&
                <PageTitle>
                  {name}
                </PageTitle>
              }

              {content &&
                <div dangerouslySetInnerHTML={{__html: content}} />
              }

              {!content && collection &&
                <>
                <FlexContainer
                  flow={isMobile ? 'column nowrap' : 'row wrap'}
                  justify={'space-between'}
                >

                  {!collection &&
                    <p>Здесь пока ничего нет</p>
                  }

                  {collection &&
                    collection.map((item, index) =>
                      <>
                        <Item
                          key={ `news_item_${ index }` }
                          createdAt={ item?.createdAt }
                          name={ item?.name }
                          image={ item?.image }
                          slug={ item?.slug }
                          author={ item?.author }
                          withAuthor={ !!item.author }
                          isSpeakArticle={ isSpeakPage }
                        />
                      </>
                    )
                  }
                </FlexContainer>
                </>
              }

            </PageContent>
          </PageWrapper>
        </AppContainer>
      </main>

      <AppFooter isMobile={isMobile} />
    </>
  )
}

export default NewsLayout