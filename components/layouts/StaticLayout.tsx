import Head from 'next/head'
import AppHeader from '../AppHeader'
import AppFooter from '../AppFooter'
import AppContainer from '../ui/AppContainer'
import Breadcrumbs from '../ui/Breadcrumbs'
import StaticSidebar from '../ui/StaticSidebar'
import styled from 'styled-components'
import { useEffect, useState } from 'react'
import { InterfaceStaticData } from '../../interfaces/staticData'

const PageWrapper = styled.div`
  display: grid;
  grid-template-columns: 276px 816px;
  column-gap: 156px;
  padding-top: 52px;

  @media ${({ theme }) => theme.media.mobile} {
    display: block;
    padding-top: 32px;
  }
`

const PageContent = styled.section`
  font-size: 18px;

  h2,
  h3,
  h4,
  h5,
  h6 {
    font-size: 28px;
    line-height: 38px;
    font-weight: 600;
    margin-bottom: 40px;
    margin-top: 96px;

    &:first-child {
      margin-top: 0;
    }

    @media ${({ theme }) => theme.media.mobile} {
      font-size: 24px;
      line-height: 34px;
      margin-bottom: 36px;
      margin-top: 60px;
    }
  }

  p {
    font-size: 18px;
    line-height: 28px;
    margin-bottom: 30px;

    @media ${({ theme }) => theme.media.mobile} {
      margin-bottom: 28px;
    }
  }

  ul {
    list-style: none;
    padding: 0;
    margin: 0;

    li {
      position: relative;
      margin-bottom: 28px;

      &:before {
        content: '\\2014';
        position: absolute;
        left: -50px;
        top: 0;
        font-size: 18px;
        line-height: 28px;
      }
    }
  }

  img {
    width: 100%;
    height: 460px;
    border-radius: 8px;
    object-fit: cover;
    margin: 60px auto;
  }

  strong,
  b {
    font-weight: 600;
  }

  @media ${({ theme }) => theme.media.mobile} {
    padding-top: 60px;
  }
`

const PageTitle = styled.h1`
  font-style: normal;
  font-weight: 600;
  font-size: 56px;
  line-height: 64px;
  margin-bottom: 60px;

  @media ${({ theme }) => theme.media.mobile} {
    font-size: 44px;
    line-height: 52px;
    margin-bottom: 26px;
  }
`

const StaticLayout = ({
  title,
  description,
  keywords,
  name,
  menu,
  content,
  sidebarNav,
  mobile,
}: InterfaceStaticData) => {
  const [nav, setNav] = useState([])

  useEffect(() => {
    setNav(sidebarNav)
  }, [sidebarNav])

  return (
    <>
      <Head>
        <title>{title || 'Поставщик счастья'}</title>
        <meta name="description" content={`${description}`} />
        <meta name="keywords" content={`${keywords}`} />
      </Head>

      <AppHeader />

      <main className={'wrapper'}>
        <AppContainer>
          <Breadcrumbs
            navList={[
              { name: 'Сотрудничество', staticUrl: '/about/' },
              { name: 'Оптовые покупки', staticUrl: '/about/mission' },
            ]}
          />
          <PageWrapper>
            {mobile && <PageTitle>{name}</PageTitle>}
            <StaticSidebar nav={nav} active={menu} />
            <PageContent>
              {!mobile && <PageTitle>{name}</PageTitle>}

              {!content && <p>Здесь пока ничего нет</p>}

              <div dangerouslySetInnerHTML={{ __html: content }} />
            </PageContent>
          </PageWrapper>
        </AppContainer>
      </main>

      <AppFooter isMobile={mobile} />
    </>
  )
}

export default StaticLayout
