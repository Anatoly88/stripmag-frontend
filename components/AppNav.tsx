import styled from 'styled-components'
import AppContainer from './ui/AppContainer'
import IconArrowNav from '../public/images/icons/arrow_nav.svg'
import { useEffect, useState } from 'react'
import NavColumn from './ui/NavColumn'
import { hideScrollbar } from '../styles/mixins'
import { nav } from '../data/nav-list'
import NavCatalogLink from './ui/NavCatalogLink'

const StyledAppNav = styled.nav`
  position: absolute;
  left: 0;
  right: 0;
  top: ${({ theme }) => theme.headerHeight};
  z-index: 8;
  padding: 44px 0 50px;
  background-color: ${({ theme }) => theme.colors.background};
  border-bottom: 1px solid ${({ theme }) => theme.colors.background};
  width: 100vw;
  height: calc(100vh - 120px);
  transition: left 0.4s;
  overflow-y: auto;
  ${hideScrollbar}

  .nav-top {
    display: flex;
    flex-flow: row nowrap;
    height: 60vh;
  }

  .nav-bottom {
    display: grid;
    grid-template-columns: repeat(6, 1fr);
    column-gap: 48px;
    padding-top: 72px;

    @media ${({ theme }) => theme.media.mobile} {
      display: block;
    }
  }

  @media ${({ theme }) => theme.media.mobile} {
    top: ${({ theme }) => theme.headerHeightMob};
  }
`

const Menu = styled.ul`
  background-color: transparent;
  width: 276px;
  margin: 0;
  padding: 0;
  max-height: 100%;
  overflow-y: auto;
  list-style: none;
  ${hideScrollbar}

  &:not(:first-of-type) {
    width: calc(276px + 48px);
    padding-left: 48px;
  }
`

const MenuPages = styled(Menu)`
  && {
    width: 600px;
  }
`

const MenuItem = styled.li`
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  height: 60px;
  box-shadow: none;
  font-size: 0;
  background-color: transparent;
`

const MenuLink = styled(NavCatalogLink)`
  position: relative;
  width: 100%;
  font-size: 18px;
  line-height: 28px;
  background-color: transparent;
  white-space: nowrap;
  overflow: hidden;
  padding-right: 16px;
  text-overflow: ellipsis;

  &:hover,
  &:focus {
    svg {
      stroke: ${({ theme }) => theme.colors.accent ?? '#000'};
      transition: stroke 0.3s;
    }
  }

  svg {
    position: absolute;
    right: 6px;
    top: 50%;
    transform: translateY(-50%);
    stroke: ${({ theme }) => theme.colors.lightGray ?? '#000'};
    transition: stroke 0.3s;
    margin-left: 6px;
  }
`

const AppNavContainer = styled(AppContainer)`
  height: auto;
`

const AppNav = ({ isShowed = false, ...props }) => {
  const [showNav, setShowNav] = useState(isShowed)
  const [navCategories, setNavCategories] = useState([])
  const [navPages, setNavPages] = useState([])
  const [categoriesIsShow, setShowCategories] = useState(false)
  const [pagesIsShow, setShowPages] = useState(false)
  const catalogList = props.nav
  const staticList = props.staticNav
  const { handleHamburgerToggle } = props

  useEffect(() => {
    setShowNav(isShowed)
  }, [setShowNav, isShowed])

  return (
    <StyledAppNav {...props} style={{ left: showNav ? 0 : '-100%' }}>
      <AppNavContainer>
        <div
          className="nav-top"
          onMouseLeave={() => {
            setShowPages(false)
            setShowCategories(false)
          }}
        >
          <Menu
            onMouseLeave={() => {
              setShowCategories(false)
              setShowPages(false)
            }}
          >
            {catalogList &&
              catalogList.length > 0 &&
              catalogList.map((item, index) => (
                <MenuItem onMouseLeave={() => setShowCategories(false)} key={`catalog_nav_${index}`}>
                  <MenuLink
                    url={`/catalog${item.slug}`}
                    text={item.name}
                    onClick={handleHamburgerToggle}
                    onMouseEnter={() => {
                      setNavCategories(item.children)
                      setShowCategories(true)
                    }}
                  >
                    <IconArrowNav />
                  </MenuLink>
                </MenuItem>
              ))}
          </Menu>

          <Menu
            style={{ display: categoriesIsShow ? 'block' : 'none' }}
            onMouseEnter={() => setShowCategories(true)}
            onMouseLeave={() => setShowPages(false)}
          >
            {navCategories &&
              navCategories.map((item, index) => (
                <MenuItem key={`submenu_categories_${index}`}>
                  <MenuLink
                    url={`/catalog${item.slug}`}
                    text={item.name}
                    onClick={handleHamburgerToggle}
                    onMouseEnter={() => {
                      setNavPages(item.children)
                      setShowPages(true)
                    }}
                  >
                    <IconArrowNav />
                  </MenuLink>
                </MenuItem>
              ))}
          </Menu>

          <MenuPages
            style={{ display: pagesIsShow ? 'block' : 'none' }}
            onMouseEnter={() => {
              setShowPages(true)
              setShowCategories(true)
            }}
            onMouseLeave={() => setShowPages(false)}
          >
            {navPages &&
              navPages.map((item, index) => (
                <MenuItem key={`submenu_pages_${index}`}>
                  <MenuLink url={`/catalog${item.slug}`} text={item.name} onClick={handleHamburgerToggle} />
                </MenuItem>
              ))}
          </MenuPages>
        </div>

        <div className="nav-bottom" onClick={handleHamburgerToggle}>
          {staticList &&
            staticList.map((col, index) => (
              <NavColumn key={`nav_bottom_${index}`} navCol={col} handleMenuToggle={handleHamburgerToggle} />
            ))}
        </div>
      </AppNavContainer>
    </StyledAppNav>
  )
}

export default AppNav
