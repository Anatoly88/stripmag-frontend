import styled from 'styled-components'

const CabinetBalance = styled.div`
  padding: 32px 32px 0;
  width: 100%;
  background-color: #e5e5e5;
  border-radius: ${({ theme }) => theme.radius};
`

const CabinetBalanceMoney = styled.h3`
  font-weight: 600;
  font-size: 36px;
  line-height: 44px;
  margin-bottom: 12px;
`

const CabinetBalanceFooter = styled.footer`
  display: flex;
`

const CabinetBalanceBtn = styled.button`
  background-color: transparent;
  border-radius: 0;
  border: none;
  padding: 24px 0;
  font-weight: 600;
  font-size: 18px;
  line-height: 28px;

  &:last-of-type {
    position: relative;
    margin-left: 36px;

    &:before {
      content: '';
      position: absolute;
      left: -20px;
      top: 50%;
      transform: translateY(-50%);
      border-radius: 50%;
      width: 4px;
      height: 4px;
      background-color: #000;
    }
  }

  @media ${({ theme }) => theme.media.desktop} {
    &:hover {
      span {
        display: block;
        transform: scale(1.05);
        transition: transform 0.2s;
      }
    }
  }
`

const CabinetBalanceInfo = styled.div`
  color: ${({ theme }) => theme.colors.grayText};
  padding-bottom: 36px;
  border-bottom: 1px solid ${({ theme }) => theme.grayText};

  ul {
    display: flex;
    list-style: none;
    padding: 0;
    margin: 0;

    li {
      &:last-of-type {
        position: relative;
        margin-left: 36px;

        &:before {
          content: '';
          position: absolute;
          left: -18px;
          top: 50%;
          transform: translateY(-50%);
          width: 4px;
          height: 4px;
          background-color: ${({ theme }) => theme.colors.grayText};
          border-radius: 50%;
        }
      }
    }
  }

  p {
    margin-top: 12px;
    margin-bottom: 0;
  }
`

const BalanceInfo = ({ moneyNow = 0, moneyReserv = 0, discount = 0 }) => {
  return (
    <>
      <CabinetBalance>
        <CabinetBalanceMoney>{`${moneyNow} ₽`}</CabinetBalanceMoney>
        <CabinetBalanceInfo>
          <ul>
            <li>{`Резерв — ${moneyReserv} ₽`}</li>
            <li>{`Скидка — ${discount}%`}</li>
          </ul>
          <p>До скидки 10% осталось отгрузить на 128 775 ₽</p>
        </CabinetBalanceInfo>
        <CabinetBalanceFooter>
          <CabinetBalanceBtn>
            <span>Пополнить</span>
          </CabinetBalanceBtn>
          <CabinetBalanceBtn>
            <span>Вывести</span>
          </CabinetBalanceBtn>
        </CabinetBalanceFooter>
      </CabinetBalance>
    </>
  )
}

export default BalanceInfo
