import Image from 'next/image'
import Link from 'next/link'
import styled from 'styled-components'
import React, { useEffect, useState } from 'react'
import AppNav from './AppNav'
import AppContainer from './ui/AppContainer'
import { isDesktop, isMobile } from 'react-device-detect'
import AppNavMob from './AppNavMob'
import { QueryClient, QueryClientProvider, useQuery } from 'react-query'
import { useMenu, useMenuStatic } from '../hooks'
import ModalAuth from './modals/ModalAuth'
import { useTypedSelector } from '../hooks/useTypedSelector'
import { useRouter } from 'next/router'

const StyledAppHeader = styled.header`
  position: relative;
  left: 0;
  top: 0;
  right: 0;
  z-index: 8;
  height: ${({ theme }) => theme.headerHeight};
  padding: 48px 0;
  width: 100%;
  border-bottom: 1px solid ${({ theme }) => theme.colors.border};
  background-color: ${({ theme }) => theme.colors.background};

  @media ${({ theme }) => theme.media.mobile} {
    padding: 30px 0;
    height: ${({ theme }) => theme.headerHeightMob};
  }
`

const HeaderMenuBtn = styled.button`
  position: relative;
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-between;
  align-items: center;
  width: 24px;
  height: 24px;
  padding: 4px 3px;
  border-radius: 0;
  border: none;
  cursor: pointer;
  background-color: transparent;

  .line {
    position: relative;
    display: block;
    width: 100%;
    height: 2px;
    background: #000;
    border: none;
    border-radius: var(--border-radius);
    transition: transform 0.4s, top 0.3s;
  }

  &.isOpen {
    padding: 0;

    .line {
      border-radius: var(--border-radius);
      transition: transform 0.4s, top 0.3s;
    }

    .line_top {
      top: 10px;
      transform: rotate(135deg);
    }

    .line_middle {
      top: -1px;
      transform: rotate(-135deg);
    }

    .line_bottom {
      opacity: 0;
      transform: rotate(180deg);
    }
  }
`

const HeaderNav = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  flex-flow: row nowrap;

  button {
    padding: 0;
    font-size: 0;
    background-color: transparent;
    border: none;
    outline: none;

    &:not(:first-of-type) {
      margin-left: 36px;

      @media ${({ theme }) => theme.media.mobile} {
        margin-left: 24px;
        margin-right: 0;
      }
    }

    &:first-of-type {
      @media ${({ theme }) => theme.media.mobile} {
        margin-left: 24px;
        margin-right: auto;
      }
    }
  }

  @media ${({ theme }) => theme.media.mobile} {
    width: calc(100% - 24px);
  }
`

const HeaderLogo = styled.a`
  display: block;
  position: absolute;
  left: 50%;
  transform: translateX(-50%);
  width: 36px;
  height: 60px;

  @media ${({ theme }) => theme.media.mobile} {
    width: 26px;
    height: 44px;
  }
`

const AppHeader = (props) => {
  const [navOpen, setNavOpened] = useState(false)
  const [showModal, setShowModal] = useState(false)
  const { isAuth } = useTypedSelector((state) => state.auth)
  const queryClient = new QueryClient()
  const router = useRouter()

  const catalogMenu = useMenu('catalog')

  if (catalogMenu.error) console.error(`${catalogMenu.error}`)

  const nav = catalogMenu.data || []

  const staticMenu = useMenuStatic()

  const staticNav = staticMenu.data || []

  const handleToggleMenu = () => {
    document.body.style.overflow = null

    if (!navOpen) {
      setNavOpened(true)
      document.body.style.overflow = 'hidden'
    } else {
      setNavOpened(false)
    }
  }

  const handlerAuth = () => {
    if (isAuth) {
      router.push('/cabinet')
    } else {
      setShowModal(!showModal)
    }
  }

  return (
    <StyledAppHeader {...props}>
      <AppContainer flex justifyContent="space-between" align="center">
        <HeaderMenuBtn onClick={handleToggleMenu} className={navOpen ? 'isOpen' : ''}>
          <span className="line line_top" />
          <span className="line line_middle" />
          <span className="line line_bottom" />
        </HeaderMenuBtn>

        <Link href="/" passHref>
          <HeaderLogo>
            <Image src="/images/logo.svg" alt="Поставщик счастья" layout="fill" objectFit="contain" />
          </HeaderLogo>
        </Link>

        <HeaderNav>
          <button>
            <Image src="/images/icons/search.svg" alt="search" width={24} height={24} />
          </button>

          <button onClick={handlerAuth}>
            <Image src="/images/icons/user.svg" alt="user" width={24} height={24} />
          </button>

          <button>
            <Image src="/images/icons/basket.svg" alt="basket" width={24} height={24} />
          </button>
        </HeaderNav>
      </AppContainer>

      {isDesktop && (
        <QueryClientProvider client={queryClient}>
          <AppNav isShowed={navOpen} nav={nav} staticNav={staticNav} handleHamburgerToggle={handleToggleMenu} />
        </QueryClientProvider>
      )}

      {isMobile && (
        <QueryClientProvider client={queryClient}>
          <AppNavMob isShowed={navOpen} nav={nav} staticNav={staticNav} handleHamburgerToggle={handleToggleMenu} />
        </QueryClientProvider>
      )}

      <ModalAuth isShowed={showModal} setIsShowed={setShowModal} />
    </StyledAppHeader>
  )
}

export default AppHeader
