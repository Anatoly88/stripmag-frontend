import styled from 'styled-components'
import AppLink from '../ui/AppLink'
import ArrowIcon from '../../public/images/icons/arrow_right.svg'
import { lineClamp } from '../../styles/mixins'
import Link from 'next/link'
import AppReadMore from '../ui/AppReadMore'

const StyledRatingCard = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-between;
  padding: 32px;
  width: 384px;
  height: 276px;
  border: 2px solid ${({ theme }) => theme.colors.black};
  border-radius: ${({ theme }) => theme.radius};

  @media ${({ theme }) => theme.media.mobile} {
    width: 100%;
    max-width: 576px;
    height: 240px;
    padding: 24px 24px 20px;

    &:not(:last-of-type) {
      margin-bottom: 28px;
    }
  }
`

const RatingTitle = styled.h4`
  font-weight: 600;
  font-size: 28px;
  line-height: 38px;
  margin: 0;
  overflow: hidden;
  max-height: 152px;

  &:first-letter {
    text-transform: capitalize;
  }

  @media ${({ theme }) => theme.media.mobile} {
    font-size: 24px;
    line-height: 34px;
  }
`

const RatingCard = ({ title, url, id }) => {
  return (
    <StyledRatingCard>
      <RatingTitle>{title}</RatingTitle>
      <Link href={`${url}?id=${id}`} passHref>
        <AppReadMore />
      </Link>
    </StyledRatingCard>
  )
}

export default RatingCard
