import styled from 'styled-components'
import RatingCard from './RatingCard'

const StyledRatingsList = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  gap: 48px;

  @media ${({ theme }) => theme.media.mobile} {
    display: flex;
    flex-flow: column nowrap;
    gap: 0;
  }
`

const RatingsList = ({ list }) => {
  const showCards = list && list.length > 0

  return (
    <StyledRatingsList>
      {showCards &&
        list.map((item, index) => (
          <RatingCard key={`rating_list_item_${index}`} title={item.name} url={`/ratings/${item.code}`} id={item.id} />
        ))}
    </StyledRatingsList>
  )
}

export default RatingsList
