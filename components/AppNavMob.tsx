import styled from 'styled-components'
import { hideScrollbar } from '../styles/mixins'
import DropdownCol from './ui/DropdownCol'
import AppContainer from './ui/AppContainer'
import { nav } from '../data/nav-list'

const StyledAppNavMob = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  top: ${({ theme }) => theme.headerHeight};
  z-index: 1;
  padding: 40px 0;
  background-color: ${({ theme }) => theme.colors.background};
  width: 100vw;
  height: calc(100vh - ${({ theme }) => theme.headerHeight});
  transition: left 0.4s;
  overflow-y: auto;
  ${hideScrollbar}

  @media ${({ theme }) => theme.media.mobile} {
    height: calc(100vh - ${({ theme }) => theme.headerHeightMob});
    top: ${({ theme }) => theme.headerHeightMob};
  }
`

const StyledNavMobContainer = styled(AppContainer)`
  overflow-y: auto;
  height: 92%;
`

const AppNavMob = ({ isShowed = false, ...props }) => {
  const catalog = props.nav
  const sections = nav.sections

  return (
    <StyledAppNavMob {...props} style={{ left: isShowed ? 0 : '-100%' }}>
      <StyledNavMobContainer>
        <DropdownCol name={'Каталог'} list={catalog} />
        {sections.map((list, index) => (
          <DropdownCol name={list.name} list={list.pages} key={`nav_mob_col_${index}`} />
        ))}
      </StyledNavMobContainer>
    </StyledAppNavMob>
  )
}

export default AppNavMob
