import styled, { css } from "styled-components"
import Link from "next/link"
import Image from "next/image"
import { INew } from "../../interfaces/new"
import Moment from 'react-moment'
import 'moment/locale/ru'

const StyledNewCard = styled.div`
  width: 100%;
  max-width: 384px;
  background-color: ${({theme}) => theme.colors.background};
`

const LinkWrapper = styled.a`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-between;
  width: 100%;
  max-width: 384px;
  height: 384px;
  border: 2px solid #a0a0a0;
  border-radius: var(--border-radius);

  &:hover {
    text-decoration: none;
  }

  @media ${({theme}) => theme.media.mobile} {
    max-width: 100%;
    height: 240px;
  }
`

const CardHeader = styled.header`
  padding: 28px 28px 0 32px;
  display: flex;
  height: 40%;
  justify-content: space-between;
  align-items: flex-start;

  time {
    font-weight: 400;
    font-size: 16px;
    line-height: 24px;
    color: #909090;
  }

  @media ${({theme}) => theme.media.mobile} {
    padding: 20px 20px 0 24px;
  }
`

const CardBody = styled.div`
  padding: 0 32px 32px;
  display: flex;
  flex-flow: column nowrap;
  justify-content: flex-end;
  height: 60%;

  @media ${({theme}) => theme.media.mobile} {
    padding: 0 24px 24px;
    height: 66%;
  }
`

const Logo = styled.picture`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 96px;
  height: 96px;
  padding: 17px;
  border-radius: 50%;
  overflow: hidden;
  background-color: #fff;

  > div {
    position: relative !important;
    padding: 17px;
  }

  @media ${({theme}) => theme.media.mobile} {
    width: 52px;
    height: 52px;
  }
`

const Title = styled.h5`
  font-weight: 600;
  font-size: 28px;
  line-height: 38px;
  margin: 0;
  display: -webkit-box;
  -webkit-line-clamp: 4;
  -webkit-box-orient: vertical;
  overflow: hidden;

  @media ${({theme}) => theme.media.mobile} {
    font-size: 24px;
    line-height: 34px;
  }

  ${props => props.withAuthor && css`
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 4;
    overflow: hidden;
    max-height: 154px;
  `}
`

const Author = styled.p`
  font-weight: 600;
  font-size: 18px;
  line-height: 28px;
  margin-bottom: 16px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;

  @media ${({theme}) => theme.media.mobile} {
    display: none;
  }
`

const NewCard = (props: INew) => {
  const { createdAt, image, isSpeakArticle } = props
  const slug = isSpeakArticle ? `/about/speak/${props.slug}` : `/about/news/${props.slug}`

  const showImage = () => {
    return image && image.url && image.url.length > 0
  }

  return (
    <StyledNewCard { ...props } >
      <Link href={slug} passHref>
        <LinkWrapper>
          <CardHeader>
            <Moment locale="ru" format="D MMM YYYY" withTitle>
              { createdAt }
            </Moment>

            {showImage() &&
              <Logo>
                <Image
                  src={image.url}
                  alt={image.alt}
                  layout="fill"
                  sizes={'10vw'}
                  objectFit="contain"
                />
              </Logo>
            }

          </CardHeader>

          <CardBody>
            {props.withAuthor &&
              <Author>
                {props.author}
              </Author>
            }

            <Title withAuthor={props.withAuthor}>
              {props.name}
            </Title>
          </CardBody>
        </LinkWrapper>
      </Link>
    </StyledNewCard>
  )
};

export default NewCard;