import styled from 'styled-components'
import { useState } from 'react'

const Field = styled.label`
  display: flex;
  flex-flow: column nowrap;
  width: 100%;
  background-color: ${({ theme }) => theme.colors.white};
`

const FieldInput = styled.input`
  width: 100%;
  padding: 8px;
  border: 2px solid ${({ theme }) => theme.colors.lightGray};
  border-radius: ${({ theme }) => theme.radius};
  height: 60px;
`

const FieldLabel = styled.span`
  display: inline-block;
  margin-bottom: 24px;
  cursor: pointer;
  color: ${({ theme }) => theme.colors.grayText};
`

const FieldError = styled.div``

interface IField {
  label: string
  type?: string
  val?: string | number
}

const AppField = (props: IField) => {
  const [value, setValue] = useState()

  const handleChange = (event) => {
    setValue(event.target.value)
  }
  // console.log(value)

  return (
    <Field {...props}>
      <FieldLabel>{props.label}</FieldLabel>
      <FieldInput type={props.type} onChange={handleChange} />
    </Field>
  )
}

export default AppField
