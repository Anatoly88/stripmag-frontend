import Link from 'next/link'
import styled from 'styled-components'
import { hideScrollbar } from '../../styles/mixins'

const StyledSidebar = styled.aside`
  padding-top: 126px;

  @media ${({ theme }) => theme.media.mobile} {
    padding-top: 0;
    margin-left: -20px;
    margin-right: -20px;
    overflow-y: auto;
    border-bottom: 1px solid ${({ theme }) => theme.colors.border};
    ${hideScrollbar};
  }
`

const SidebarList = styled.ul`
  padding: 0;
  margin: 0;
  list-style: none;
  border-right: 1px solid ${({ theme }) => theme.colors.border};

  li {
    position: relative;
    height: 60px;
    font-size: 0;

    &.isActive {
      &:after {
        content: '';
        position: absolute;
        right: -1px;
        top: 0;
        height: 100%;
        width: 2px;
        background-color: #000;

        @media ${({ theme }) => theme.media.mobile} {
          width: 100%;
          height: 2px;
          right: auto;
          left: 0;
          top: calc(100% - 1px);
        }
      }

      a {
        color: #000000;
      }
    }

    @media ${({ theme }) => theme.media.mobile} {
      margin-right: 32px;
    }
  }

  @media ${({ theme }) => theme.media.mobile} {
    display: flex;
    border-right: none;
    padding-left: 20px;
  }
`

const SidebarLink = styled.a`
  display: inline-block;
  font-size: 18px;
  line-height: 60px;
  height: 60px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  color: ${({ theme }) => theme.colors.grayText};

  @media ${({ theme }) => theme.media.mobile} {
    height: 62px;
  }
`

const StaticSidebar = (props) => {
  return (
    <StyledSidebar {...props}>
      <SidebarList>
        {props.nav &&
          props.nav.length > 0 &&
          props.nav.map((item, index) => (
            <li key={`sidebar_nav_${index}`} className={item.name === props.active ? 'isActive' : ''}>
              <Link href={`${item.staticUrl}`} passHref scroll={false}>
                <SidebarLink>{item.name}</SidebarLink>
              </Link>
            </li>
          ))}
      </SidebarList>
    </StyledSidebar>
  )
}

export default StaticSidebar
