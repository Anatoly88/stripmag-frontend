import styled from "styled-components"
import Link from 'next/link'

const StyledNavCatalogLink = styled.a`
  color: ${({color, theme}) => color ?? theme.colors.grayText};
  transition: color 0.3s;
  
  &:hover,
  &:focus {
    opacity: 1;
    color: ${({theme}) => theme.colors.accent};
    text-decoration: none;
    transition: color 0.3s;
  }
`

type IQuery = {
  id: string;
}

interface ILinkProps {
  url: string
  text: string
  query: IQuery
  color?: string
  children?: object
}

const NavCatalogLink = (props: ILinkProps) => {
  return (
    <Link
      href={{
        pathname: props.url,
        query: props.query
      }}
      passHref
    >
      <StyledNavCatalogLink { ...props }>
        {props.text}
        {props.children}
      </StyledNavCatalogLink>
    </Link>
  )
};

export default NavCatalogLink;