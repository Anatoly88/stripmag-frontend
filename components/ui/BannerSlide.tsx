import styled from "styled-components"
import Image from "next/image";
import { FC } from "react";
import AppButton from "./AppButton";
import { IBanner } from "../../interfaces/banner";
import {imageOrDefault} from "../../helpers/image-or-default";


const StyledBannerSlide = styled.div`
  align-items: center;
  justify-content: space-between;
  background-color: #000;
  border-radius: 8px;
  color: #fff;
  height: 560px;
  padding: 0 var(--page-padding);
  overflow: hidden;
  
  @media ${({theme}) => theme.media.mobile} {
    flex-flow: column-reverse nowrap;
    padding: 58px 32px 32px;
    height: 480px;
  }
`

const BannerText = styled.article`
  width: 100%;
  max-width: 520px;
  
  h2 {
    font-weight: 600;
    font-size: 56px;
    line-height: 64px;
    max-height: 128px;
    overflow: hidden;
    color: #ffffff;
    margin-bottom: 40px;

    @media ${({theme}) => theme.media.mobile} {
      font-size: 24px;
      line-height: 34px;
      margin-bottom: 26px;
    }
  }

  p {
    font-size: 22px;
    line-height: 32px;
    margin-bottom: 40px;
    
    @media ${({theme}) => theme.media.mobile} {
      display: none;
    }
  }
  
  @media ${({theme}) => theme.media.mobile} {
    max-width: 384px;
  }
`

const BannerPicture = styled.picture`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 360px;
  width: 360px;
  margin-right: 30px;
  border-radius: var(--border-radius);
  transform: rotate(-6deg);
  
  > div {
    border-radius: inherit;
  }

  &:before {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background: linear-gradient(135deg, #FF5F6D 2.88%, #FFC371 100%);
    border-radius: 8px;
    overflow: hidden;
    transform: rotate(12deg);
  }

  @media ${({theme}) => theme.media.mobile} {
    width: 180px;
    height: 180px;
    margin: 0;
  }
`

const BannerSlide: FC<IBanner> = (props) => {
  const {title, image, description, htmlUrl, html} = props

  return (
    <StyledBannerSlide { ...props } style={{ display: 'flex' }}>
      <BannerText>
        {title && <h2>{title}</h2>}
        {description && <p>{description}</p>}
        {html && <div dangerouslySetInnerHTML={{__html: html}}></div>}
        {htmlUrl &&
          <AppButton
              width='247px'
              white
          >
            <a href={htmlUrl}>
              Перейти
            </a>
          </AppButton>
        }
      </BannerText>

      {image && image.url &&
        <BannerPicture>
          <Image
              src={image.url}
              alt={image?.alt}
              layout="fill"
              objectFit="cover"
              unoptimized
          />
        </BannerPicture>
      }
    </StyledBannerSlide>
  )
};

export default BannerSlide;