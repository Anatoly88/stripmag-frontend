import styled, { css } from 'styled-components'

const StyledAppSectionTitle = styled.h2`
  font-weight: 600;
  font-size: 56px;
  line-height: 64px;
  margin-bottom: ${({ marginBottom }) => marginBottom || '72px'};

  ${(props) =>
    props.marginTop &&
    css`
      margin-top: ${({ marginTop }) => marginTop || '0'};
    `}

  @media ${({ theme }) => theme.media.mobile} {
    font-size: 44px;
    line-height: 52px;
    margin-bottom: ${({ marginBottom }) => marginBottom || '36px'};
  }
`

interface InterfaceTitle {
  text: string
  marginTop?: string
  marginBottom?: string
}

const AppSectionTitle = (props: InterfaceTitle) => {
  return <StyledAppSectionTitle {...props}>{props.text}</StyledAppSectionTitle>
}

export default AppSectionTitle
