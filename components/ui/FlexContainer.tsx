import styled, { css } from "styled-components"
import { hideScrollbar } from "../../styles/mixins";

const StyledFlexContainer = styled.div`
  display: flex;
  flex-flow: ${({ flow }) => flow ?? 'row nowrap'};
  align-items: ${({ align }) => align ?? 'flex-start'};
  justify-content: ${({ justify }) => justify ?? 'flex-start'};
  
  ${props => props.scroll && css`
    overflow-x: auto;
    ${hideScrollbar}
  `}
`

const FlexContainer = (props) => {
  return <StyledFlexContainer { ...props } />
};

export default FlexContainer;