import IconCheck from '../../public/images/icons/check.svg'
import React from 'react'
import styled from 'styled-components'

const RadioText = styled.span`
  font-size: 18px;
  line-height: 28px;
  transition: color 0.25s;
`

const RadioIcon = styled.i`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 24px;
  height: 24px;
  border: 2px solid ${({ theme }) => theme.colors.lightGray};
  border-radius: 50%;
  color: ${({ theme }) => theme.colors.lightGray};
  margin-right: 16px;
  transition: border-color 0.25s;

  svg {
    opacity: 0;
    transition: opacity 0.25s;
  }
`

const RadioEl = styled.input.attrs(({ val, name }) => ({
  type: 'radio',
  value: val,
  name,
}))`
  display: none;

  &:checked {
    + ${RadioIcon} {
      border-color: ${({ theme }) => theme.colors.black};
      color: ${({ theme }) => theme.colors.black};
      transition: border-color 0.25s;

      svg {
        opacity: 1;
        transition: opacity 0.25s;
      }

      + ${RadioText} {
        color: ${({ theme }) => theme.colors.black};
        transition: color 0.25s;
      }
    }
  }
`

const StyledRadio = styled.label`
  display: flex;
  align-items: center;
  flex-flow: row nowrap;
  cursor: pointer;
  color: ${({ theme }) => theme.colors.grayText};

  @media ${({ theme }) => theme.media.desktop} {
    &:hover,
    &:focus {
      color: ${({ theme }) => theme.colors.black};

      ${RadioIcon} {
        border-color: ${({ theme }) => theme.colors.black};
        color: ${({ theme }) => theme.colors.black};
      }
    }
  }
`

interface IRadio {
  val: string | number
  group: string
  text?: string
}

const handlerChange = (e, id) => {
  const isChecked = e.target.checked
  console.log(e.target)
  if (isChecked) {
    console.log(id)
    // setCheckbox(id, filterType)
  } else {
    console.log(id)
    // removeCheckbox(id, filterType)
  }
}

const Radio = ({ val, text, group }: IRadio) => {
  return (
    <StyledRadio>
      <RadioEl val={val} onChange={(event) => handlerChange(event, val)} name={group} />
      <RadioIcon>
        <IconCheck />
      </RadioIcon>
      <RadioText>{text}</RadioText>
    </StyledRadio>
  )
}

export default Radio
