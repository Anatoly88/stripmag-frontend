import styled from 'styled-components'
import ArrowRightIcon from '../../public/images/icons/arrow_right.svg'

const StyledAppReadMore = styled.a`
  display: flex;
  align-items: center;
  font-weight: 600;
  font-size: 18px;
  line-height: 26px;
  color: ${({ theme }) => theme.colors.black};
`

const StyledAppReadMoreIcon = styled.span`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 2px;
  width: 24px;
  height: 24px;
  margin-left: 16px;

  svg {
    width: 100%;
    height: 100%;
  }
`

const AppReadMore = (props) => {
  return (
    <StyledAppReadMore {...props}>
      <span>Подробнее</span>
      <StyledAppReadMoreIcon>
        <ArrowRightIcon />
      </StyledAppReadMoreIcon>
    </StyledAppReadMore>
  )
}

export default AppReadMore
