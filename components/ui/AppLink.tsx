import styled from "styled-components"
import Link from 'next/link'

const StyledAppLink = styled.a`
  color: ${({color, theme}) => color ?? theme.colors.grayText};
  transition: color 0.3s;
  
  &:hover,
  &:focus {
    opacity: 1;
    color: ${({theme}) => theme.colors.accent};
    text-decoration: none;
    transition: color 0.3s;
  }
`

interface ILinkProps {
  url: string
  text: string
  color?: string
  children?: object
}

const AppLink = (props: ILinkProps) => {
  return (
    <Link
      href={props.url}
      passHref
    >
      <StyledAppLink { ...props }>
        {props.text}
        {props.children}
      </StyledAppLink>
    </Link>
  )
};

export default AppLink;