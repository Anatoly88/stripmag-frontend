import styled from 'styled-components'
import Slider from 'react-slick'
import { INew } from '../../interfaces/new'
import NewCard from '../news/NewCard'
import IconArrowLeft from '../../public/images/icons/arrow_left.svg'
import IconArrowRight from '../../public/images/icons/arrow_right.svg'
import AppSectionTitle from './AppSectionTitle'

function getRandomInt(min, max) {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min)) + min //Максимум не включается, минимум включается
}

function SampleNextArrow(props) {
  return (
    <ArrowBtn {...props}>
      <IconArrowRight />
    </ArrowBtn>
  )
}

function SamplePrevArrow(props) {
  return (
    <ArrowBtn {...props}>
      <IconArrowLeft />
    </ArrowBtn>
  )
}

const StyledSliderCarousel = styled.div`
  position: relative;
  padding: var(--section-padding-top) 0 var(--section-padding-bottom);

  .slick-slide {
    margin: 0 24px;

    @media ${({ theme }) => theme.media.mobile} {
      margin: 0 14px;
    }
  }

  .slick-list {
    margin-left: -24px;
    margin-right: -24px;
    height: 384px;

    @media ${({ theme }) => theme.media.mobile} {
      margin-left: 0;
      margin-right: 0;
      height: 240px;
    }
  }

  .slick-track {
    display: flex;
    flex-flow: row nowrap;
    justify-content: space-between;
  }
`

const ArrowBtn = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  outline: none;
  border: none;
  top: -136px;
  right: 0;
  width: 64px;
  height: 64px;
  border-radius: 50%;
  background-color: #e5e5e5;
  fill: #a0a0a0;
  z-index: 5;
  transform: none;
  transition: fill 0.2s, background-color 0.2s;

  svg {
    width: 24px;
    height: 14px;
  }

  &.slick-prev {
    left: auto;
    right: 92px;
  }

  &:before {
    display: none;
  }

  &:hover,
  &:focus {
    background-color: rgba(229, 229, 229, 0.8);
    fill: #000;
    transition: fill 0.2s, background-color 0.2s;
  }
`

const SliderTitle = styled(AppSectionTitle)`
  @media ${({ theme }) => theme.media.mobile} {
    padding-right: 36px;
  }
`

const SliderNewCard = styled(NewCard)`
  @media ${({ theme }) => theme.media.mobile} {
    width: 276px !important;
    max-width: none;
  }
`

const SliderCarousel = (props) => {
  const settings = {
    dots: false,
    infinite: true,
    arrows: true,
    speed: 500,
    slidesToShow: props.countShow ? props.countShow : 3,
    slidesToScroll: props.countScroll ? props.countShow : 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    variableWidth: false,
    responsive: [
      {
        breakpoint: 1280,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          dots: false,
          arrows: false,
          variableWidth: true,
        },
      },
    ],
  }

  const uid = getRandomInt(0, 100)

  return (
    <StyledSliderCarousel {...props}>
      {props.title && <SliderTitle text={props.title} />}

      <Slider {...settings}>
        {props.slides.map((slide: INew, index) => (
          <SliderNewCard
            key={`slider_${uid}_${index}`}
            createdAt={slide?.createdAt}
            name={slide?.name}
            image={slide?.image}
            slug={slide?.slug}
            author={slide?.author}
            withAuthor={!!slide.author}
          />
        ))}
      </Slider>
    </StyledSliderCarousel>
  )
}

export default SliderCarousel
