import styled, { css } from 'styled-components'

const StyledAppContainer = styled.div`
  margin: 0 auto;
  width: 100%;
  min-width: 320px;
  max-width: ${({ theme }) => theme.layoutWidth};
  height: 100%;

  @media ${({ theme }) => theme.media.mobile} {
    padding: 0 20px;
  }

  ${(props) =>
    props.noPadding &&
    css`
      padding: 0;

      @media ${({ theme }) => theme.media.mobile} {
        padding: 0;
      }
    `}

  ${(props) =>
    props.noPaddingRight &&
    css`
      @media ${({ theme }) => theme.media.mobile} {
        padding-right: 0;
      }
    `}

  ${(props) =>
    props.flex &&
    css`
      display: flex;
      flex-direction: ${({ direction }) => direction ?? 'row'};
      align-items: ${({ align }) => align ?? 'stretch'};
      justify-content: ${({ justifyContent }) => justifyContent ?? 'stretch'};
    `}
`

const AppContainer = (props) => {
  return <StyledAppContainer {...props} />
}

export default AppContainer
