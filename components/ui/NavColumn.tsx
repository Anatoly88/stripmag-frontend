import styled from 'styled-components'
import AppLink from './AppLink'
import IconArrowNav from '../../public/images/icons/arrow_nav.svg'
import { useEffect, useState } from 'react'
import { isMobile } from 'react-device-detect'

const StyledNavColumn = styled.div`
  list-style: none;
`

const NavColTitle = styled(AppLink)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-weight: 400;
  font-size: 16px;
  line-height: 24px;
  margin-bottom: 24px;
  padding-bottom: 12px;
  border-bottom: 1px solid ${({ theme }) => theme.colors.border};

  svg {
    display: none;
    stroke: ${({ theme }) => theme.colors.lightGray ?? '#000'};
    transform: rotate(90deg);

    @media ${({ theme }) => theme.media.mobile} {
      display: inline-block;
    }
  }

  @media ${({ theme }) => theme.media.mobile} {
    padding: 24px 20px;
    margin: 0;
  }
`

const NavColTitleDropdown = styled.h3`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-weight: 400;
  font-size: 16px;
  line-height: 24px;
  padding: 24px 20px;
  margin: 0;
  border-top: 1px solid ${({ theme }) => theme.colors.border};
  color: ${({ theme }) => theme.colors.grayText};

  &.isOpen {
    svg {
      transform: rotate(-90deg);
      transition: transform 0.3s;
    }
  }

  svg {
    display: inline-block;
    stroke: ${({ theme }) => theme.colors.lightGray ?? '#000'};
    transform: rotate(90deg);
    transition: transform 0.3s;
  }
`

const NavColList = styled.ul`
  position: relative;
  font-size: 16px;
  line-height: 24px;
  list-style: none;
  padding: 0;
  margin: 0;

  li {
    &:not(:last-of-type) {
      margin-bottom: 12px;

      @media ${({ theme }) => theme.media.mobile} {
        margin-bottom: 16px;
      }
    }
  }

  &.isOpen {
    position: relative;
    opacity: 1;
    transform: translateY(0);
    visibility: visible;
    transition: opacity 0.3s;
  }

  @media ${({ theme }) => theme.media.mobile} {
    position: absolute;
    opacity: 0;
    visibility: hidden;
    padding: 24px 0 32px;
    margin: 0 20px;
    border-top: 1px solid ${({ theme }) => theme.colors.border};
    transform: translateY(-100%);
    transition: opacity 0.3s;
  }
`

interface INavColLink {
  name: string
  menu: string
  staticUrl: string
}

interface INavColItem {
  name: string
  menu: string
  staticUrl: string
  children: Array<INavColLink>
}

interface INavColList {
  navCol: INavColItem
  handleMenuToggle?: () => void
}

const NavColumn = ({ navCol, ...props }: INavColList) => {
  const header = navCol.menu
  const colUrl = navCol.staticUrl
  const list = navCol.children
  const [isOpen, setIsOpen] = useState(false)
  const [mobile, setMobile] = useState<Boolean>()
  const { handleMenuToggle } = props

  useEffect(() => {
    setMobile(isMobile)
  }, [setMobile])

  return (
    <StyledNavColumn {...props}>
      {mobile ? (
        <NavColTitleDropdown className={isOpen ? 'isOpen' : ''} onClick={() => setIsOpen(!isOpen)}>
          {header}
          <IconArrowNav />
        </NavColTitleDropdown>
      ) : (
        <NavColTitle url={`${colUrl}`} text={header} onClick={handleMenuToggle}>
          <IconArrowNav />
        </NavColTitle>
      )}

      <NavColList className={isOpen ? 'isOpen' : ''}>
        {list.map((item, index) => (
          <li key={`${item.menu}_${index}`} onClick={handleMenuToggle}>
            <AppLink url={`${item.staticUrl}`} text={item.menu} />
          </li>
        ))}
      </NavColList>
    </StyledNavColumn>
  )
}

export default NavColumn
