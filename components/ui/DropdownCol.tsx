import styled, { keyframes } from 'styled-components'
import { useState } from 'react'
import IconArrowNav from '../../public/images/icons/arrow_nav.svg'
import AppLink from './AppLink'

const dropdownKeyframes = keyframes`
  0% {
    transform: scaleY(0);
  }
  80% {
    transform: scaleY(1.1);
  }
  100% {
    transform: scaleY(1);
  }
`

const SIconArrow = styled(IconArrowNav)`
  display: inline-block;
  stroke: ${({ theme }) => theme.colors.lightGray ?? '#000'};
  transform: rotate(90deg);
  transition: transform 0.3s;
`

const StyledDropdownCol = styled.div`
  position: relative;

  &:not(:last-of-type) {
    margin-bottom: 28px;
  }
`

const SList = styled.ul`
  width: 100%;
  padding: 0;
  margin: 0;
  opacity: 0;
  display: none;
  animation: ${dropdownKeyframes} 0.3s ease-in-out forwards;
  transform-origin: top center;
`

const SItem = styled.li`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-flow: row wrap;
  font-size: 18px;
  line-height: 28px;
  padding-left: 20px;
  padding-right: 6px;
  margin-bottom: 32px;

  span {
    display: inline-block;
  }
`

const STitle = styled.h5`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-weight: 600;
  font-size: 24px;
  line-height: 34px;
  padding-right: 8px;
  margin-bottom: 0;

  &.isOpen {
    margin-bottom: 32px;
    transition: margin-bottom 0.3s;

    + ${SList} {
      display: block;
      opacity: 1;
    }

    ${SIconArrow} {
      transform: rotate(-90deg);
      transition: transform 0.3s;
    }
  }
`

const SItemTitle = styled.h5`
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-weight: 400;
  font-size: 18px;
  line-height: 28px;
  margin-bottom: 0;
  width: 100%;

  &.isOpen {
    margin-bottom: 32px;

    + ${SList} {
      display: block;
      opacity: 1;
    }

    ${SIconArrow} {
      transform: rotate(-90deg);
      transition: transform 0.3s;
    }
  }

  &.isPages {
    margin-bottom: 28px;

    + ${SList} > ${SItem} {
      margin-bottom: 24px;
    }
  }
`

const SLink = styled(AppLink)`
  font-size: 16px;
  line-height: 24px;
  color: #000000;

  &:hover,
  &:focus {
    color: #000000;
  }
`

const DropdownCol = ({ name, list, ...props }) => {
  const [isOpen, setIsOpen] = useState(false)

  const openCat = (event) => {
    const el = event.currentTarget

    if (!el.classList.contains('isOpen')) {
      el.classList.add('isOpen')
    } else {
      el.classList.remove('isOpen')
    }
  }

  const openPages = (event) => {
    const el = event.currentTarget
    const list = ['isOpen', 'isPages']
    if (!el.classList.contains('isOpen')) {
      el.classList.add(...list)
    } else {
      el.classList.remove(...list)
    }
  }

  return (
    <StyledDropdownCol {...props}>
      <STitle className={isOpen ? 'isOpen' : ''} onClick={() => setIsOpen(!isOpen)}>
        <span>{name}</span>
        <SIconArrow />
      </STitle>

      <SList>
        {list &&
          list.map((item, index) => (
            <SItem key={`dropdown_list_item_${index}`}>
              {item.children && item.children.length > 0 && (
                <SItemTitle onClick={openCat}>
                  <span>{item.name}</span>
                  <SIconArrow />
                </SItemTitle>
              )}

              {!item.children && <SLink text={item.name} url={item.staticUrl} />}

              {item.children && item.children.length > 0 && (
                <SList>
                  {item.children.map((category, index) => (
                    <SItem key={`dropdown_list_category_${index}`}>
                      <SItemTitle onClick={openPages}>
                        <span>{category.name}</span>
                        <SIconArrow />
                      </SItemTitle>

                      {category.children.length > 0 && (
                        <SList>
                          {category.children.map((page, index) => (
                            <SItem key={`dropdown_list_link_${index}`}>
                              <SLink text={page.name} url={`/catalog${page.slug}`} />
                            </SItem>
                          ))}
                        </SList>
                      )}
                    </SItem>
                  ))}
                </SList>
              )}
            </SItem>
          ))}
      </SList>
    </StyledDropdownCol>
  )
}

export default DropdownCol
