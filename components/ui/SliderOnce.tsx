import styled from "styled-components"
import Slider from "react-slick"
import BannerSlide from "./BannerSlide"
import { IBanner } from "../../interfaces/banner"

const StyledSliderOnce = styled.div`
  padding: 96px 90px 134px;
  margin-left: calc(-1 * var(--page-padding));
  margin-right: calc(-1 * var(--page-padding));

  .slick-current > div {
    padding: 0 48px;
    
    @media ${({theme}) => theme.media.mobile} {
      padding: 0 12px;
    }
  }
  
  .slick-slider {
    min-width: 320px;
    max-width: ${({theme}) => theme.bannersWidth};
    margin: 0 auto;
  }
  
  .slick-list {
    overflow: visible;
  }
  
  .slick-track {
    min-width: 100%;
    display: flex;
    justify-content: center;
  }

  .slick-dots {
    bottom: -44px;
    font-size: 0;

    li {
      width: 60px;
      height: 4px;
      margin: 0 14px;

      &.slick-active {
        button {
          background-color:#000;
        }
      }
      
      @media ${({theme}) => theme.media.mobile} {
        width: 40px;
        margin: 0 12px;
      }
    }

    button {
      background-color: #D0D0D0;
      border-radius: 2px;
      width: 60px;
      height: 4px;
      padding: 0;
      
      &:before {
        display:none;
      }
      
      @media ${({theme}) => theme.media.mobile} {
        width: 40px;
      }
    }
    
    @media ${({theme}) => theme.media.mobile} {
      bottom: -32px;
    }
  }

  @media ${({theme}) => theme.media.mobile} {
    padding: 20px 20px 80px;
    margin-left: -60px;
    margin-right: -60px;
  }
`

const SliderOnce = ({slides, ...props}) => {
  const settings = {
    dots: true,
    infinite: true,
    arrows: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    centerMode: true,
    centerPadding: '48px'
  }

  return (
    <StyledSliderOnce { ...props }>
      {slides &&
        <>
          <Slider {...settings}>
            {slides.map((slide: IBanner, index) =>
              <BannerSlide
                key={`slide_${index}`}
                title={slide.title}
                description={slide.description}
                htmlUrl={slide.htmlUrl}
                image={slide?.image}
                html={slide?.html}
              />
            )}
          </Slider>
        </>
      }
    </StyledSliderOnce>
  )
};

export default SliderOnce;