import styled from 'styled-components'
import IconCheck from '../../public/images/icons/check.svg'
import React from 'react'
import { lineClamp } from '../../styles/mixins'

const CheckboxText = styled.span`
  display: inline-block;
  width: calc(100% - 24px - 16px);
  max-height: 56px;
  font-size: 18px;
  line-height: 28px;
  transition: color 0.25s;
  padding-right: 15px;
  hyphens: auto;
  cursor: pointer;
  ${lineClamp(2, '56px')};

  @media ${({ theme }) => theme.media.desktop} {
    &:hover,
    &:focus {
      color: ${({ theme }) => theme.colors.black};
    }
  }
`

const CheckboxIcon = styled.i`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 24px;
  height: 24px;
  border: 2px solid ${({ theme }) => theme.colors.lightGray};
  border-radius: ${({ theme }) => theme.radius};
  color: ${({ theme }) => theme.colors.lightGray};
  margin-right: 16px;
  margin-top: 2px;
  transition: border-color 0.25s;
  cursor: pointer;

  svg {
    opacity: 0;
    transition: opacity 0.25s;
  }

  @media ${({ theme }) => theme.media.desktop} {
    &:hover {
      border-color: ${({ theme }) => theme.colors.black};
      color: ${({ theme }) => theme.colors.black};
    }
  }
`

const CheckboxEl = styled.input.attrs(({ val }) => ({
  type: 'checkbox',
  value: val,
}))`
  display: none;

  &:checked {
    + ${CheckboxIcon} {
      border-color: ${({ theme }) => theme.colors.black};
      color: ${({ theme }) => theme.colors.black};
      transition: border-color 0.25s;

      svg {
        opacity: 1;
        transition: opacity 0.25s;
      }

      + ${CheckboxText} {
        color: ${({ theme }) => theme.colors.black};
        transition: color 0.25s;
      }
    }
  }

  &:disabled {
    + ${CheckboxIcon} {
      border-color: ${({ theme }) => theme.colors.lightGray};
      opacity: 0.5;
      cursor: not-allowed;

      + ${CheckboxText} {
        color: ${({ theme }) => theme.colors.lightGray};
        opacity: 0.7;
        cursor: not-allowed;
      }
    }

    &:checked {
      + ${CheckboxIcon} {
        svg {
          opacity: 1;
          transition: opacity 0.25s;
        }
      }
    }
  }
`

const StyledCheckbox = styled.label`
  display: flex;
  align-items: flex-start;
  flex-flow: row nowrap;
  color: ${({ theme }) => theme.colors.grayText};
`

const Checkbox = ({ id, text, setCheckbox, removeCheckbox, filterType, isChecked = false, isDisabled = false }) => {
  const handlerChange = (e, id) => {
    const isChecked = e.target.checked
    if (isChecked) {
      setCheckbox(id, filterType)
    } else {
      removeCheckbox(id, filterType)
    }
  }

  return (
    <StyledCheckbox>
      <CheckboxEl val={id} onChange={(event) => handlerChange(event, id)} disabled={isDisabled} checked={isChecked} />
      <CheckboxIcon>
        <IconCheck />
      </CheckboxIcon>
      <CheckboxText>{text}</CheckboxText>
    </StyledCheckbox>
  )
}

export default Checkbox
