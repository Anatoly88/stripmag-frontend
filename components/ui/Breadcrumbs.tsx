import Link from "next/link"
import styled from "styled-components"

const StyledBreadcrumbs = styled.ul`
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  list-style: none;
  padding: 32px 0;
  margin: 0;
  
  @media ${({theme}) => theme.media.mobile} {
    padding: 20px 0;
  }
`

const BreadcrumbItem = styled.li`
  font-size: 16px;
  line-height: 24px;
  color: ${({theme}) => theme.colors.lightGray};
  
  &:not(:last-of-type) {
    &:after {
      content: '\\2014';
      display: inline-block;
      margin: 0 16px;
    }
  }
`

const BreadcrumbLink = styled.a`
  font-size: 16px;
  line-height: 24px;
  color: ${({theme}) => theme.colors.lightGray};
`

const Breadcrumbs = ({ navList = [] ,...props }) => {
  const total = navList.length
  const lastItem = navList.length - 1

  return (
    <StyledBreadcrumbs { ...props }>
      {navList && navList.length > 0 &&
        navList.map((item, index) =>
          <BreadcrumbItem key={`breadcrumb_${index}`}>
            {lastItem !== index ?
              (
                <Link href={item.staticUrl} passHref>
                  <BreadcrumbLink>
                    {item.name}
                  </BreadcrumbLink>
                </Link>
              ) : (
                <span>{item.name}</span>
              )
            }
          </BreadcrumbItem>
        )
      }
    </StyledBreadcrumbs>
  )
};

export default Breadcrumbs;