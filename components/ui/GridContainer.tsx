import styled, { css } from "styled-components"
import { hideScrollbar } from "../../styles/mixins";
import React from "react";

const StyledGridContainer = styled.div`
  display: grid;
  grid-auto-flow: ${({ flow }) => flow ?? 'row'};
  grid-template-columns: ${({ cols }) => cols ?? 'initial'};
  grid-template-rows: ${({ rows }) => rows ?? 'initial'};
  column-gap: ${({columnGap}) => columnGap ?? '0'};
  row-gap: ${({rowGap}) => rowGap ?? '0'};
  gap: ${({ gap }) => gap ?? '15px'};
  
  ${props => props.scroll && css`
    overflow-x: auto;
    ${hideScrollbar}
  `}
`

interface ICatalogGrid {
  flow?: string
  cols?: string
  rows?: string
  columnGap?: string
  rowGap?: string
  gap?: string
  scroll?: boolean
  children: React.ReactNode
}

const GridContainer = (props: ICatalogGrid) => {
  return <StyledGridContainer { ...props } />
};

export default GridContainer;