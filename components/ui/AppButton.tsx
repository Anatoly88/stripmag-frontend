import styled, { css } from 'styled-components'

const StyledButton = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  width: 100%;
  max-width: ${(props) => props.width ?? '384px'};
  height: 60px;
  background-color: #000;
  font-weight: 600;
  font-size: 18px;
  line-height: 28px;
  text-align: center;
  color: #fff;
  border: 2px solid transparent;
  outline: none;
  border-radius: var(--border-radius);
  transition: border-color 0.3s, background-color 0.3s, color 0.3s;

  > a {
    position: absolute;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    height: 100%;
  }

  &:hover,
  &:focus {
    background-color: transparent;
    border-color: #000;
    color: #000;
    transition: border-color 0.3s, background-color 0.3s, color 0.3s;
  }

  @media ${({ theme }) => theme.media.mobile} {
    max-width: 100%;
  }

  ${(props) =>
    props.white &&
    css`
      color: #000;
      background-color: #fff;
      border-radius: 4px;

      &:hover,
      &:focus {
        background-color: transparent;
        border-color: #fff;
        color: #fff;
      }
    `}
`

const AppButton = ({ white = false, width = '384px', children, ...props }) => {
  return (
    <StyledButton width={width} white={white} {...props}>
      {children}
    </StyledButton>
  )
}

export default AppButton
