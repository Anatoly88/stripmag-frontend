import styled from "styled-components"
import Image from "next/image";
import Link from 'next/link'
import {imageOrDefault} from "../../helpers/image-or-default";

const StyledPopularBrand = styled.div`
	position: relative;
	width: 100%;
	max-width: 384px;
	height: 160px;
	border-radius: var(--border-radius);
	background-color: #fff;
	opacity: 1;
	overflow: hidden;
	transition: transform 0.3s, opacity 0.3s;
	
	&.isHidden {
	position: absolute;
	visibility: hidden;
	opacity: 0;
	transform: translateY(-100%);
	transition: transform 0.3s, opacity 0.3s;
	}
	
	@media ${({theme}) => theme.media.mobile} {
	max-width: 100%;
	height: 128px;
	}
`

const PopularBrand = (props) => {
	return (
		<StyledPopularBrand {...props} >
			<Link href={props?.url}>
				<a href={props?.url} title={props?.name}>
					<Image
						src={imageOrDefault(props?.image)}
						alt={props?.name}
						layout="fill"
						objectFit="contain"
					/>
				</a>
			</Link>
		</StyledPopularBrand>
	)
};

export default PopularBrand;

