import styled from 'styled-components'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { hideScrollbar } from '../../styles/mixins'
import {getBrandMenuItem} from "../../hooks";

const StyledAlphabet = styled.ul`
  display: flex;
  align-items: center;
  margin: 0;
  padding: 0;
  list-style: none;
  width: 100%;
  border-bottom: 1px solid ${({ theme }) => theme.colors.border};

  @media ${({ theme }) => theme.media.mobile} {
    overflow-x: auto;
    ${hideScrollbar}
  }
`

const AlphabetLink = styled.a`
  display: inline-block;
  width: 36px;
  text-align: center;
  font-size: 18px;
  line-height: 28px;
  white-space: nowrap;
  text-overflow: ellipsis;
  cursor: pointer;
  color: ${({ theme }) => theme.colors.lightGray};
`

const AlphabetItem = styled.li`
  position: relative;
  padding-bottom: 20px;

  &:not(:last-of-type) {
    margin-right: 8px;
  }

  &:after {
    content: '';
    position: absolute;
    left: 0;
    bottom: -1px;
    height: 2px;
    width: 0;
    background-color: #000;
    transition: width 0.25s;
  }

  &.isActive {
    &:after {
      width: 100%;
    }
    ${AlphabetLink} {
      color: ${({ theme }) => theme.colors.black};
    }
  }

  @media ${({ theme }) => theme.media.desktop} {
    &:hover {
      &:after {
        width: 100%;
        transition: width 0.25s;
      }
    }
  }
`

const BrandsAlphabet = ({ list }) => {
  const showList = list && list.length > 0
  const { query } = useRouter()
  const activeItem = query?.letter || '#'

  return (
    <StyledAlphabet>
      {showList &&
        list.map((item, index) => (
          <AlphabetItem className={item === activeItem ? 'isActive' : ''} key={`rating_year_${index}`}>
            <Link
              href={{
                pathname: getBrandMenuItem().staticUrl,
                query: { letter: item },
              }}
              passHref
            >
              <AlphabetLink>{item}</AlphabetLink>
            </Link>
          </AlphabetItem>
        ))}
    </StyledAlphabet>
  )
}

export default BrandsAlphabet
