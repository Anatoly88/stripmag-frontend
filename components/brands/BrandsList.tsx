import styled from 'styled-components'
import Link from 'next/link'
import {getBrandMenuItem} from "../../hooks";

const StyledBrandsList = styled.div`
  padding-top: 96px;

  @media ${({ theme }) => theme.media.mobile} {
    padding-top: 60px;
  }
`

const BrandsListLetter = styled.h5`
  font-weight: 600;
  font-size: 24px;
  line-height: 34px;
  margin-bottom: 36px;

  @media ${({ theme }) => theme.media.mobile} {
    margin-bottom: 32px;
  }
`

const BrandsListGrid = styled.ul`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  gap: 0 48px;
  padding: 0;
  margin: 0;
  list-style: none;

  @media ${({ theme }) => theme.media.mobile} {
    display: flex;
    flex-flow: column nowrap;
    gap: 0;
  }
`

const BrandsListItem = styled.li`
  padding: 0;
  margin-bottom: 16px;
`

const BrandsListLink = styled.a`
  cursor: pointer;
  font-size: 18px;
  line-height: 26px;
`

const BrandsList = ({ list, firstLetter }) => {
  return (
    <StyledBrandsList>
      <BrandsListLetter>{firstLetter}</BrandsListLetter>
      <BrandsListGrid>
        {list &&
          list.map((item, index) => (
            <BrandsListItem key={`brands_list_link${index}`}>
              <Link href={getBrandMenuItem().staticUrl + item.code} passHref>
                <BrandsListLink>{item.name}</BrandsListLink>
              </Link>
            </BrandsListItem>
          ))}
      </BrandsListGrid>
    </StyledBrandsList>
  )
}

export default BrandsList
