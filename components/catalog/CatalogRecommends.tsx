import styled from 'styled-components'
import CatalogCard from './CatalogCard'
import { hideScrollbar } from '../../styles/mixins'

const StyledCatalogRecommends = styled.div`
  padding-bottom: 80px;
`

const RecommendsTitle = styled.h2`
  font-weight: 600;
  font-size: 48px;
  line-height: 60px;
  margin-bottom: 72px;

  @media ${({ theme }) => theme.media.mobile} {
    font-size: 24px;
    line-height: 34px;
    margin-bottom: 36px;
  }
`

const RecommendsList = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  gap: 0 48px;

  @media ${({ theme }) => theme.media.mobile} {
    margin-right: -20px;
    grid-template-columns: repeat(4, 276px);
    gap: 0 28px;
    overflow-x: auto;

    ${hideScrollbar}
  }
`

const CatalogRecommends = (props) => {
  return (
    <StyledCatalogRecommends>
      <RecommendsTitle>Рекомендуемое</RecommendsTitle>

      <RecommendsList>
        <CatalogCard
          name={'Страстная сорочка с пажами для чулок Indiana'}
          brand_name={'Arianna Afari'}
          image={'https://placeimg.com/640/480/any'}
          slug={'/'}
          price={900}
          tooltip={{ new: 'some', discount: '%' }}
        />
        <CatalogCard
          name={'Кружевная сорочка Physis с бахромой'}
          brand_name={'Arianna Afari'}
          image={'https://placeimg.com/640/480/any'}
          slug={'/'}
          price={900}
        />
        <CatalogCard
          name={'Страстная сорочка с пажами для чулок Indiana'}
          brand_name={'Arianna Afari'}
          image={'https://placeimg.com/640/480/any'}
          slug={'/'}
          price={900}
        />
        <CatalogCard
          name={'Страстная сорочка с пажами для чулок Indiana'}
          brand_name={'Arianna Afari'}
          image={'https://placeimg.com/640/480/any'}
          slug={'/'}
          price={900}
        />
      </RecommendsList>
    </StyledCatalogRecommends>
  )
}

export default CatalogRecommends
