import styled, { css } from 'styled-components'
import CatalogCard from './CatalogCard'
import Pagination from '../ui/pagination/Pagination'
import { ICatalogItem } from '../../interfaces/catalog'
import CatalogSort from './CatalogSort'
import { isDesktop } from 'react-device-detect'

const StyledCatalogList = styled.div``

const CatalogListWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  gap: 96px 48px;
  align-content: start;
  padding-bottom: 96px;

  @media ${({ theme }) => theme.media.mobile} {
    display: flex;
    flex-flow: column nowrap;
    align-items: center;
    justify-content: center;
    grid-template-columns: none;
    gap: 0;
    padding-bottom: 60px;
  }
`

const StyledCatalogCard = styled(CatalogCard)`
  ${(mobile) =>
    mobile &&
    css`
      &:not(:last-of-type) {
        margin-bottom: 60px;
      }
    `}
`

const CatalogList = ({ list, total = 0, sort = {} }) => {
  return (
    <StyledCatalogList>
      {isDesktop && <CatalogSort sortList={sort} />}
      <CatalogListWrapper>
        {list &&
          list.map((item: ICatalogItem, index) => {
            return (
              <StyledCatalogCard
                key={index}
                name={item.name}
                brand_name={item.brand_name}
                brand_code={item.brand_code}
                price={item.price}
                image={item.image}
                slug={item.slug}
              />
            )
          })}
        {!list && <p>Элементов не найдено</p>}
      </CatalogListWrapper>
      {total && total > 30 && <Pagination total={total} sizes={5} />}
    </StyledCatalogList>
  )
}

export default CatalogList
