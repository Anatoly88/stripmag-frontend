import styled, { css } from 'styled-components'

const StyledCatalogNoPrice = styled.div`
  p {
    margin: 0;
    font-size: 16px;
    line-height: 24px;
    color: ${({ theme }) => theme.colors.grayText};
  }
`

const PriceThumb = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 16px;

  span {
    width: 36px;
    height: 40px;
    margin-right: 2px;
    background: #dadada;
    box-shadow: inset 0px -2px 0px rgba(0, 0, 0, 0.08);

    &:first-of-type {
      border-radius: 8px 0 0 8px;
    }

    &:last-of-type {
      margin-right: 0;
      border-radius: 0 8px 8px 0;
    }

    ${(props) =>
      props.large &&
      css`
        width: 40px;
        height: 44px;
      `}
  }

  i {
    font-weight: 600;
    font-size: 28px;
    line-height: 40px;
    font-style: normal;
    color: #ccc;
    margin-left: 12px;

    ${(props) =>
      props.large &&
      css`
        font-size: 36px;
        line-height: 44px;
      `}
  }
`

const CatalogNoPrice = (props) => {
  return (
    <StyledCatalogNoPrice {...props}>
      <PriceThumb {...props}>
        <span />
        <span />
        <span />
        <span />
        <i>₽</i>
      </PriceThumb>
      {props.descriptionText && <p>{props.descriptionText}</p>}
    </StyledCatalogNoPrice>
  )
}

export default CatalogNoPrice
