import styled, { css } from 'styled-components'
import AppLink from '../ui/AppLink'
import Link from 'next/link'

const Item = styled.li`
  display: flex;
  align-items: center;
  justify-content: space-between;
  list-style: none;
  font-size: 18px;
  line-height: 26px;
  padding: 13px 0 12px 2px;
  border-bottom: 1px solid ${({ theme }) => theme.colors.border};

  @media ${({ theme }) => theme.media.mobile} {
    padding-bottom: 0;
    padding-top: 0;

    &:last-of-type {
      border-bottom: none;
    }
  }
`

const CatalogNavTitle = styled.h3`
  font-weight: 600;
  font-size: 28px;
  line-height: 38px;
  margin-bottom: 40px;

  @media ${({ theme }) => theme.media.mobile} {
    font-size: 24px;
    line-height: 34px;
    margin-bottom: 24px;
  }
`

const CatalogNavLink = styled(AppLink)`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  max-width: 94%;
  color: ${({ theme }) => theme.colors.black};

  &&&.isActive {
    color: ${({ theme }) => theme.colors.black};
  }

  @media ${({ theme }) => theme.media.mobile} {
    padding: 12px 0 13px;
  }
`

const TotalCount = styled.span`
  font-size: 16px;
  line-height: 26px;
  color: ${({ theme }) => theme.colors.grayText};
`

const StyledCatalogNav = styled.ul`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  gap: 0 50px;
  list-style: none;
  padding: 0;
  margin: 0 0 44px;
  width: 100%;

  &.inSidebar {
    display: block;
    margin: 0 0 50px;

    ${Item} {
      border: none;
      padding: 10px 0;
      line-height: 28px;

      &:first-of-type {
        padding-top: 0;
        padding-bottom: 12px;
        font-size: 16px;
        line-height: 24px;
        margin-bottom: 18px;
        border-bottom: 1px solid ${({ theme }) => theme.colors.border};
      }
    }

    ${CatalogNavLink} {
      color: ${({ theme }) => theme.colors.grayText};

      @media ${({ theme }) => theme.media.desktop} {
        &:hover,
        &:focus {
          color: ${({ theme }) => theme.colors.black};
        }
      }
    }
  }

  @media ${({ theme }) => theme.media.mobile} {
    display: block;
    grid-template-columns: none;
    gap: 0;
    margin: 0 0 40px;
  }
`

interface ICatalogNavItem {
  name: string
  slug: string
  count: number | string
  id: number | string
}

const CatalogNav = (props) => {
  return (
    <>
      {props.title && props.title.length > 0 && (
        <CatalogNavTitle>
          <Link href={props.titleLink || '#'}>
            <a href=" ">{props.title}</a>
          </Link>
        </CatalogNavTitle>
      )}

      {!props.inSidebar && (
        <StyledCatalogNav {...props}>
          {props.list.map((item, index) => (
            <Item inSidebar={props.inSidebar} key={`catalog_nav_item${index}`}>
              <CatalogNavLink
                text={item.name}
                url={props.isBrand ? `/collections/${item.code}` : `/catalog${item.slug}`}
                className={props.active === item.id ? 'isActive' : ''}
              />
              <TotalCount>{item.count}</TotalCount>
            </Item>
          ))}
        </StyledCatalogNav>
      )}

      {props.inSidebar && (
        <StyledCatalogNav className={'inSidebar'}>
          <Item>
            <CatalogNavLink
              text={props.list.name}
              url={`/catalog${props.list.slug}`}
              className={props.active === props.list.id ? 'isActive' : ''}
            />
          </Item>
          {props.list.children &&
            props.list.children.map((item, index) => (
              <Item key={`catalog_nav_item${index}`}>
                <CatalogNavLink
                  text={item.name}
                  url={`/catalog${item.slug}`}
                  className={props.active === item.id ? 'isActive' : ''}
                />
              </Item>
            ))}
        </StyledCatalogNav>
      )}
    </>
  )
}

export default CatalogNav
