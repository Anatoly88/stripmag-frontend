import styled, { css } from 'styled-components'
import Image from 'next/image'
import Link from 'next/link'
import { ICatalogItem } from '../../interfaces/catalog'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import {getBrandMenuItem} from "../../hooks";

const StyledCard = styled.div`
  position: relative;
  width: 100%;
  max-width: 276px;

  @media ${({ theme }) => theme.media.mobile} {
    max-width: 576px;
  }
`

const CardPicture = styled.picture`
  position: relative;
  overflow: hidden;
  display: block;
  width: 100%;
  height: 276px;
  border-radius: var(--border-radius);
  background-color: #fff;

  @media ${({ theme }) => theme.media.mobile} {
    height: 320px;
  }
`

const CardBody = styled.div`
  padding: 24px 0 0;
`

const CardBrand = styled.p`
  font-size: 16px;
  line-height: 24px;
  margin: 0;
  color: #909090;
`

const CardInfo = styled.span`
  position: absolute;
  left: 8px;
  top: 8px;
  display: flex;
  align-items: center;
  width: 100%;
`

const CardTooltip = styled.span`
  display: block;
  background-color: #000;
  color: #fff;
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  border-radius: 4px;
  text-align: center;
  white-space: nowrap;
  text-overflow: ellipsis;
  max-width: 80%;
  overflow: hidden;
  padding: 2px 8px;
`

const CardSale = styled.span`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #bf1b2c;
  width: 28px;
  height: 24px;
  border-radius: 4px;
  margin-right: 8px;
  color: #fff;
`

const CardPrice = styled.strong`
  display: block;
  font-weight: 600;
  font-size: 28px;
  line-height: 40px;
  margin-bottom: 16px;
`

const CardNoPrice = styled.strong`
  display: flex;
  align-items: center;
  margin-bottom: 16px;

  span {
    width: 36px;
    height: 40px;
    margin-right: 2px;
    background: #dadada;
    box-shadow: inset 0px -2px 0px rgba(0, 0, 0, 0.08);

    &:first-of-type {
      border-radius: 8px 0 0 8px;
    }

    &:last-of-type {
      margin-right: 0;
      border-radius: 0 8px 8px 0;
    }
  }

  i {
    font-weight: 600;
    font-size: 28px;
    line-height: 40px;
    font-style: normal;
    color: #ccc;
    margin-left: 12px;
  }
`

const CardTitle = styled.h5`
  font-weight: 400;
  font-size: 18px;
  line-height: 26px;
  margin-bottom: 8px;

  @media ${({ theme }) => theme.media.mobile} {
    line-height: 28px;
  }

  ${(props) =>
    props.boldTitle &&
    css`
      font-weight: 600;
    `}
`

const CatalogCard = (props: ICatalogItem) => {
  const { isAuth } = useTypedSelector((state) => state.auth)
  const slug = `/catalog${props.slug}`

  return (
    <StyledCard {...props}>
      <Link href={slug}>
        <a rel="noreferrer">
          <CardPicture>
            <Image src={props.image} alt="" layout="fill" objectFit="contain" />
          </CardPicture>
        </a>
      </Link>

      <CardBody>
        {props.price && <CardPrice>{`${props.price} ₽`}</CardPrice>}

        {!props.price && (
          <CardNoPrice>
            <span />
            <span />
            <span />
            <span />
            <i>₽</i>
          </CardNoPrice>
        )}

        <CardTitle>
          <Link href={slug}>
            <a rel="noreferrer" dangerouslySetInnerHTML={{ __html: props.name }} />
          </Link>
        </CardTitle>

        <CardBrand>
          <Link href={getBrandMenuItem().staticUrl + props.brand_code}>
            <a rel="noreferrer">{props.brand_name}</a>
          </Link>
        </CardBrand>

        {props.tooltip && (
          <CardInfo>
            <CardSale>%</CardSale>
            <CardTooltip>{props.tooltip.new}</CardTooltip>
          </CardInfo>
        )}
      </CardBody>
    </StyledCard>
  )
}

export default CatalogCard
