import React, { useState } from 'react'
import Modal from 'styled-react-modal'
import IconFilter from '../../public/images/icons/filter.svg'
import IconClose from '../../public/images/icons/close-modal.svg'
import styled from 'styled-components'
import CatalogSidebar from './CatalogSidebar'
import CatalogSort from './CatalogSort'
import AppButton from '../ui/AppButton'
import CatalogSidebarMob from './CatalogSidebarMob'

const StyledModal = Modal.styled`
  position: relative;
  width: 100%;
  height: 100%;
  display: flex;
  flex-flow: column nowrap;
  align-items: flex-start;
  justify-content: flex-start;
  background-color: white;
  overflow-y: auto;
  padding: 96px 20px 100px;
  background-color: #fff;
  opacity: ${(props) => props.opacity};
  transition : all 0.3s ease-in-out;
`

const OpenFilterBtn = styled.button`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  border: none;
  outline: none;
  background-color: transparent;
  font-weight: 600;
  font-size: 18px;
  line-height: 28px;
  padding: 0;
  color: #000;
`

const CloseFilterBtn = styled.button`
  position: absolute;
  right: 20px;
  top: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 36px;
  height: 36px;
  border-radius: 50%;
  background-color: ${({ theme }) => theme.colors.background};
  border: none;
  outline: none;
  padding: 0;
`

const CatalogFilterTitle = styled.h2`
  font-weight: 600;
  font-size: 26px;
  line-height: 34px;
  margin-bottom: 36px;
`

const CatalogSortFooter = styled.footer`
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;
  padding: 20px;
  border-top: 1px solid ${({ theme }) => theme.colors.border};
  background-color: #fff;
`
const CatalogSortShowBtn = styled(AppButton)``

const CatalogFiltersModal = ({ filters, sort, total = 0 }) => {
  const [isOpen, setIsOpen] = useState(false)
  const [opacity, setOpacity] = useState(0)

  function toggleModal(e) {
    setOpacity(0)
    setIsOpen(!isOpen)
  }

  function afterOpen() {
    setTimeout(() => {
      setOpacity(1)
    }, 100)
  }

  function beforeClose() {
    return new Promise((resolve) => {
      setOpacity(0)
      setTimeout(resolve, 300)
    })
  }

  return (
    <>
      <OpenFilterBtn onClick={toggleModal}>
        <span>Фильтры и сортировка</span>
        <IconFilter />
      </OpenFilterBtn>

      <StyledModal
        isOpen={isOpen}
        afterOpen={afterOpen}
        beforeClose={beforeClose}
        onBackgroundClick={toggleModal}
        onEscapeKeydown={toggleModal}
        opacity={opacity}
        backgroundProps={{ opacity }}
      >
        <CloseFilterBtn onClick={toggleModal}>
          <IconClose />
        </CloseFilterBtn>
        <CatalogFilterTitle>Фильтры и сортировка</CatalogFilterTitle>
        <CatalogSort sortList={sort} />
        <CatalogSidebar filters={filters} />
        <CatalogSortFooter>
          <CatalogSortShowBtn onClick={toggleModal}>
            <span>Показать {total} товаров</span>
          </CatalogSortShowBtn>
        </CatalogSortFooter>
      </StyledModal>
    </>
  )
}

export default CatalogFiltersModal
