import styled from 'styled-components'
import Slider from 'react-slick'
import React, { useEffect, useRef, useState } from 'react'
import Image from 'next/image'
import { isMobile } from 'react-device-detect'
import IconDownload from '../../public/images/icons/download.svg'

const StyledCatalogDetailPictures = styled.div`
  position: relative;
  height: 640px;
  width: 816px;
  background-color: ${({ theme }) => theme.colors.white};
  border-radius: ${({ theme }) => theme.radius};

  .slick-track .slick-slide {
    > div {
      width: 100%;
      height: 100%;
    }
  }

  @media ${({ theme }) => theme.media.mobile} {
    width: 100%;
    height: 282px;
  }
`

const SliderLarge = styled(Slider)`
  position: relative;
  height: 100%;

  .slick-track {
    display: flex;
    height: 640px;

    .slick-slide {
      > div {
        padding: 0 200px;

        @media ${({ theme }) => theme.media.mobile} {
          padding: 0;
        }
      }
    }

    @media ${({ theme }) => theme.media.mobile} {
      height: 282px;
    }
  }

  .slick-dots {
    position: relative;
    display: flex !important;
    align-items: center;
    bottom: 0;
    height: 4px;
    text-align: left;
    background-color: ${({ theme }) => theme.colors.border};

    li {
      flex: 1;
      width: auto;
      height: 4px;
      margin: 0;

      button {
        width: 100%;
        height: 4px;
        background-color: transparent;
        font-size: 0;
        padding: 0;

        &:before {
          display: none;
        }
      }

      &.slick-active {
        button {
          background-color: #000;
        }
      }
    }
  }

  @media ${({ theme }) => theme.media.mobile} {
    margin-left: -20px;
    margin-right: -20px;
  }
`

const SliderLargeItem = styled.picture`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  overflow: hidden;
`

const SliderPreviewItem = styled.picture`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  border-radius: 4px;
  overflow: hidden;
  opacity: 0.3;
  transition: opacity 0.25s;
`

const SliderPreview = styled(Slider)`
  position: absolute;
  left: 48px;
  top: 48px;
  z-index: 5;
  width: 72px;

  .slick-slide {
    margin-bottom: 24px;
    cursor: pointer;
    width: 72px;
    height: 72px;
    border-radius: 4px;
    overflow: hidden;
    border: 2px solid transparent;
    background-color: ${({ theme }) => theme.colors.background};
    transition: background-color 0.25s, border-color 0.25s;

    @media ${({ theme }) => theme.media.desktop} {
      &:hover {
        border-color: ${({ theme }) => theme.colors.black};
        transition: border-color 0.25s;

        ${SliderPreviewItem} {
          opacity: 1;
          transition: opacity 0.25s;
        }
      }
    }
  }

  .slick-current {
    border-color: ${({ theme }) => theme.colors.border};
    background-color: #fff;
    transition: background-color 0.25s, border-color 0.25s;

    ${SliderPreviewItem} {
      opacity: 1;
    }
  }

  @media ${({ theme }) => theme.media.mobile} {
    display: none;
  }
`

const DetailPicturesLinks = styled.div`
  display: flex;
  align-items: center;
  flex-flow: row wrap;
  max-width: 79%;
  padding-top: 72px;

  a {
    font-weight: 600;
    font-size: 18px;
    line-height: 26px;
    margin-left: 16px;

    &:not(:first-of-type) {
      &:before {
        display: inline-block;
        vertical-align: middle;
        margin-right: 16px;
        content: '';
        width: 4px;
        height: 4px;
        border-radius: 50%;
        background-color: #000;
      }
    }
  }
`

const CatalogDetailPictures = ({ list, isAuth = false }) => {
  const [nav1, setNav1] = useState(null)
  const [nav2, setNav2] = useState(null)
  const [mobile, setMobile] = useState(false)
  const slider1 = useRef(null)
  const slider2 = useRef(null)
  const showSliders = list && list.length > 0
  const slidesToShow = list.length > 4 ? 4 : list.length

  useEffect(() => {
    setMobile(isMobile)
    setNav1(slider1.current)
    setNav2(slider2.current)
  }, [setMobile])

  return (
    <StyledCatalogDetailPictures>
      <SliderLarge
        asNavFor={!mobile ? nav2 : false}
        ref={slider1}
        adaptiveHeight
        arrows={false}
        dots={mobile}
        fade={!mobile}
      >
        {showSliders &&
          list.map((item, index) => (
            <React.Fragment key={`slider_large_item_${index}`}>
              <SliderLargeItem index={index}>
                <Image src={item} alt={''} layout="fill" objectFit={mobile ? 'cover' : 'contain'} />
              </SliderLargeItem>
            </React.Fragment>
          ))}
      </SliderLarge>

      {!mobile && (
        <SliderPreview
          asNavFor={nav1}
          ref={slider2}
          slidesToShow={slidesToShow}
          slidesToScroll={1}
          swipeToSlide={true}
          focusOnSelect={true}
          vertical
          verticalSwiping
          dots={false}
          arrows={false}
        >
          {showSliders &&
            list.map((item, index) => (
              <React.Fragment key={`slider_preview_item_${index}`}>
                <SliderPreviewItem>
                  <Image src={item} alt={''} layout="fill" objectFit="contain" />
                </SliderPreviewItem>
              </React.Fragment>
            ))}
        </SliderPreview>
      )}
      {isAuth && (
        <DetailPicturesLinks>
          <IconDownload />
          {list.map((item, index) => (
            <a href={item} key={`photo_link_${index}`} target="_blank" rel="noreferrer">
              {`Фото 0${index + 1}`}
            </a>
          ))}
        </DetailPicturesLinks>
      )}
    </StyledCatalogDetailPictures>
  )
}

export default CatalogDetailPictures
