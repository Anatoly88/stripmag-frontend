import styled, { css } from 'styled-components'
import CatalogSortRadio from './CatalogSortRadio'

const CatalogSortHeader = styled.div`
  padding-bottom: 12px;
  border-bottom: 1px solid ${({ theme }) => theme.colors.border};
  width: 100%;

  span {
    color: ${({ theme }) => theme.colors.grayText};
  }

  @media ${({ theme }) => theme.media.desktop} {
    border: none;
    padding: 0;
    width: auto;
  }
`

const CatalogSortList = styled.ul`
  padding: 28px 0 36px;
  list-style: none;
  margin: 0;
  width: 100%;
  @media ${({ theme }) => theme.media.desktop} {
    display: flex;
    justify-content: flex-end;
    align-items: center;
    padding: 0;
  }
`

const CatalogSortItem = styled.li`
  &:not(:last-of-type) {
    margin-bottom: 20px;

    @media ${({ theme }) => theme.media.desktop} {
      margin-bottom: 0;
    }
  }

  @media ${({ theme }) => theme.media.desktop} {
    margin-left: 10px;
  }
`

const CatalogSortWrapper = styled.div`
  @media ${({ theme }) => theme.media.desktop} {
    display: grid;
    grid-template-columns: 100px 1fr;
    align-items: center;
    align-items: center;
    padding-bottom: 20px;
  }
`

const CatalogSort = ({ sortList }) => {
  const sortItems = Object.entries(sortList).map(([key, val]) => {
    return { id: key, name: val }
  })

  return (
    <CatalogSortWrapper>
      <CatalogSortHeader>
        <span>Сортировка</span>
      </CatalogSortHeader>
      <CatalogSortList>
        {sortItems &&
          sortItems.length > 0 &&
          sortItems.map((item, index) => (
            <CatalogSortItem key={`sort_${index}`}>
              {/*// @ts-ignore*/}
              <CatalogSortRadio val={item.id} text={item.name} group={'sort'} />
            </CatalogSortItem>
          ))}
      </CatalogSortList>
    </CatalogSortWrapper>
  )
}

export default CatalogSort
