import styled, { css } from 'styled-components'

const StyledCatalogTitle = styled.h1`
  font-weight: 600;
  font-size: 56px;
  line-height: 64px;
  margin-top: 52px;
  margin-bottom: 60px;
  word-wrap: break-word;

  &:first-letter {
    text-transform: capitalize;
  }

  @media ${({ theme }) => theme.media.mobile} {
    margin: 16px 0 30px;
    font-size: 44px;
    line-height: 52px;

    ${(props) =>
      props.margin &&
      css`
        margin: ${({ margin }) => margin};
      `}
  }

  ${(props) =>
    props.margin &&
    css`
      margin: ${({ margin }) => margin};
    `}
`

interface ICatalogTitle {
  text: string
  margin?: string
}

const CatalogTitle = (props: ICatalogTitle) => {
  return <StyledCatalogTitle {...props}>{props.text}</StyledCatalogTitle>
}

export default CatalogTitle
