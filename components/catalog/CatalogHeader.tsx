import styled, { keyframes } from 'styled-components'
import CatalogTitle from './CatalogTitle'
import React, { useState } from 'react'
import IconArrowDown from '../../public/images/icons/arrow_down.svg'
import { lineClamp } from '../../styles/mixins'

const dropdownKeyframes = keyframes`
  0% {
    opacity: 0;
  }
  80% {
    opacity: 0.8;
  }
  100% {
    opacity: 1;
  }
`

const StyledCatalogHeader = styled.header`
  padding-bottom: 72px;

  @media ${({ theme }) => theme.media.mobile} {
    padding-bottom: 46px;
  }
`

const CatalogHeaderDescription = styled.div`
  max-width: 816px;
  font-size: 18px;
  line-height: 28px;
  margin-bottom: 72px;

  div {
    animation: ${dropdownKeyframes} 0.3s ease-in-out forwards;

    @media ${({ theme }) => theme.media.mobile} {
      ${lineClamp(5, '140px')}
    }
  }

  p {
    margin-bottom: 16px;

    &:last-of-type {
      margin-bottom: 0;
    }
  }

  ul {
    padding: 0;
    list-style: none;

    li {
      margin-bottom: 16px;

      &:last-of-type {
        margin-bottom: 0;
      }
    }
  }

  @media ${({ theme }) => theme.media.mobile} {
    margin-bottom: 60px;
  }
`

const CatalogHeaderShowBtn = styled.button`
  display: flex;
  align-items: center;
  font-weight: 600;
  font-size: 18px;
  line-height: 26px;
  background-color: transparent;
  border: none;
  outline: none;
  padding: 0;
  margin-top: 24px;

  svg {
    margin-left: 16px;
    transition: transform 0.25s;
  }

  &.isShowed {
    svg {
      transform: rotate(180deg);
      transition: transform 0.25s;
    }
  }
`

interface ICatalogHeaderProps {
  title: string
  titleMargin?: string
  preview?: string
  description?: string
  children?: React.ReactNode
}

const CatalogHeader = (props: ICatalogHeaderProps) => {
  const { title, titleMargin, preview, description } = props
  const [showDescription, setShowDescription] = useState(false)
  const showPreview = !showDescription && preview && preview.length > 0
  const showBtnDescription = description && description.length > 0
  return (
    <StyledCatalogHeader {...props}>
      <CatalogTitle text={title} margin={titleMargin} />

      {preview && (
        <>
          <CatalogHeaderDescription>
            {showPreview && <div dangerouslySetInnerHTML={{ __html: preview }} />}

            {showDescription && <div dangerouslySetInnerHTML={{ __html: description }} />}

            {showBtnDescription && (
              <CatalogHeaderShowBtn
                onClick={() => setShowDescription(!showDescription)}
                className={showDescription ? 'isShowed' : ''}
              >
                {!showDescription ? 'Развернуть' : 'Свернуть'}
                <IconArrowDown />
              </CatalogHeaderShowBtn>
            )}
          </CatalogHeaderDescription>
        </>
      )}
      {props.children}
    </StyledCatalogHeader>
  )
}

export default CatalogHeader
