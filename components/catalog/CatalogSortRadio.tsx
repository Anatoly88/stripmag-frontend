import IconCheck from '../../public/images/icons/check.svg'
import React from 'react'
import styled from 'styled-components'
import pickBy from 'lodash/pickBy'
import isEmpty from 'lodash/isEmpty'
import { useRouter } from 'next/router'
import queryString from 'query-string'

const RadioText = styled.span`
  font-size: 18px;
  line-height: 28px;
  transition: color 0.25s;

  @media ${({ theme }) => theme.media.desktop} {
    font-size: 14px;
    line-height: 20px;
    white-space: nowrap;
  }
`

const RadioIcon = styled.i`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 24px;
  height: 24px;
  border: 2px solid ${({ theme }) => theme.colors.lightGray};
  border-radius: 50%;
  color: ${({ theme }) => theme.colors.lightGray};
  margin-right: 16px;
  transition: border-color 0.25s;

  svg {
    opacity: 0;
    transition: opacity 0.25s;
  }

  @media ${({ theme }) => theme.media.desktop} {
    width: 20px;
    height: 20px;
    margin-right: 5px;
    display: none;
  }
`

const RadioEl = styled.input.attrs(({ val, name }) => ({
  type: 'radio',
  value: val,
  name,
}))`
  display: none;

  &:checked {
    + ${RadioIcon} {
      border-color: ${({ theme }) => theme.colors.black};
      color: ${({ theme }) => theme.colors.black};
      transition: border-color 0.25s;

      svg {
        opacity: 1;
        transition: opacity 0.25s;
      }

      + ${RadioText} {
        color: ${({ theme }) => theme.colors.black};
        transition: color 0.25s;
      }
    }
  }
`

const StyledRadio = styled.label`
  display: flex;
  align-items: center;
  flex-flow: row nowrap;
  cursor: pointer;
  color: ${({ theme }) => theme.colors.grayText};

  @media ${({ theme }) => theme.media.desktop} {
    &:hover,
    &:focus {
      color: ${({ theme }) => theme.colors.black};

      ${RadioIcon} {
        border-color: ${({ theme }) => theme.colors.black};
        color: ${({ theme }) => theme.colors.black};
      }
    }
  }
`

interface IRadio {
  val: string | number
  group: string
  text?: string
}

const CatalogSortRadio = ({ val, text, group }: IRadio) => {
  const router = useRouter()
  const { asPath, query } = router
  const currentQuery = pickBy(
    { ...(query || {}) },
    (q, key) => !isEmpty(q) && key !== 'slug' && key !== 'page' && key !== 'per_page'
  )
  const slug = asPath.split('?')[0]

  const handlerChange = (e, id) => {
    const isChecked = e.target.checked
    if (isChecked) {
      const newQuery = `?${queryString.stringify({
        ...currentQuery,
        sort: id,
      })}`

      router.replace(
        {
          pathname: slug,
          search: newQuery,
        },
        undefined,
        { scroll: false }
      )
    }
  }

  return (
    <StyledRadio>
      <RadioEl
        val={val}
        name={group}
        onChange={(event) => handlerChange(event, val)}
        checked={currentQuery.sort === val}
      />
      <RadioIcon>
        <IconCheck />
      </RadioIcon>
      <RadioText>{text}</RadioText>
    </StyledRadio>
  )
}

export default CatalogSortRadio
