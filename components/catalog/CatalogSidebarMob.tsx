import styled from 'styled-components'
import React, { useEffect, useState } from 'react'
import Checkbox from '../ui/Checkbox'
import Toggle from '../ui/Toggle'
import Radio from '../ui/Radio'
import { useRouter } from 'next/router'
import pickBy from 'lodash/pickBy'
import isEmpty from 'lodash/isEmpty'
import includes from 'lodash/includes'
import queryString from 'query-string'
import { useTypedSelector } from '../../hooks/useTypedSelector'
import { useDispatch } from 'react-redux'
import { addFilter, removeFilter } from '../../store/actions/filterActions'

const StyledCatalogSidebar = styled.aside`
  color: ${({ theme }) => theme.colors.grayText};
  width: 100%;
`

const SidebarItem = styled.div`
  &:not(:last-of-type) {
    margin-bottom: 60px;
  }
`

const SidebarHeader = styled.header`
  display: flex;
  justify-content: space-between;
  font-size: 16px;
  line-height: 24px;
  padding-bottom: 12px;
  border-bottom: 1px solid ${({ theme }) => theme.colors.border};
  margin-bottom: 28px;

  span {
    max-width: calc(100% - 100px);
  }
`

const SidebarSelectAll = styled.button`
  padding: 0;
  border: none;
  outline: none;
  font-size: 16px;
  line-height: 24px;
  background-color: transparent;
  width: 100px;
  white-space: nowrap;
`

const SidebarListWrapper = styled.div`
  overflow: hidden;
  border-bottom: ${({ withBorder, theme }) => (withBorder ? `1px solid ${theme.colors.border}` : 'none')};
`

const SidebarList = styled.ul`
  list-style: none;
  margin: 0 0 28px 0;
  padding: 0;
  height: 100%;
  max-height: 364px;
  overflow-y: auto;

  &::-webkit-scrollbar {
    height: 48px;
    width: 4px;
  }

  &::-webkit-scrollbar-track {
    background: transparent;
  }

  &::-webkit-scrollbar-thumb {
    background-color: ${({ theme }) => theme.colors.border};
    border-radius: 3px;
    width: 4px;
    height: 48px;
    border: 0;
  }
`

const SidebarListItem = styled.li`
  display: flex;
  align-items: center;
  padding-left: 0;
  font-size: 18px;
  line-height: 28px;

  &:not(:last-of-type) {
    margin-bottom: 20px;
  }
`

const CatalogSidebar = ({ children = null, filters = [], toggleList = [] }) => {
  const filtersCollection = filters.filter
  const router = useRouter()
  const { asPath, query } = router
  const currentQuery = pickBy({ ...(query || {}) }, (q, key) => !isEmpty(q) && key !== 'slug' && key !== 'page')
  const { filteredList } = useTypedSelector((state) => state.filter)
  const dispatch = useDispatch()
  const [routeParams, setRouteParams] = useState('')

  useEffect(() => {
    // console.log('useEffect', filteredList)
    // const nado = [{ brand: [1, 2, 3] }, { material: [1, 2, 3] }]
    // const newQueryStringify = nado.map((item) => {
    //   return Object.entries(item).map(([key, val]) => {
    //     return queryString.stringify(
    //       {
    //         [key]: val,
    //       },
    //       { arrayFormat: 'comma' }
    //     )
    //   })
    // })
    //
    // console.log(newQueryStringify.join('&'))
  }, [filteredList])

  const handlerSelectAll = (e) => {
    const el = e.target
    console.log('select all', el)
  }

  const handlerChecked = async (id, type) => {
    await dispatch(addFilter({ id, type }))
  }

  const handlerUnChecked = async (id, type) => {
    await dispatch(removeFilter({ id, type }))
  }

  return (
    <StyledCatalogSidebar>
      {children}
      {Object.entries(filtersCollection).map(([type, item], index) => {
        // @ts-ignore
        if (item.type === 'many' && item.values && item.values.length > 0) {
          return (
            <SidebarItem key={`many_filter_${index}`}>
              <SidebarHeader>
                {/*// @ts-ignore*/}
                <span>{item.name}</span>
                <SidebarSelectAll onClick={handlerSelectAll}>Выбрать все</SidebarSelectAll>
              </SidebarHeader>

              <SidebarListWrapper withBorder>
                <SidebarList>
                  {/*// @ts-ignore*/}
                  {item.values.map((el, index) => (
                    <SidebarListItem key={index}>
                      <Checkbox
                        text={el.name}
                        id={el.id}
                        filterType={type}
                        setCheckbox={handlerChecked}
                        removeCheckbox={handlerUnChecked}
                        // isChecked={getChecked(el.id, type)}
                      />
                    </SidebarListItem>
                  ))}
                  {/*// @ts-ignore*/}
                  {item.disabled_values.map((el, index) => (
                    <SidebarListItem key={index}>
                      <Checkbox
                        text={el.name}
                        id={el.id}
                        filterType={type}
                        setCheckbox={handlerChecked}
                        removeCheckbox={handlerUnChecked}
                        isDisabled
                      />
                    </SidebarListItem>
                  ))}
                </SidebarList>
              </SidebarListWrapper>
            </SidebarItem>
          )
        }

        // @ts-ignore
        if (item.type === 'one' && item.values && item.values.length > 0) {
          return (
            <SidebarItem key={`radio_filter_${index}`}>
              <SidebarHeader>
                {/*// @ts-ignore*/}
                <span>{item.name}</span>
              </SidebarHeader>

              <SidebarListWrapper>
                <SidebarList>
                  {/*// @ts-ignore*/}
                  {item.values.map((el, index) => (
                    <SidebarListItem key={index}>
                      <Radio text={el.name} val={el.id} group={`radio_1`} />
                    </SidebarListItem>
                  ))}
                </SidebarList>
              </SidebarListWrapper>
            </SidebarItem>
          )
        }
      })}

      {/* TOGGLE ITEM */}

      {/*<SidebarItem>*/}
      {/*  <SidebarHeader>*/}
      {/*    <span>{toggleList.name}</span>*/}
      {/*  </SidebarHeader>*/}

      {/*  <SidebarList>*/}
      {/*    {toggleList.disabled_values.map((item, index) =>*/}
      {/*      <SidebarListItem key={index}>*/}
      {/*        {item.name}*/}
      {/*        <Toggle margin={'0 0 0 auto'} disabled/>*/}
      {/*      </SidebarListItem>*/}
      {/*    )}*/}

      {/*    {toggleList.values.map((item, index) =>*/}
      {/*      <SidebarListItem key={index}>*/}
      {/*        {item.name}*/}
      {/*        <Toggle margin={'0 0 0 auto'} checked/>*/}
      {/*      </SidebarListItem>*/}
      {/*    )}*/}
      {/*  </SidebarList>*/}
      {/*</SidebarItem>*/}
    </StyledCatalogSidebar>
  )
}

export default CatalogSidebar
