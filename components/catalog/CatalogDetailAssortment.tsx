import styled from 'styled-components'
import Image from 'next/image'
import Moment from 'react-moment'
import 'moment/locale/ru'

const StyledCatalogDetailAssortment = styled.div`
  max-width: 600px;
  margin-left: auto;
  margin-bottom: 96px;

  @media ${({ theme }) => theme.media.mobile} {
    margin-bottom: 36px;
  }
`

const AssortmentTitle = styled.h3`
  font-weight: 600;
  font-size: 28px;
  line-height: 38px;
  margin-bottom: 40px;

  @media ${({ theme }) => theme.media.mobile} {
    font-size: 24px;
    line-height: 34px;
    margin-bottom: 36px;
  }
`

const AssortmentTable = styled.table`
  border-collapse: collapse;
  width: 100%;
`

const AssortmentHead = styled.th`
  border-top: 1px solid ${({ theme }) => theme.colors.border};
  border-bottom: 1px solid ${({ theme }) => theme.colors.border};
  display: flex;
  width: 100%;
`

const AssortmentRow = styled.tr`
  border-bottom: 1px solid ${({ theme }) => theme.colors.border};
  display: flex;
  width: 100%;
`
const AssortmentCol = styled.td`
  padding: 24px 12px 23px;
  font-size: 16px;
  line-height: 24px;
  text-align: left;
  width: 27%;
  border-right: 1px solid ${({ theme }) => theme.colors.border};

  &:first-of-type {
    width: 46%;
    padding-left: 0;
  }

  &:last-of-type {
    border-right: none;
  }

  div {
    display: flex;
    align-items: center;
    justify-content: flex-start;
    max-width: 100%;

    span {
      display: inline-block;
      margin-right: 12px;
      max-width: calc(100% - 16px - 12px);
    }
  }

  time {
    display: block;
  }
`

const AssortmentColorImage = styled(Image)`
  border-radius: 50%;
  overflow: hidden;
`

const AssortmentList = styled.ul`
  padding: 24px 0;
  margin: 0;
  list-style: none;

  &:not(:last-of-type) {
    border-bottom: 1px solid ${({ theme }) => theme.colors.border};
  }
`

const AssortmentItem = styled.li`
  display: flex;
  align-items: center;
  color: ${({ theme }) => theme.colors.grayText};

  &.assortment-color {
    color: ${({ theme }) => theme.colors.black};

    > span {
      display: flex;
      align-items: center;

      > span {
        display: inline-block;
        margin-left: 12px;

        &:first-letter {
          text-transform: capitalize;
        }
      }

      + span {
        position: relative;
        margin-left: 36px;

        &:before {
          content: '';
          position: absolute;
          left: -20px;
          top: 50%;
          width: 4px;
          height: 4px;
          border-radius: 50%;
          background-color: #000;
          transform: translateY(-50%);
        }
      }
    }
  }

  &:not(:last-of-type) {
    margin-bottom: 8px;
  }
`

interface IAssortmentItem {
  barcode: string
  color_id: string
  color_image: string
  color_name: string
  deltacode: string
  dsBarcode: string
  id: string
  num: string
  size_id: string
  size_name: string
  shipping_date: string
}

interface IAssortmentProps {
  list: IAssortmentItem[]
  isMob?: boolean
}

const CatalogDetailAssortment = ({ list, isMob = false }: IAssortmentProps) => {
  const showRow = list && list.length > 0

  return (
    <StyledCatalogDetailAssortment>
      <AssortmentTitle>Варианты</AssortmentTitle>
      {isMob &&
        showRow &&
        list.map((item, index) => (
          <AssortmentList key={`assortment_list_${index}`}>
            <AssortmentItem className={'assortment-color'}>
              <span>
                <AssortmentColorImage src={item.color_image} alt={item.color_name} width={16} height={16} />
                <span>{item.color_name}</span>
              </span>
              <span>{item.size_name}</span>
            </AssortmentItem>
            <AssortmentItem>
              <span>Номер (aID) — </span>
              <span>{item.id}</span>
            </AssortmentItem>
            <AssortmentItem>
              <span>Штрихкод — </span>
              <span>{item.barcode}</span>
            </AssortmentItem>
            {item.shipping_date && (
              <AssortmentItem>
                <span>Дата отгрузки </span>
                <span>
                  <>
                    <Moment locale="ru" format="DD.MM.YYYY" withTitle>
                      {item.shipping_date}
                    </Moment>
                    <Moment locale="ru" format="HH:mm" withTitle>
                      {item.shipping_date}
                    </Moment>
                  </>
                </span>
              </AssortmentItem>
            )}
          </AssortmentList>
        ))}
      {!isMob && (
        <AssortmentTable>
          <AssortmentHead>
            <AssortmentCol>
              Цвет / <br />
              размер
            </AssortmentCol>
            <AssortmentCol>
              aID /
              <br />
              штрихкод
            </AssortmentCol>
            <AssortmentCol>
              Дата
              <br />
              отгрузки
            </AssortmentCol>
          </AssortmentHead>
          {showRow &&
            list.map((item, index) => (
              <AssortmentRow key={`row_${index}`}>
                <AssortmentCol>
                  <div>
                    <span>{item.color_name}</span>
                    <AssortmentColorImage src={item.color_image} alt={item.color_name} width={16} height={16} />
                  </div>
                  <div>{item.size_name}</div>
                </AssortmentCol>
                <AssortmentCol>
                  <div>{item.id}</div>
                  <div>{item.barcode}</div>
                </AssortmentCol>
                <AssortmentCol>
                  {item.shipping_date && (
                    <>
                      <Moment locale="ru" format="DD.MM.YYYY" withTitle>
                        {item.shipping_date}
                      </Moment>
                      <Moment locale="ru" format="HH:mm" withTitle>
                        {item.shipping_date}
                      </Moment>
                    </>
                  )}
                  {!item.shipping_date && <>&mdash;</>}
                </AssortmentCol>
              </AssortmentRow>
            ))}
        </AssortmentTable>
      )}
    </StyledCatalogDetailAssortment>
  )
}

export default CatalogDetailAssortment
