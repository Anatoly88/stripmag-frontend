import styled from "styled-components"
import { ICatalogItem } from "../../interfaces/catalog";
import Link from 'next/link'
import CatalogCard from "./CatalogCard";


const StyledPreviewMain = styled.div`
  position: relative;
  
  a {
    &:hover {
      text-decoration: none;
    }
  }
`

const PreviewMain = (props: ICatalogItem) => {
  return (
    <StyledPreviewMain { ...props } >
      <Link href={props.slug}>
        <a href="">
          <CatalogCard { ...props } />
        </a>
      </Link>
    </StyledPreviewMain>
  )
};

export default PreviewMain;