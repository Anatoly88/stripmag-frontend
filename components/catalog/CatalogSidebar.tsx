import styled from 'styled-components'
import React from 'react'
import Checkbox from '../ui/Checkbox'
import Toggle from '../ui/Toggle'
import Radio from '../ui/Radio'
import { useRouter } from 'next/router'
import pickBy from 'lodash/pickBy'
import isEmpty from 'lodash/isEmpty'
import queryString from 'query-string'

const StyledCatalogSidebar = styled.aside`
  color: ${({ theme }) => theme.colors.grayText};
  width: 100%;
`

const SidebarItem = styled.div`
  &:not(:last-of-type) {
    margin-bottom: 60px;
  }

  &.isHidden {
    display: none;
  }
`

const SidebarHeader = styled.header`
  display: flex;
  justify-content: space-between;
  font-size: 16px;
  line-height: 24px;
  padding-bottom: 12px;
  border-bottom: 1px solid ${({ theme }) => theme.colors.border};
  margin-bottom: 28px;

  span {
    max-width: calc(100% - 100px);
  }
`

const SidebarSelectAll = styled.button`
  padding: 0;
  border: none;
  outline: none;
  font-size: 16px;
  line-height: 24px;
  background-color: transparent;
  width: 100px;
  white-space: nowrap;
`

const SidebarListWrapper = styled.div`
  overflow: hidden;
  border-bottom: ${({ withBorder, theme }) => (withBorder ? `1px solid ${theme.colors.border}` : 'none')};
`

const SidebarList = styled.ul`
  list-style: none;
  margin: 0 0 28px 0;
  padding: 0;
  height: 100%;
  max-height: 364px;
  overflow-y: auto;

  &::-webkit-scrollbar {
    height: 48px;
    width: 4px;
  }

  &::-webkit-scrollbar-track {
    background: transparent;
  }

  &::-webkit-scrollbar-thumb {
    background-color: ${({ theme }) => theme.colors.border};
    border-radius: 3px;
    width: 4px;
    height: 48px;
    border: 0;
  }
`

const SidebarListItem = styled.li`
  display: flex;
  align-items: center;
  padding-left: 0;
  font-size: 18px;
  line-height: 28px;

  &:not(:last-of-type) {
    margin-bottom: 20px;
  }
`

const CatalogSidebar = ({ children = null, filters, toggleList = [] }) => {
  const filtersCollection = filters.filter
  const router = useRouter()
  const { asPath, query } = router
  const slug = asPath.split('?')[0]
  const currentQuery = pickBy(
    { ...(query || {}) },
    (q, key) => !isEmpty(q) && key !== 'slug' && key !== 'page' && key !== 'id'
  )
  // TODO отрефакторить
  // const addFilter = (filter) => {
  //   const slug = asPath.split('?')[0]
  //   const selectedFilterType = `${filter.type}`
  //   const isFiltered = Object.fromEntries(
  //     Object.entries(currentQuery).filter(([key, value]) => key === selectedFilterType)
  //   )
  //   let params = ''
  //
  //   if (!isEmpty(isFiltered)) {
  //     const newQuery = Object.entries(currentQuery).map(([key, val]) => {
  //       let tmp = {}
  //       if (key !== filter.type) {
  //         // @ts-ignore
  //         tmp = { [key]: val.split(',') }
  //       }
  //       return tmp
  //     })
  //
  //     const newQueryStringify = newQuery.map((item) => {
  //       return queryString.stringify(
  //         {
  //           ...item,
  //         },
  //         { arrayFormat: 'comma' }
  //       )
  //     })
  //     const selectedParams = newQueryStringify.join('')
  //     const filteredValues = currentQuery[filter.type].split(',')
  //     const currentParams = `?${queryString.stringify(
  //       {
  //         [filter.type]: [filter.id, ...filteredValues],
  //       },
  //       { arrayFormat: 'comma' }
  //     )}`
  //
  //     params = `${currentParams}&${selectedParams}`
  //     const isCurrentVal = filteredValues.includes(String(filter.id))
  //     if (!isCurrentVal) {
  //       router.replace(`${slug}${params}`, undefined, { scroll: false })
  //     }
  //   } else {
  //     const newQuery = Object.entries(currentQuery).map(([key, val]) => {
  //       // @ts-ignore
  //       return { [key]: val.split(',') }
  //     })
  //     const newQueryStringify = newQuery.map((item) => {
  //       return queryString.stringify(
  //         {
  //           ...item,
  //         },
  //         { arrayFormat: 'comma' }
  //       )
  //     })
  //     const selectedParams = newQueryStringify.join('&')
  //     const currentParams = `?${queryString.stringify(
  //       {
  //         [filter.type]: [filter.id],
  //       },
  //       { arrayFormat: 'comma' }
  //     )}`
  //     params = `${currentParams}&${selectedParams}`
  //     router.replace(`${slug}${params}`, undefined, { scroll: false })
  //   }
  // }

  const handlerSelectAll = (e) => {
    const el = e.target
    console.log('select all', el)
  }

  const handlerChecked = (id, type) => {
    const selectedFilterType = type
    const isFiltered = Object.fromEntries(
      Object.entries(currentQuery).filter(([key, value]) => key === selectedFilterType)
    )
    let params = ''

    if (!isEmpty(isFiltered)) {
      const newQuery = Object.entries(currentQuery).map(([key, val]) => {
        let tmp = {}
        if (key !== type) {
          // @ts-ignore
          tmp = { [key]: val.split(',') }
        }
        return tmp
      })

      const newQueryStringify = newQuery.map((item) => {
        return queryString.stringify(
          {
            ...item,
          },
          { arrayFormat: 'comma' }
        )
      })
      const selectedParams = newQueryStringify.join('')
      const filteredValues = currentQuery[type].split(',')
      const currentParams = `?${queryString.stringify(
        {
          [type]: [id, ...filteredValues],
        },
        { arrayFormat: 'comma' }
      )}`

      params = `${currentParams}&${selectedParams}`
      const isCurrentVal = filteredValues.includes(String(id))
      if (!isCurrentVal) {
        router.replace(`${slug}${params}`, undefined, { scroll: false })
      }
    } else {
      const newQuery = Object.entries(currentQuery).map(([key, val]) => {
        // @ts-ignore
        return { [key]: val.split(',') }
      })
      const newQueryStringify = newQuery.map((item) => {
        return queryString.stringify(
          {
            ...item,
          },
          { arrayFormat: 'comma' }
        )
      })
      const selectedParams = newQueryStringify.join('&')
      const currentParams = `?${queryString.stringify(
        {
          [type]: [id],
        },
        { arrayFormat: 'comma' }
      )}`
      params = `${currentParams}&${selectedParams}`
      router.replace(`${slug}${params}`, undefined, { scroll: false })
    }
  }

  const handlerUnChecked = (id, type) => {
    const currentFilterQuery = Object.fromEntries(Object.entries(currentQuery).filter(([key, value]) => key === type))
    const othersFilterQuery = Object.fromEntries(Object.entries(currentQuery).filter(([key, value]) => key !== type))
    // @ts-ignore
    const currentFilterValues = currentFilterQuery[type].split(',')

    if (currentFilterValues.length > 1) {
      const newValues = currentFilterValues.filter((item) => item !== `${id}`)
      const othersQuery = Object.entries(othersFilterQuery).map(([key, val]) => {
        // @ts-ignore
        return { [key]: val.split(',') }
      })
      const currentStringify = `?${queryString.stringify(
        {
          [type]: [...newValues],
        },
        { arrayFormat: 'comma' }
      )}`
      const othersQueryStringify = othersQuery.map((item) => {
        return queryString.stringify(
          {
            ...item,
          },
          { arrayFormat: 'comma' }
        )
      })
      const othersParams = othersQueryStringify.join('&')
      const params = `${currentStringify}&${othersParams}`
      router.push(`${slug}${params}`, undefined, { scroll: false })
    } else {
      const othersQuery = Object.entries(othersFilterQuery).map(([key, val]) => {
        // @ts-ignore
        return { [key]: val.split(',') }
      })
      const othersQueryStringify = othersQuery.map((item) => {
        return `?${queryString.stringify(
          {
            ...item,
          },
          { arrayFormat: 'comma' }
        )}`
      })
      const params = othersQueryStringify.join('&')
      router.push(`${slug}${params}`, undefined, { scroll: false })
    }
  }

  const getChecked = (id, type) => {
    const newQuery = Object.entries(currentQuery).map(([key, val]) => {
      // @ts-ignore
      return { [key]: val.split(',') }
    })

    const isChecked = newQuery.map((item) => {
      if (item.hasOwnProperty(type)) {
        return item[type].includes(`${id}`)
      }
    })
    return isChecked.includes(true)
  }

  return (
    <StyledCatalogSidebar>
      {children}
      {Object.entries(filtersCollection).map(([type, item], index) => {
        // @ts-ignore
        if (item.type === 'many' && item.values && item.values.length > 0) {
          return (
            <SidebarItem
              key={`many_filter_${index}`}
              // @ts-ignore
              className={item.values.length === 1 && item.disabled_values.length === 0 ? 'isHidden' : ''}
            >
              <SidebarHeader>
                {/*// @ts-ignore*/}
                <span>{item.name}</span>
                <SidebarSelectAll onClick={handlerSelectAll}>Выбрать все</SidebarSelectAll>
              </SidebarHeader>

              <SidebarListWrapper withBorder>
                <SidebarList>
                  {/*// @ts-ignore*/}
                  {item.values.map((el, index) => (
                    <SidebarListItem key={index}>
                      <Checkbox
                        text={el.name}
                        id={el.id}
                        filterType={type}
                        setCheckbox={handlerChecked}
                        removeCheckbox={handlerUnChecked}
                        isChecked={getChecked(el.id, type)}
                      />
                    </SidebarListItem>
                  ))}
                  {/*// @ts-ignore*/}
                  {item.disabled_values.map((el, index) => (
                    <SidebarListItem key={index}>
                      <Checkbox
                        text={el.name}
                        id={el.id}
                        filterType={type}
                        setCheckbox={handlerChecked}
                        removeCheckbox={handlerUnChecked}
                        isChecked={getChecked(el.id, type)}
                        isDisabled
                      />
                    </SidebarListItem>
                  ))}
                </SidebarList>
              </SidebarListWrapper>
            </SidebarItem>
          )
        }

        // @ts-ignore
        if (item.type === 'one' && item.values && item.values.length > 0) {
          return (
            <SidebarItem key={`radio_filter_${index}`}>
              <SidebarHeader>
                {/*// @ts-ignore*/}
                <span>{item.name}</span>
              </SidebarHeader>

              <SidebarListWrapper>
                <SidebarList>
                  {/*// @ts-ignore*/}
                  {item.values.map((el, index) => (
                    <SidebarListItem key={index}>
                      <Radio text={el.name} val={el.id} group={`radio_1`} />
                    </SidebarListItem>
                  ))}
                </SidebarList>
              </SidebarListWrapper>
            </SidebarItem>
          )
        }
      })}

      {/* TOGGLE ITEM */}

      {/*<SidebarItem>*/}
      {/*  <SidebarHeader>*/}
      {/*    <span>{toggleList.name}</span>*/}
      {/*  </SidebarHeader>*/}

      {/*  <SidebarList>*/}
      {/*    {toggleList.disabled_values.map((item, index) =>*/}
      {/*      <SidebarListItem key={index}>*/}
      {/*        {item.name}*/}
      {/*        <Toggle margin={'0 0 0 auto'} disabled/>*/}
      {/*      </SidebarListItem>*/}
      {/*    )}*/}

      {/*    {toggleList.values.map((item, index) =>*/}
      {/*      <SidebarListItem key={index}>*/}
      {/*        {item.name}*/}
      {/*        <Toggle margin={'0 0 0 auto'} checked/>*/}
      {/*      </SidebarListItem>*/}
      {/*    )}*/}
      {/*  </SidebarList>*/}
      {/*</SidebarItem>*/}
    </StyledCatalogSidebar>
  )
}

export default CatalogSidebar
