export const hideScrollbar = () => {
  return `
    scrollbar-width: none; /* For Firefox */
    -ms-overflow-style: none; /* For Internet Explorer and Edge */

    &::-webkit-scrollbar {
      width: 0; /* For Chrome, Safari, and Opera */
    }
    
    &::-webkit-scrollbar {
      width: 0;
      height: 0;
    }

    &::-webkit-scrollbar-button {
      background: #ccc
    }

    &::-webkit-scrollbar-track-piece {
      background: #888
    }

    &::-webkit-scrollbar-thumb {
      background: #eee
    }
  `
}

export const lineClamp = (numRows = 1, height = '20px') => {
  return `
    display: -webkit-box; 
    overflow: hidden;
    -webkit-line-clamp: ${numRows};
    -webkit-box-orient: vertical; 
    max-height: ${height};
  `
}