export const nav = {
  catalog: [
    {
      name: '18+',
      staticUrl: '/adult/',
      categories: [
        {
          name: 'Фаллоимитаторы',
          staticUrl: '/adult/dick/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/dick/water/'
            },{
              name: 'На силиконовой основе',
              staticUrl: '/adult/dick/silicon/'
            },{
              name: 'Анальные смазки',
              staticUrl: '/adult/dick/anal/'
            },{
              name: 'Возбуждающие',
              staticUrl: '/adult/dick/exciting/'
            },{
              name: 'Пролонгаторы' ,
              staticUrl: '/adult/dick/prolongators/'
            }
          ]
        },
        {
          name: 'Вибраторы',
          staticUrl: '/adult/vibration/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/vibration/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/vibration/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/vibration/anal/'
            }
          ]
        },
        {
          name: 'Анальные игрушки',
          staticUrl: '/adult/anal/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/anal/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/anal/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/anal/anal/'
            }
          ]
        },
        {
          name: 'Страпоны, фаллопротезы',
          staticUrl: '/adult/strap/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/strap/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/strap/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/strap/anal/'
            }
          ]
        },
        {
          name: 'Смазки, лубриканты',
          staticUrl: '/adult/wd40/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/wd40/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/wd40/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/wd40/anal/'
            }
          ]
        },
        {
          name: 'Секс-мебель и качели',
          staticUrl: '/adult/mebel/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/mebel/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/mebel/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/mebel/anal/'
            }
          ]
        },
        {
          name: 'Приятные мелочи',
          staticUrl: '/adult/enjoy/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/enjoy/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/enjoy/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/enjoy/anal/'
            }
          ]
        },
        {
          name: 'БАДы',
          staticUrl: '/adult/bad/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/bad/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/bad/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/bad/anal/'
            }
          ]
        },
        {
          name: 'BDSM',
          staticUrl: '/adult/bdsm',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/bdsm/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/bdsm/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/bdsm/anal/'
            }
          ]
        }
      ]
    },
    {
      name: 'Классическое белье',
      staticUrl: '/classic-underwear/',
      categories: [
        {
          name: 'Фаллоимитаторы',
          staticUrl: '/adult/dick/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/dick/water/'
            },{
              name: 'На силиконовой основе',
              staticUrl: '/adult/dick/silicon/'
            },{
              name: 'Анальные смазки',
              staticUrl: '/adult/dick/anal/'
            },{
              name: 'Возбуждающие',
              staticUrl: '/adult/dick/exciting/'
            },{
              name: 'Пролонгаторы' ,
              staticUrl: '/adult/dick/prolongators/'
            }
          ]
        },
        {
          name: 'Вибраторы',
          staticUrl: '/adult/vibration/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/vibration/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/vibration/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/vibration/anal/'
            }
          ]
        },
        {
          name: 'Анальные игрушки',
          staticUrl: '/adult/anal/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/anal/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/anal/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/anal/anal/'
            }
          ]
        },
        {
          name: 'Страпоны, фаллопротезы',
          staticUrl: '/adult/strap/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/strap/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/strap/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/strap/anal/'
            }
          ]
        },
        {
          name: 'Смазки, лубриканты',
          staticUrl: '/adult/wd40/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/wd40/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/wd40/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/wd40/anal/'
            }
          ]
        },
        {
          name: 'Секс-мебель и качели',
          staticUrl: '/adult/mebel/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/mebel/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/mebel/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/mebel/anal/'
            }
          ]
        },
        {
          name: 'Приятные мелочи',
          staticUrl: '/adult/enjoy/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/enjoy/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/enjoy/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/enjoy/anal/'
            }
          ]
        },
        {
          name: 'БАДы',
          staticUrl: '/adult/bad/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/bad/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/bad/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/bad/anal/'
            }
          ]
        },
        {
          name: 'BDSM',
          staticUrl: '/adult/bdsm',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/bdsm/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/bdsm/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/bdsm/anal/'
            }
          ]
        }
      ]
    },
    {
      name: 'Эротическое белье',
      staticUrl: '/erotic-underwear/',
      categories: [
        {
          name: 'Фаллоимитаторы',
          staticUrl: '/adult/dick/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/dick/water/'
            }, {
              name: 'На силиконовой основе',
              staticUrl: '/adult/dick/silicon/'
            }, {
              name: 'Анальные смазки',
              staticUrl: '/adult/dick/anal/'
            }, {
              name: 'Возбуждающие',
              staticUrl: '/adult/dick/exciting/'
            }, {
              name: 'Пролонгаторы',
              staticUrl: '/adult/dick/prolongators/'
            }
          ]
        },
        {
          name: 'Вибраторы',
          staticUrl: '/adult/vibration/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/vibration/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/vibration/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/vibration/anal/'
            }
          ]
        },
        {
          name: 'Анальные игрушки',
          staticUrl: '/adult/anal/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/anal/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/anal/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/anal/anal/'
            }
          ]
        },
        {
          name: 'Страпоны, фаллопротезы',
          staticUrl: '/adult/strap/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/strap/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/strap/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/strap/anal/'
            }
          ]
        },
        {
          name: 'Смазки, лубриканты',
          staticUrl: '/adult/wd40/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/wd40/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/wd40/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/wd40/anal/'
            }
          ]
        },
        {
          name: 'Секс-мебель и качели',
          staticUrl: '/adult/mebel/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/mebel/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/mebel/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/mebel/anal/'
            }
          ]
        },
        {
          name: 'Приятные мелочи',
          staticUrl: '/adult/enjoy/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/enjoy/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/enjoy/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/enjoy/anal/'
            }
          ]
        },
        {
          name: 'БАДы',
          staticUrl: '/adult/bad/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/bad/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/bad/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/bad/anal/'
            }
          ]
        },
        {
          name: 'BDSM',
          staticUrl: '/adult/bdsm',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/bdsm/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/bdsm/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/bdsm/anal/'
            }
          ]
        },
        {
          name: 'Секс-мебель и качели',
          staticUrl: '/adult/mebel/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/mebel/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/mebel/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/mebel/anal/'
            }
          ]
        },
        {
          name: 'Приятные мелочи',
          staticUrl: '/adult/enjoy/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/enjoy/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/enjoy/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/enjoy/anal/'
            }
          ]
        },
        {
          name: 'БАДы',
          staticUrl: '/adult/bad/',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/bad/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/bad/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/bad/anal/'
            }
          ]
        },
        {
          name: 'BDSM',
          staticUrl: '/adult/bdsm',
          pages: [
            {
              name: 'На водной основе',
              staticUrl: '/adult/bdsm/water/'
            },
            {
              name: 'На силиконовой основе',
              staticUrl: '/adult/bdsm/silicon/'
            },
            {
              name: 'Анальные смазки',
              staticUrl: '/adult/bdsm/anal/'
            }
          ]
        },
      ]
    }
  ],
  sections: [
    {
      name: 'О компании',
      staticUrl: '/about/',
      pages: [
        {
          name: 'Миссия',
          staticUrl: '/about/mission/'
        },
        {
          name: 'Новости',
          staticUrl: '/about/news/'
        },
        {
          name: 'О нас говорят',
          staticUrl: '/about/speak/'
        },
        {
          name: 'Для СМИ',
          staticUrl: '/about/media/'
        },
        {
          name: 'Вакансии',
          staticUrl: '/about/vacancy/'
        },
        {
          name: 'Контакты',
          staticUrl: '/about/contacts/'
        }
      ]
    },
    {
      name: 'Сотрудничество',
      staticUrl: '/partnership/',
      pages: [
        {
          name: 'Маркетплейсы',
          staticUrl: '/partnership/marketplaces/'
        },
        {
          name: 'Интернет-магазины',
          staticUrl: '/partnership/online-shop/'
        },
        {
          name: 'Дропшиппинг',
          staticUrl: '/partnership/dropshipping/'
        },
        {
          name: 'Оптовые покупки',
          staticUrl: '/partnership/opt'
        },
        {
          name: 'Совместные покупки',
          staticUrl: '/partnership/sp'
        },
        {
          name: 'Готовые сайты',
          staticUrl: 'https://fire.p5s.ru/?utm_source=p5s.ru&utm_medium=organic&utm_campaign=website_p5s.ru'
        }
      ]
    },
    {
      name: 'Партнеры',
      staticUrl: '/partners/',
      pages: [
        {
          name: 'InSales',
          staticUrl: '/partners/insales/'
        },
        {
          name: 'AdvantShop',
          staticUrl: '/partners/advantshop/'
        },
        {
          name: 'Робокасса',
          staticUrl: '/partners/robokassa/'
        },
        {
          name: 'Розовый слоник',
          staticUrl: '/partners/pink-elephant/'
        },
        {
          name: 'Kokoc group',
          staticUrl: '/partners/kokoc/'
        }
      ]
    },
    {
      name: 'Ценовая политика',
      staticUrl: '/price-policy/',
      pages: [
        {
          name: 'Бренды без маркетплейсов',
          staticUrl: '/price-policy/no-mp/'
        },
        {
          name: 'Бренды без ограничений',
          staticUrl: '/price-policy/no-limits/'
        },
        {
          name: 'Бренды с РРЦ',
          staticUrl: '/price-policy/with-rrc/'
        }
      ]
    },
    {
      name: 'Возврат и гарантия',
      staticUrl: '/return-warranty/',
      pages: [
        {
          name: 'Возврат дропшиппинг',
          staticUrl: '/return-warranty/ds/'
        },
        {
          name: 'Возврат Почтой РФ',
          staticUrl:' /return-warranty/post/'
        },
        {
          name: 'Возврат PickPoint',
          staticUrl: '/return-warranty/pick-point/'
        },
        {
          name: 'Возврат в офисе',
          staticUrl:' /return-warranty/office/'
        },
        {
          name: 'Гарантия',
          staticUrl:' /return-warranty/warranty/'
        }
      ]
    },
    {
      name: 'API и выгрузки',
      staticUrl: '/api-uploads/',
      pages: [
        {
          name: 'API',
          staticUrl: '/api-uploads/api/'
        },
        {
          name: 'API дропшиппинг',
          staticUrl: '/api-uploads/api-ds/'
        },
        {
          name: 'Популярные выгрузки',
          staticUrl: '/api-uploads/popular/'
        },
        {
          name: 'Smart Feed',
          staticUrl: '/api-uploads/smart-feed/'
        }
      ]
    }
  ]
}

const menuExample = {
  status: 1,
  data: [
    {
      "id": "395",
      "parentId": "0",
      "name": "18+",
      "children": [
        {
          "id": "4",
          "parentId": "395",
          "name": "Фаллоимитаторы",
          "children": [
            {
              "id": "21",
              "parentId": "4",
              "name": "Реалистичные",
              "children": []
            },
            {
              "id": "82",
              "parentId": "4",
              "name": "Классические дилдо",
              "children": []
            },
          ]
        }
      ]
    }
  ]
}