interface IBannerImage {
  url: string
  alt?: string
}

export interface IBanner {
  title: string
  description: string
  html: string
  image: IBannerImage | null
  htmlUrl: string
}