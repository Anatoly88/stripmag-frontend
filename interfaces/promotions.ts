export interface IPromotionsCard {
  name: string
  id: string
  start_date?: string
  end_date: string
  image200?: string
  image500?: string
}
