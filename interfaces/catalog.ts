interface IImage {
  url: string
  alt?: string
}

interface ICatalogItemTooltip {
  discount: string
  new: string
}

export interface ICatalogItem {
  id?: number
  name: string
  prodcode?: string
  brand_id?: number
  brand_name: string
  brand_code?: string
  image: string
  slug: string
  tooltip?: ICatalogItemTooltip
  price: number
  old_price?: number
}

export interface ICatalogFilterValue {
  name: string
  id: number | string
}

export interface ICatalogFilterItem {
  name: string
  type: string
  values?: ICatalogFilterValue[]
  disabled_values?: any[]
}

export interface ICatalogSeo {
  pageTitle: string
  pageDescription: string
  pageKeywords: string
}

export interface ICatalogPageData {
  id: string
  name: string
  description: string
  h1Title: string
  seo: ICatalogSeo
}

export interface IFilters {
  status: number | string
  filter: ICatalogFilterItem[]
  total: number
}

export interface ICatalogDetailPageData {
  id?: string
  name?: string
  marked?: string
  prodcode?: string
  hometext?: string
  bodytext?: string
  modified?: string
  usage_method?: string
  blend?: string
  dimensions?: string
  brutto?: string
  volume?: string
  length?: string
  diameter?: string
  vibration?: string
  gender_specific?: string
  function_id?: string
  function_name?: string
  additional_function_id?: string
  additional_function_name?: string
  batteries_id?: string
  batteries_function_name?: string
  pack_id?: string
  pack_name?: string
  brand_id?: string
  collection_id?: string
  material_id?: string
  basis_id?: string
  brand_name?: string
  collection?: string
  material?: string
  basis?: string
  model_year?: string
  price?: string
  old_price?: string
  discount?: string
  pageTitle?: string
  pageDescription?: string
  pageKeywords?: string
  images?: string[]
  assortment?: any[]
  bonuses?: string
}
