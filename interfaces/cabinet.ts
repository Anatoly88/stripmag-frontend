interface IBalanceDebt {
  balance: number
  over_credit: number
  day_limit: number
  delay: string
  shipments: number
  recommend_payment: number
}

export interface IBalance {
  is_dropshipping: boolean
  discount_type: string
  discount: number
  money_now: number
  money_reserv: number
  debt: IBalanceDebt
}

export interface IBalancePeriod {
  startBalance: number
  endBalance: number
  accrual: number
  writeOff: number
}

export interface IBalanceCurrent {
  main: number
  reserv: number
}

export interface IBalanceOperation {
  name: string
  description: string
  order: string
  date: string
  change: number
  balance: number
}

export interface IBalanceOperations {
  operationsForPeriod: IBalanceOperation[]
}
