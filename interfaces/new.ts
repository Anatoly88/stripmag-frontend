interface INewImage {
  url: string
  alt?: string
}

export interface INew {
  name: string
  image: INewImage
  slug: string
  createdAt: string
  author?: string
  withAuthor?: boolean
  isSpeakArticle?: boolean
}