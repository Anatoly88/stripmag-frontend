interface InterfaceSidebarNav {
  name: string
  staticUrl: string
}

export interface InterfaceStaticData {
  name: string
  menu: string
  title: string
  keywords: string
  description: string
  content: string
  pages: string[]
  status: number
  staticUrl: string
  sidebarNav: InterfaceSidebarNav[]
  mobile: boolean
}