export function imageOrDefault(item, key = null) {
	if (typeof item === 'object') {
		item = item?.(key ?? 'image')
	}

	return  item ?? '/images/no_image.svg'
}
