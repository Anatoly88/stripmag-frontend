module.exports = {
  images: {
    domains: ['p5s.ru', 'feed.p5s.ru', 'placeimg.com', 'stripmag.ru', 'strip.loc', 'p5s.local', 'p5s.loc'],
  },
    env: {
      API_URL: process.env.API_URL,
      API_MOCK_URL: process.env.API_MOCK_URL,
      NEXT_PUBLIC_API_URL: process.env.NEXT_PUBLIC_API_URL,
      NEXT_PUBLIC_API_MOCK_URL: process.env.NEXT_PUBLIC_API_MOCK_URL,
  },
}
